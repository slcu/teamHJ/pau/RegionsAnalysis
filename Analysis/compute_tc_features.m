function [data]=compute_tc_features(data,regionnumber)

%ii=find(data.regionlist==regionnumber);
ii=regionnumber;
dirout=strcat('region_',num2str(regionnumber));


font=18;
tc=data.region{ii}.radial_smooth_timecourse(:,:);
maxs=max(tc(:,:),[],1);
mins=min(tc(:,:),[],1);
fch=maxs./mins;


%tc_prod=data.region{ii}.radial_production_timecourse(:,:);
%maxs_prod=max(tc_prod(:,:),[],1);
%mins_prod=min(tc(:,:),[],1);

perf=zeros(length(data.region{ii}.rs),data.time_frames);
sqperf=zeros(length(data.region{ii}.rs),data.time_frames);
for k=1:data.time_frames
perf(:,k)=(tc(:,k)/mins(k))-1;
%perf(:,k)=tc(:,k);
sqperf(:,k)=(perf(:,k)).^2;
end
cm=(sum(sqperf(:,:)/2,1)./sum(perf(:,:),1))./fch;

data.region{ii}.measures.maxs=maxs;
%data.region{ii}.measures.maxs_prod=maxs_prod;
data.region{ii}.measures.distr=cm;


