function [data]=intensity_rectangular_region_in_time(inout,data,regionnumber)

% intensity_circular_region_in_time computes the intensity along the x-direction of the rectangle 

%ii=find(data.regionlist==regionnumber);

ii=regionnumber;

selected_image_set=zeros(data.image_size(1),data.image_size(2),2,data.time_frames);

if inout.background_subtraction==1
    %selected_image_set=data.back_YFP_sub_fluor_timecourse;     
    selected_image_set(:,:,1,1)=data.first_image_backgr_sub;   
else
    %selected_image_set=zeros(data.image_size(1),data.image_size(2),2,data.time_frames);
    %selected_image_set(:,:,1,data.time_frames)=data.first_image;    
    if inout.islsm==0
        selected_image_set=data.fluor_marker_timecourse;
    elseif inout.islsm==1
        selected_image_set(:,:,1,data.time_frames)=data.first_image;
    end
end

microns_per_pixel=data.microns_per_pixel;
microns_per_pixel_orthos=data.microns_per_pixel_orthos;
rect_length=(data.region{regionnumber}.pos1(1)-data.region{regionnumber}.pos0(1))*microns_per_pixel;

regionmask_Pixels=data.region{regionnumber}.regionmask;

ind=0; hwait=waitbar(0,'% of progress for time course profile computation'); % generates a waitbar

[row, col]=find(regionmask_Pixels);

for k=1:data.time_frames

    waitbar(1.0*ind/data.time_frames); ind=ind+1; 
    
    image=selected_image_set(:,:,1,k);
    image_tomeasure=double(regionmask_Pixels).*double(image);
    
    % extracting the portion of the image to measure
    bound_image_tomeasure=image_tomeasure(min(row):max(row),min(col):max(col));
    
    intensity_xs=mean(bound_image_tomeasure);
    av_intensity=mean2(bound_image_tomeasure);

    if inout.spatial_filtering==1
        % filtering
        intensity_xs=medfilt1(intensity_xs,5,'omitnan','truncate');
        intensity_xs=medfilt1(intensity_xs,5,'omitnan','truncate');
        intensity_xs=medfilt1(intensity_xs,5,'omitnan','truncate');
    end
    
    data.region{ii}.concentrations_in_x(:,k)=intensity_xs*microns_per_pixel_orthos;
    data.region{ii}.concentrations(k)=av_intensity*microns_per_pixel_orthos;

    % note that we don't need correction of the fluorescense here due to
    % zoom. See code in angular_integration2; we are computing a
    % concentration of intensity:
    % [I]= (sum pix intensity *resolution)/(number of pixels in the torus*
    % resolution) -> the resolution is cancelled out!
    
    %%%Casanova 2021-05-26
    %As we are measuring the intensity of a projection is a volumetric intensity 
    %so we need to multiply by the zresolution.
        
end
%rs=double((0:microns_per_pixel:rect_length));
conc_vector_length=size(data.region{ii}.concentrations_in_x,1);
rs=double((0:microns_per_pixel:(conc_vector_length-1)*microns_per_pixel));

data.region{ii}.rs=rs; % NOTE: this is defined for representation purposes


close(hwait);

end




