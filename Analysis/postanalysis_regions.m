function [dirdata]=postanalysis_regions(inout)
    % This function makes the fit with a fixed exponent into the dirdata
    % superstructure
    
    strname='fixed_exp_fit';
    
    % import dirdata
    dirdataname=['dir' inout.data_structure_filename '.mat'];    
    %if isfield(inout,'signal_channel_index')
    %    inout.data_structure_filename=strcat(inout.data_structure_filename,'_channel_',num2str(inout.signal_channel_index));
    %end
    src=load(fullfile(inout.datapath,dirdataname));
    
    dirdata=src.dirdata;
    num_datasets=size(dirdata.namefiles,2);
    
    
    inout.fixed_exp_exp=dirdata.stats.exp_exp;
    inout.fixed_hill_exp=dirdata.stats.hill_exp;
    
    for j=1:num_datasets
        subdir=dirdata.namefiles{j};
        if subdir(:,end)==' '
         subdir=subdir(:,1:end-1); % correcting those paths ending with a space
        end
        src=load(fullfile(inout.datapath,subdir,[inout.data_structure_filename '.mat']));
        data=src.data;
        
        list_of_regions=data.regionlist;
        % analysis on each data.first_image
        for i=1:length(list_of_regions) 
            regionnumber=list_of_regions(i);
            ii=regionnumber;
            time_point=data.time_frames;
            profile=data.region{ii}.radial_concentrations;
            crit_conc=inout.crit_conc;
            
            [data]=make_nonlinfit_profile_fixed_exponent(data,time_point,profile,inout,crit_conc,regionnumber,strname);
        end
        
        if inout.export_data==1     % export data 
            save(fullfile(inout.datapath,subdir,'data'),'data');
        end
    end % end for the different datasets
    
     % extract features and generating a new cvs file
    [dirdata]=create_dirdata(inout);
    
    
    disp(['Postanalysis done for ' inout.data_structure_filename]);
end 

