function get_fluorprofile(folderwithframes,nameofmovie,wheretofolder,normfactor,data)

folderwithframes(end+1) = '\';
wheretofolder(end+1) = '\';

zfiles = dir([folderwithframes,nameofmovie,'_*.tif']);

writerObj = VideoWriter([wheretofolder,nameofmovie,'_int',num2str(normfactor),'.avi']);
writerObj.FrameRate = 10;
open(writerObj);



% Geting one image
I = imread([folderwithframes,nameofmovie,'_t',num2str(length(zfiles)),'.tif']
I = I*normfactor;
imshow(I)

select_region_to_study_grad(data,imm,f3)
    
for i = 1:length(zfiles)
    I = imread([folderwithframes,nameofmovie,'_t',num2str(i),'.tif']);
    I = I*normfactor;
    imshow(I)
    frame = getframe;
    writeVideo(writerObj,frame);
end
close(writerObj)

end