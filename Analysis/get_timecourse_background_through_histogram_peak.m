function [inout,data]=get_timecourse_background_through_histogram_peak(inout,data)
% this function finds the first histogram peak to get the background
% estimation

nbins=40;
        
image=data.fluor_signal_timecourse(:,:,1);
[smooth_histofunc,xi]=ksdensity(image(:)) ;
% figure()
% plot(xi,smooth_histofunc)
[aux,int_peaks]=findpeaks(smooth_histofunc);
data.signal_background=xi(int_peaks(1));

inout.thresh_fluor_signal=data.signal_background;

if inout.number_tiff_channels==2
    image=data.fluor_marker_timecourse(:,:,1);
    [smooth_histofunc,xi]=ksdensity(image(:)) ;
   % figure()
   % plot(xi,smooth_histofunc)
    [aux,int_peaks]=findpeaks(smooth_histofunc);
    data.marker_background=xi(int_peaks(1));
    inout.thresh_fluor_marker=data.marker_background;

end

end

            
            