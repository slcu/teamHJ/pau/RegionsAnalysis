function [inout,data]=get_background_through_histogram_peak(inout,data)
% this function finds the first histogram peak to get the background
% estimation

nbins=40;
        
image=data.first_image;
%[smooth_histofunc,xi]=ksdensity(image(:)) ;
[smooth_histofunc,xi]=ksdensity(image(:),'NumPoints',1000) ; % note: default is 100

h=figure();
font=16;
plot(xi,smooth_histofunc,'linewidth',2)
xlabel('Intensity','fontsize',font); ylabel('Number of pixels','fontsize',font)
set(gca,'fontSize',font);
filename=strcat(data.rootstring_first_image_name, '_histo.pdf');
xlim([0,5000])
outfilename=fullfile(inout.datapath,filename);
save2pdf(outfilename,h);
    
[aux,int_peaks]=findpeaks(double(smooth_histofunc));
data.signal_background=xi(int_peaks(1));

inout.thresh_fluor_signal=data.signal_background;


end

            
            