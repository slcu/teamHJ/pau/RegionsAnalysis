function [data]=analyse_data(inout)

% analyse_data is a function that reads a set of TIF or LSM images,
% analyses and finally creates a data structure that stores the original
% rawdata and the background subtracted data as well.

% For executing it with the default inout structure parameters, write 
% [data]=read_rawdata; in the command line, 
% or just read_rawdata; in case a whole directory is explored 
% (and hence inout.open_all_files_in_directory=1)

if(nargin < 1)
    %inout=default_inout; % get the default inout parameters if none provided
    inout=quickdefault_inout; % get the default inout parameters if none provided
end


datafolder=inout.datafolder;

if ~isfield(inout,'datapath')
    datapath = uigetdir(datafolder,'Select the data set location');

    if ~datapath
    return;
    end

    disp('chosen directory is');
    disp(datapath)

    data.datapath=datapath;
else
    data.datapath=inout.datapath;
    datapath=inout.datapath;
end 


if inout.open_all_files_in_directory==1
    Dtif = dir(fullfile(data.datapath,'*.tif'));
    Dlsm = dir(fullfile(data.datapath,'*.lsm'));
    D=[Dlsm,Dtif];
    numfiles_to_open=length(D);
        
    inout.out_tiffs_path=fullfile(datapath,'tiffs_with_numbered_regions');
    create_dir(inout.out_tiffs_path);

else  
    src = uigetfile(fullfile(datapath,'*.*'), 'Select one TIF or LSM image file ');
    if ~src
    return;
    end
    numfiles_to_open=1;
end


for ifiles=1:numfiles_to_open  % Starting the directory loop

if inout.open_all_files_in_directory==1
    fullname=D(ifiles).name
    src=fullname;
end

% Name of the selected file
first_im_name=src;
data.first_image_name=first_im_name;

fin_rootstring=strfind(first_im_name,'.');

data.rootstring_first_image_name=first_im_name(1:fin_rootstring-1);

if inout.export_subfolderdatapath==1
        outdatapath=fullfile(datapath,data.rootstring_first_image_name);
    data.outdatapath=outdatapath;

    ex=exist(outdatapath);
    if ex==0
        mkdir(datapath,data.rootstring_first_image_name)
    end
else
        outdatapath=datapath;
        data.outdatapath=outdatapath;
end


if inout.timecourse==1
    fulldiroutdata=fullfile(outdatapath,'MOVS'); 
    ex=exist(fulldiroutdata);
    if ex==0
        mkdir(outdatapath,'MOVS')
    end
end

% Parse first_im_name to identify the string location of timepoint, color, stage
% position, and extension

fname=fullfile(datapath,first_im_name);

ini_ext_loc = regexp(first_im_name, '(\.tif|\.TIF|\.lsm)', 'start');
ext=first_im_name(ini_ext_loc+1:size(first_im_name,2));
inout.ext=ext;
islsm=0;

% Extracting relevant image information if the image file if it is in lsm format
if sum(ext=='lsm')==3
    islsm=1;
    infolsm=lsminfo(fname);
    data.resolution=(infolsm.VOXELSIZES)*10^6;
    xresolution=data.resolution(1,1);
    data.numchannels_image=infolsm.NUMBER_OF_CHANNELS; % not the same as inout.numchannels
    data.time_frames_image=infolsm.TIMESTACKSIZE; % 
    data.zs_image=infolsm.DimensionZ;
    inout.islsm=islsm;
    inout.radiusstep=inout.radiusstep_in_microns/xresolution;
end


info = imfinfo(fname);
num_images = numel(info);

% This part of the code read the lsm or tiff selected image

if num_images>1 && islsm==0  % lsm z-stack
    for k = 1:num_images
        A(:,:,k) = imread(fname, k, 'Info', info);
    end
    
    imshow3D(mat2gray(A));
    
    % Select the time interval you wish to focus on
    prompt = {'z-stack'};
    dlg_title = 'Select z';
    num_lines = 1;
    defaultanswer = {'15'};
    options.WindowStyle='normal';
    options.Interpreter='tex';
    answer = inputdlg(prompt, dlg_title, num_lines, defaultanswer, options);
    zstack=answer{1};

    im=A(:,:,str2num(zstack));
    
elseif islsm==1    % lsm z-stack 
    
    num_zs=data.zs_image;
    indexz=1;
    lsmdata=lsmread(fname);
    sq_lsmdata=squeeze(lsmdata(1,inout.lsm_signal_channel_index,:,:,:));
    A=permute(sq_lsmdata,[2 3 1]);

    % Managing different options for z-stacks: maximal intensity
    % projection, sum of the intensities or just deal with one z level.
    if num_zs>1  
        zproj=0;
        if and(inout.lsm_zsum==0,inout.lsm_zmax==1) 
            zproj=1;
        elseif and(inout.lsm_zsum==1,inout.lsm_zmax==0)
            zproj=2;
        else
            zproj=0;
        end

        
    switch zproj 
        case 0   
            imshow3D(mat2gray(A));

            % Select the time interval you wish to focus on
            prompt = {'z-stack'};
            dlg_title = 'Select z';
            num_lines = 1;
            defaultanswer = {'15'};
            options.WindowStyle='normal';
            options.Interpreter='tex';
            answer = inputdlg(prompt, dlg_title, num_lines, defaultanswer, options);
            zstack=answer{1};

            im=A(:,:,str2num(zstack));
        case 1
            im= max(A,[],3);
        case 2
            im= sum(A,3);
    end % end of what to do with the stack projection
    
    else % if it is not a stack
        im=sq_lsmdata;
    end
    
    data.selected_working_image=im;
    
    if inout.lsm_gauss_filter==1
        finalsigma=inout.gauss_sigma_microns/xresolution ;
        im = imgaussfilt(im,finalsigma);
    end
    
else % if it is not an lsm but a tiff
    im=imread(fname);
    %obj = Tiff(fname,'r')
end

if inout.standard_format_filename==1;
    ini_id = regexp(first_im_name, '_w[^_]*_s\d*_t\d*(\.tif|\.TIF)', 'match');
    ini_id_loc = regexp(first_im_name, '_w[^_]*_s\d*_t\d*(\.tif|\.TIF)', 'start');
    ini_t=strfind(ini_id{1},'_t') + ini_id_loc - 1;
    fin_t=strfind(ini_id{1},'.TIF') + ini_id_loc - 2;
    ini_w=strfind(ini_id{1},'_w') +  ini_id_loc - 1;
    ini_s=strfind(ini_id{1},'_s')  + ini_id_loc;
    stagepos=first_im_name(ini_s+1:ini_t-1);
    twochannels_string=strcat(first_im_name(1:ini_w),first_im_name(ini_s:ini_t-1));
    
    data.twochannels_string=twochannels_string;

    selected_time=first_im_name(ini_t+2:fin_t);

    ini_chan=ini_w+2;

    chosenchannel=first_im_name(ini_chan:ini_s-2);
    if sum(inout.scope(1:3)=='spi')
        
    if chosenchannel(1:19)=='1GFP-dsRED dual GFP'
        otherchannel='2GFP-dsRED dual DSRED';
        marker_channel=2; 
        signal_channel=1;
        data.selected_channel_string='1GFP-dsRED dual GFP';
    else
        otherchannel='1GFP-dsRED dual GFP';
        marker_channel=1; 
        signal_channel=2;
        data.selected_channel_string='2GFP-dsRED dual DSRED';
    end
    else
        
    aa=sum(findstr(chosenchannel, 'GFP'));

    if aa>0
        otherchannel='2TRICT';
        marker_channel=2; 
        signal_channel=1;
        data.selected_channel_string=chosenchannel;
    else
        otherchannel='3GFP';
        marker_channel=1; 
        signal_channel=2;
        data.selected_channel_string=chosenchannel;
    end
    end
    
    
    second_im_name=strcat(first_im_name(1:ini_chan-1),otherchannel,first_im_name(ini_s-1:end));

    imbis=imread(fullfile(datapath,second_im_name));

    gimbis=mat2gray(imbis);

    f2=figure();
    grgimbis=imshow(gimbis,[]);

    if marker_channel==1
    marker_image=im;
    signal_image=imbis;
    else
    marker_image=imbis;
    signal_image=im;
    end

    imm=zeros(size(im,1),size(im,2),3);
    imm(:,:,1)=mat2gray(marker_image);
    imm(:,:,2)=mat2gray(signal_image);
    %imm(:,:,3)=mat2gray(marker_image);

    f3=figure();
    is=imshow(imm);

    string_firstchannel=first_im_name(1:ini_t+1);
    string_secondchannel=second_im_name(1:ini_t+1);
    
    xx=length(chosenchannel)-length(otherchannel);
    string_secondchannel=second_im_name(1:ini_t+1-xx);

    strings={string_firstchannel string_secondchannel};
    
    data.stringmarker=strings{marker_channel};
    data.stringsignal=strings{signal_channel};
    %data.fluor_CFP_timecourse=fluor_CFP_timecourse;
    %data.fluor_YFP_timecourse=fluor_YFP_timecourse;

else 
    data.selected_channel_string='default_channel';
end

if isfield(inout,'selected_channel_string')
    data.selected_channel_string=inout.selected_channel_string;
end

data.image_size=size(im);
data.first_image=im;
gim=mat2gray(im);

f1=figure();
grgim=imshow(gim);

close(h)

end 


if inout.analysis_regions==1
    [data]=analysis_regions(inout,data,data.regionlist);
end


if inout.export_data==1
    save(fullfile(outdatapath, 'data'),'data');
    if inout.ok_boxes==1
        msgbox('Successfully exported data to the raw images folder.');
    end
end

close all



if and(inout.open_all_files_in_directory==1,inout.create_dirdata==1);
    %auxdata=data;
    dirdata.dataset{ifiles}=data;
end

% Make sure we start with an empty data structure when doing the directory
% systematic analysis; data structure is deleted from the memory to start building another data
% structure.


if and(inout.open_all_files_in_directory==1,exist('data','var'))
    clear data
end

% End of the directory reading loop
end

if and(inout.open_all_files_in_directory==1,inout.create_dirdata==1)
inout.fit_fixed_exp=0;    
[dirdata]=extract_features(dirdata,inout);
save(fullfile(datapath, 'dirdata'),'dirdata');



% Checking the numbers of determined regions in all data structures

multiple_regions=0;
number_of_regions_list=[];

for yy=1:size(dirdata.dataset,2);
    num_reg=size(dirdata.dataset{1}.regionlist,2);
    number_of_regions_list=[number_of_regions_list, num_reg];
    if num_reg>1
        multiple_regions=1;
    end
end

% Post analysis implemented for meristem data

if multiple_regions==0;
    inout.fit_fixed_exp=1;    
    if inout.fit_fixed_exp==1 && inout.post_analysis==1
        [dirdata]=postanalysis_regions(inout);
    end
end

% Exporting to features to a csv file
csv_file=fullfile(datapath,'dirdata_features.csv');
struct2csv(dirdata.features,csv_file);


end


end

function [inout]=quickdefault_inout

[inout.codefolder,inout.datafolder]=check_computer;

inout.numchannels=1;
inout.standard_format_filename=0;
inout.timecourse=0;

inout.export_data=1;
inout.make_movie=0;
inout.select_region=1;
inout.analysis_regions=0;

end
