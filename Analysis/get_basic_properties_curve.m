function [data]=get_basic_properties_curve(inout,data,numregion)
regionmask_Pixels=data.region{numregion}.regionmask;
% figure();imshow(regionmask_Pixels)
[pos_curve_y,pos_curve_x]=find(regionmask_Pixels);
% figure();plot(pos_curve_x,pos_curve_y)
if length(pos_curve_x)-length(unique(pos_curve_x))>length(pos_curve_y)-length(unique(pos_curve_y)) %Check proper X/Y assignation
    [pos_curve_y,pos_curve_x]=find(regionmask_Pixels.');    
end
% figure();plot(pos_curve_x,pos_curve_y)
Mean=mean(pos_curve_y);
if and(abs(Mean)<abs(pos_curve_y(end)),abs(Mean)<abs(pos_curve_y(1)))    %Check parabola orientation 
    pos_curve_y=-pos_curve_y;
    pos_curve_x=-pos_curve_x;
end
% figure();plot(pos_curve_x,pos_curve_y)

[data]=make_parabola_profile(inout,data,numregion,pos_curve_x,pos_curve_y);

end
