function [data]=find_central_domain(inout,data)

% Note we resize the orthos sum and max intensity projections in the xy
% direction, so that the zresolution is the same in all directions.

if isfield(inout,'outsu_threshold_prefactor')
    data.outsu_threshold_prefactor=inout.outsu_threshold_prefactor;
else
    data.outsu_threshold_prefactor=1;
end 

if isfield(inout,'selected_channel_string')
    strng=[data.rootstring_first_image_name '_' inout.selected_channel_string];
else
    strng=data.rootstring_first_image_name;
end

resolution=data.resolution(2); %The second position always defines the proper resolution: In inout.work_on_orthos==1 is z and else is xy.

finalsigma=inout.gauss_sigma_microns/resolution ;

% if isfield(data,'zsum')
%     imsum=data.zsum;
%     data.xy_centroid=size(data.zsum)/2;
% else
%     dim=ndims(data.selected_working_image);
%     if dim==2
%         disp('domain pipeline applied to 2D - instead of Sum projection')
%         imsum=data.selected_working_image;
%         data.xy_centroid=size(imsum)/2;
%     end
% end

%imsum=imgaussfilt(imsum,finalsigma);
%level_sum = graythresh(imsum);

%name=strcat('zsum_bw_',data.rootstring_first_image_name,'.tiff');
%outfilename=fullfile(inout.out_tiffs_xy_domain_processed_path,name);
%BW = im2bw(imsum,level_sum*inout.outsu_threshold_prefactor);
%imwrite(BW,outfilename);

if isfield(data,'zmax')
    immax=data.zmax;
    data.xy_centroid=size(immax)/2;
else
    dim=ndims(data.selected_working_image);
    if dim==2
        disp('domain pipeline applied to 2D - instead of Maximal intensity projection')
        immax=data.selected_working_image;
        data.xy_centroid=size(immax)/2;
    end
    auxzmax=0; % introducing this variable (0 is irrelevant) for generating the correct string of the output file
end

immax=imgaussfilt(immax,finalsigma);
if isfield(inout,'num_thresholds')
    data.num_thresholds=inout.num_thresholds;
    mlevel_zmax = multithresh(immax,inout.num_thresholds);
else
    data.num_thresholds=1;
    mlevel_zmax = multithresh(immax);    
end

if exist('auxzmax')
    name=strcat('bw_',strng,'.tiff');
else
    name=strcat('zmax_bw_',strng,'.tiff');
end

outfilename=fullfile(inout.out_tiffs_xy_domain_processed_path,name);
mlevels = imquantize(immax,mlevel_zmax*data.outsu_threshold_prefactor);
 
figure(1)
imshow(mlevels,[])
imwrite(mat2gray(mlevels),outfilename);
 
mlevels(mlevels<max(max(mlevels)))=0;
BW=logical(mlevels);
 L1= bwlabel(BW);

figure(2)
imshow(L1)

%%Control Old Threshholding
% clevel_zmax = multithresh(immax);    
% olevel_zmax = graythresh(mat2gray(immax)); 
% clevels = imquantize(immax,clevel_zmax);
% figure(3)
% imshow(clevels,[])
% figure(4)
% oBW = imbinarize(mat2gray(immax),olevel_zmax);
% oL1= bwlabel(oBW);
% imshow(oL1)

image_size=size(immax);

r=regionprops(BW,'Area','Centroid','MajorAxisLength', ...
    'MinorAxisLength','Circularity');
for i=1:length(r) %%data.xy_centroid is inverted because the order of order of intrinsic coordinate (3.0,5.0) is reversed relative to pixel indices (5,3).
    r(i).Distcenter=sqrt((data.xy_centroid(2)-r(i).Centroid(1))^2.0+...
        (data.xy_centroid(1)-r(i).Centroid(2))^2.0);
end

[x_cm, y_cm]=find_CM_central_ROIs(r,image_size);
[r]=get_dist_cm(r,x_cm,y_cm);

% Finding index of the biggest domain (previously: closest domain to the center of the image)
if length(r)>1    
    [mn, Idx_min]=min([r.DistCM]);
%     [mn Idx_min]=min([r.Distcenter]);
    [mx, Idx_max]=max([r.Area]);
    if Idx_min~=Idx_max
        disp('Discrepancy Area/Location !!!')
        text=['For more safety check image: ', fullfile(inout.out_tiffs_xy_domain_processed_path,strcat('zmax_seg_elipse_',strng,'.jpeg'))];
        disp(text)
        mini_r=r;
        mini_r(Idx_max).Area=0;
        [mn Idx_max]=max([mini_r.Area]);
        if or(r(Idx_min).DistCM>r(Idx_max).DistCM,mn<0.5*mx)
            Idx_min=Idx_max;
        end     
    end
else
    Idx_min=1;
end

% Storing the Major and Minor axis of such domain
data.central_domain.MajorAxisLength_in_um=(r(Idx_min).MajorAxisLength)*resolution;
data.central_domain.MinorAxisLength_in_um=(r(Idx_min).MinorAxisLength)*resolution;
data.central_domain.aspect_ratio=data.central_domain.MinorAxisLength_in_um/data.central_domain.MajorAxisLength_in_um;

radius_central_domain=(data.central_domain.MinorAxisLength_in_um+data.central_domain.MinorAxisLength_in_um)/4;
data.central_domain.radius_in_um=radius_central_domain;
data.central_domain.centroid=(r(Idx_min).Centroid);
data.central_domain.area_in_um2=((r(Idx_min).Area))*(resolution)^2;
data.central_domain.circularity=(r(Idx_min).Circularity);
% 'Erasing' the other domains
L2=(L1 == Idx_min);

%fpts= find([r.Area]<400); 
figure(1)
imshow(L1)
%for i= 1:length(fpts)
%    L1(L1 == fpts(i))= 0;
%end;
%L1= renumberimage(L1);

h=figure(2);
imshow(L2);
s=regionprops(L2, 'Orientation', 'MajorAxisLength', ...
    'MinorAxisLength', 'Eccentricity', 'Centroid');

% Drawing the elipse
t = linspace(0,2*pi,50);
hold on
for k = 1:length(s)
    a = s(k).MajorAxisLength/2;
    b = s(k).MinorAxisLength/2;
    Xc = s(k).Centroid(1);
    Yc = s(k).Centroid(2);
    phi = deg2rad(-s(k).Orientation);
    x = Xc + a*cos(t)*cos(phi) - b*sin(t)*sin(phi);
    y = Yc + a*cos(t)*sin(phi) + b*sin(t)*cos(phi);
    plot(x,y,'r','Linewidth',2)
end
hold off

if exist('auxzmax')
    name=strcat('central_seg_elipse_',strng,'.jpeg');
else
    name=strcat('central_zmax_seg_elipse_',strng,'.jpeg');
end

outfilename=fullfile(inout.out_tiffs_xy_domain_processed_path,name);
saveas(h,outfilename)

data.central_domain.max_domain_mask=L2;
% imshow(data.central_domain.max_domain_mask)

i=figure(3);
imshow(data.zmax,[]);
% Drawing the elipse
t = linspace(0,2*pi,50);
hold on
for k = 1:length(s)
    a = s(k).MajorAxisLength/2;
    b = s(k).MinorAxisLength/2;
    Xc = s(k).Centroid(1);
    Yc = s(k).Centroid(2);
    phi = deg2rad(-s(k).Orientation);
    x = Xc + a*cos(t)*cos(phi) - b*sin(t)*sin(phi);
    y = Yc + a*cos(t)*sin(phi) + b*sin(t)*cos(phi);
    plot(x,y,'r','Linewidth',2)
end
hold off

if exist('auxzmax')
    name=strcat('seg_elipse_',strng,'.jpeg');
else
    name=strcat('zmax_seg_elipse_',strng,'.jpeg');
end

outfilename=fullfile(inout.out_tiffs_xy_domain_processed_path,name);
saveas(i,outfilename)

end
