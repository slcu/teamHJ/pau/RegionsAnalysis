function [data]=crop_images(inout,data)

if inout.timecourse==1
    disp(['Cropping throughout the time-course, given by the cropping at the first time point.'])

    figure();imshow(data.fluor_marker_timecourse(:,:,:,1))
    disp(['Please, determine the top left corner of the cropping rectangle.'])

    [y0,x0] = ginputc(1,'Color', 'w', 'LineWidth', 2);

    disp(['Now, determine the right vertical edge location of the rectangle.'])

    [y1,x1] =ginputc(1,'Color', 'w', 'LineWidth', 2);
    point1=[x1,y1];
    disp(['Finally, determine the rectangle bottom.'])
    [y2,x2] = ginputc(1,'Color', 'w', 'LineWidth', 2);

    %BW = roipoly(I,xi,yi) fluor_marker_timecourse
    figure();imshow(mat2gray(data.fluor_marker_timecourse(x0:x2,y0:y1,1,1)));
    for j=1:size(data.fluor_marker_timecourse,4)
        aux_marker(:,:,1,j)=data.fluor_marker_timecourse(x0:x2,y0:y1,:,j);
        aux_signal(:,:,1,j)=data.fluor_signal_timecourse(x0:x2,y0:y1,:,j);
    end
    
    data.fluor_marker_timecourse=aux_marker;
    data.fluor_signal_timecourse=aux_signal;
        
else if inout.timecourse==0
    figure();imshow(data.fluor_marker_timecourse(:,:))
    disp(['Please, determine the top left corner of the cropping rectangle.'])

    [y0,x0] = ginputc(1,'Color', 'w', 'LineWidth', 2);

    disp(['Now, determine the right vertical edge location of the rectangle.'])

    [y1,x1] =ginputc(1,'Color', 'w', 'LineWidth', 2);
    point1=[x1,y1];
    disp(['Finally, determine the rectangle bottom.'])
    [y2,x2] = ginputc(1,'Color', 'w', 'LineWidth', 2);

    %BW = roipoly(I,xi,yi) fluor_marker_timecourse
    figure();imshow(mat2gray(data.fluor_marker_timecourse(x0:x2,y0:y1)));

    data.fluor_marker_timecourse=data.fluor_marker_timecourse(x0:x2,y0:y1);
    if inout.number_tiff_channels==2;
        data.fluor_signal_timecourse=data.fluor_signal_timecourse(x0:x2,y0:y1);
    end
end

end
