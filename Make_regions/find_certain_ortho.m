function [X]=find_certain_ortho(inout,cellimage)

    zsumaux=histeq(mat2gray(max(cellimage,[],3))); 
    
%     figure();
%     imshow(zsumaux);

    a=sum(zsumaux(1,:))/length(zsumaux(1,:));
    b=sum(zsumaux(end,:))/length(zsumaux(end,:));
    c=sum(zsumaux(:,1))/length(zsumaux(:,1));
    d=sum(zsumaux(:,end))/length(zsumaux(:,end));
    [minval,pos]=min([a,b,c,d]);

if isfield(inout,'otherOrtho')
    if inout.otherOrtho==0
        if or(pos==1,pos==2)
            X=[2 3 1];
        else
            X=[3 1 2];
        end 
    else
        if or(pos==3,pos==4)
            X=[2 3 1];
        else
            X=[3 1 2];
        end
    end
else
    if or(pos==1,pos==2)
        X=[2 3 1];
    else
        X=[3 1 2];
    end
end
     
