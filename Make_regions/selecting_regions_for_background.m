function [data] = selecting_regions_for_background(inout,data,image,f3)
% Selecting_regions

data.backgroundregionlisttemp=[];

if inout.ask_numberbackgroundregions==1
    inout.defaultnumberregions=1;
    prompt = {'Enter the number of background regions you want to select '};
    dlg_title = 'Number of background regions';
    num_lines = 1;
    defaultanswer = {num2str(inout.defaultnumberbackgroundregions)};
    options.WindowStyle='normal';
    options.Interpreter='tex';
    answer = inputdlg(prompt, dlg_title, num_lines, defaultanswer, options);
    number_of_regions=str2num(answer{1});
else
    number_of_regions=inout.defaultnumberbackgroundregions;
end

if and(isfield(data,'backgroundregionlist'),~isempty(data.backgroundregionlist))
    index=max(data.backgroundregionlist)+1;
else
    index=1;
end

for i=1:number_of_regions
[data]=select_region_used_for_background(inout,data,image,f3,index);
index=index+1;
image=data.image_with_background_regions;

end


