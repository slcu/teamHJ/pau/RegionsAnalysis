function [data]=process_orthos(inout,data)

% gaussian filter, otsu thresholding, and binarising

if isfield(inout,'selected_channel_string')
    strng=[data.rootstring_first_image_name '_' inout.selected_channel_string];
else
    strng=data.rootstring_first_image_name;
end

label={'xz_','yz_'};

for ii=1:2   
% imsum=data.ortho_planes{ii}.sum;
immax=data.ortho_planes{ii}.max;
% imshow(immax);

[DX,DY]=size(immax);
if and(DX==data.xys_image,DY==data.xys_image)
    resolution=data.microns_per_pixel;
else
    resolution=data.microns_per_pixel_orthos;
end
pic_centroid=[DX/2, DY/2];  
finalsigma=inout.gauss_sigma_microns/resolution;
    
% imsum=imgaussfilt(imsum,finalsigma);
%data.ortho_planes{ii}.level_sum = graythresh(imsum);

immax=imgaussfilt(immax,finalsigma);
if isfield(inout,'num_thresholds')
    data.ortho_planes{ii}.level_max = multithresh(immax,inout.num_thresholds);
else
    data.ortho_planes{ii}.level_max = multithresh(immax);    
end 
 
name=strcat(strng,'_ortho_max_bw_',label{ii},'.tiff');
outfilename=fullfile(inout.out_tiffs_orthos_processed_path,name);
mlevels = imquantize(immax,data.ortho_planes{ii}.level_max);

figure(1)
imshow(mat2gray(mlevels))
imwrite(mat2gray(mlevels),outfilename);

mlevels(mlevels<max(max(mlevels)))=0;
BW=logical(mlevels);
L1= bwlabel(BW);

figure(2)
imshow(L1)

%%Control Old Threshholding
% clevel_zmax = multithresh(immax);   
% olevel_zmax = graythresh(mat2gray(immax));   
% clevels = imquantize(immax,clevel_zmax);
% figure(3)
% imshow(clevels,[])
% figure(4)
% oBW = imbinarize(mat2gray(immax),olevel_zmax);
% oL1= bwlabel(oBW);
% imshow(oL1)

image_size=size(immax);

r=regionprops(BW,'Area','Centroid','MajorAxisLength', ...
    'MinorAxisLength','Circularity','Orientation');

if ~isfield(data,'orientation')
    data.orientation='None';
end
    
for i=1:length(r) %%pic_centroid is inverted because the order of order of intrinsic coordinate (3.0,5.0) is reversed relative to pixel indices (5,3).
    r(i).Distcenter=sqrt((pic_centroid(2)-r(i).Centroid(1))^2.0+...
        (pic_centroid(1)-r(i).Centroid(2))^2.0);
    
    if strcmp(data.orientation,'Vertical')
        r(i).Distaxis=sqrt((pic_centroid(2)-r(i).Centroid(1))^2.0);
    elseif strcmp(data.orientation,'Horizontal')
        r(i).Distaxis=sqrt((pic_centroid(1)-r(i).Centroid(2))^2.0);
    end
end

% [x_cm,y_cm]=find_CM_central_ROIs(r,image_size);
% if strcmp(data.orientation,'Vertical')
%     y_cm=0;
% elseif strcmp(data.orientation,'Horizontal')
%     x_cm=0;
% end
% [r]=get_dist_cm(r,x_cm,y_cm);

% Finding index of the closest domain to the center of the image axis
if length(r)>1  
    if isfield(r,'Distaxis')
        [mn Idx_min]=min([r.Distaxis]);
    else
        [mn Idx_min]=min([r.Distcenter]);
    end
    [mx Idx_max]=max([r.Area]);
    if Idx_min~=Idx_max
        disp('Discrepancy Area/Location in ortho !!!')
        text=['For more safety check image: ', fullfile(inout.out_tiffs_xy_domain_processed_path,strcat('seg_elipse_',strng,'_ortho_max_',label{ii},'.jpeg'))];
        disp(text)
        if isfield(r,'Distaxis')
            [mn Idx_min]=min([r.Distaxis]);
        else
            mini_r=r;
            mini_r(Idx_max).Area=0;
            [mn Idx_min]=max([mini_r.Area]);
            if or(r(Idx_min).Distcenter>r(Idx_max).Distcenter,mn<0.5*mx)
                Idx_min=Idx_max;
            end 
        end
    end
else
    Idx_min=1;  
end

% Storing the Major and Minor axis of such domain
if or(and(strcmp(data.orientation,'Vertical'),abs(abs(r(Idx_min).Orientation)-90)>45),...
      and(strcmp(data.orientation,'Horizontal'),abs(abs(r(Idx_min).Orientation)-90)<45))
    data.ortho_planes{ii}.height_in_um=(r(Idx_min).MinorAxisLength)*resolution;
    data.ortho_planes{ii}.width_in_um=(r(Idx_min).MajorAxisLength)*resolution;
else
    data.ortho_planes{ii}.height_in_um=(r(Idx_min).MajorAxisLength)*resolution;
    data.ortho_planes{ii}.width_in_um=(r(Idx_min).MinorAxisLength)*resolution;
end
data.ortho_planes{ii}.aspect_ratio=((r(Idx_min).MinorAxisLength)*resolution)/((r(Idx_min).MajorAxisLength)*resolution);
data.ortho_planes{ii}.area_in_um2=((r(Idx_min).Area))*(resolution)^2;
data.ortho_planes{ii}.circularity=(r(Idx_min).Circularity);

% 'Erasing' the other domains
L1(L1 ~= Idx_min)= 0;

%fpts= find([r.Area]<400); 
%figure(1)
%imshow(L1)
%for i= 1:length(fpts)
%    L1(L1 == fpts(i))= 0;
%end;
%L1= renumberimage(L1);

h=figure(2);
imshow(L1);
s=regionprops(L1, 'Orientation', 'MajorAxisLength', ...
    'MinorAxisLength', 'Eccentricity', 'Centroid','Area');

% Drawing the elipse
t = linspace(0,2*pi,50);
hold on
for k = 1:length(s)
    a = s(k).MajorAxisLength/2;
    b = s(k).MinorAxisLength/2;
    Xc = s(k).Centroid(1);
    Yc = s(k).Centroid(2);
    phi = deg2rad(-s(k).Orientation);
    x = Xc + a*cos(t)*cos(phi) - b*sin(t)*sin(phi);
    y = Yc + a*cos(t)*sin(phi) + b*sin(t)*cos(phi);
    plot(x,y,'r','Linewidth',2)
end
hold off

name=strcat('ortho_max_seg_elipse_',strng,'_ortho_max_',label{ii},'.jpeg');
outfilename=fullfile(inout.out_tiffs_orthos_processed_path,name);
saveas(h,outfilename)

% name=strcat('ortho_max_seg_',strng,'_ortho_max_',label{ii},'.jpeg');
% outfilename=fullfile(inout.out_tiffs_orthos_processed_path,name);
% imwrite(L1,outfilename);

data.ortho_max_domain_mask{ii}=L1;
% imshow(data.ortho_max_domain_mask{ii})

i=figure(3);

imshow(data.ortho_planes{ii}.max,[]);
% Drawing the elipse
t = linspace(0,2*pi,50);
hold on
for k = 1:length(s)
    a = s(k).MajorAxisLength/2;
    b = s(k).MinorAxisLength/2;
    Xc = s(k).Centroid(1);
    Yc = s(k).Centroid(2);
    phi = deg2rad(-s(k).Orientation);
    x = Xc + a*cos(t)*cos(phi) - b*sin(t)*sin(phi);
    y = Yc + a*cos(t)*sin(phi) + b*sin(t)*cos(phi);
    plot(x,y,'r','Linewidth',2)
end
hold off

name=strcat('seg_elipse_',strng,'_ortho_max_',label{ii},'.jpeg');
outfilename=fullfile(inout.out_tiffs_orthos_processed_path,name);
saveas(i,outfilename)

%{ 
watershed attempt
D = bwdist(~BW);
D = -D;
D(~BW) = -Inf;
L = watershed(D);
rgb = label2rgb(L,'jet',[.5 .5 .5]);
figure
%imshow(rgb,'InitialMagnification','fit')
name=strcat('ortho_max_seg_',label{ii},data.rootstring_first_image_name,'_ortho_sum_',label{ii},'.jpeg');;
outfilename=fullfile(inout.out_tiffs_orthos_processed_path,name);
%}

%thresholding raw trial
%{

data.ortho_planes{ii}.level_sum = graythresh(data.ortho_planes{ii}.sum)
data.ortho_planes{ii}.level_max = graythresh(data.ortho_planes{ii}.max)


name=strcat('ortho_sum_bw_',label{ii},data.rootstring_first_image_name,'_ortho_sum_',label{ii},'.tiff');
outfilename=fullfile(inout.out_tiffs_orthos_path,name);
BW = im2bw(data.ortho_planes{ii}.sum,data.ortho_planes{ii}.level_sum);
imwrite(BW,outfilename);

name=strcat('ortho_max_bw_',label{ii},data.rootstring_first_image_name,'_ortho_sum_',label{ii},'.tiff');
outfilename=fullfile(inout.out_tiffs_orthos_path,name);
BW = im2bw(data.ortho_planes{ii}.max,data.ortho_planes{ii}.level_max);
imwrite(BW,outfilename);
%}
close all;
end
end
