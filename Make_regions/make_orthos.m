function [data]=make_orthos(inout,data)


if isfield(inout,'selected_channel_string')
    strng=[data.rootstring_first_image_name '_' inout.selected_channel_string];
else
    strng=data.rootstring_first_image_name;
end

% We are homogenizing the resolution to the best one
xresolution=data.microns_per_pixel;
zresolution=data.microns_per_pixel_orthos;
if data.microns_per_pixel==data.microns_per_pixel_orthos
    if inout.work_on_orthos==1
        imagesize=data.image_size;
    else
        imagesize=[data.xys_image data.zs_image];        
    end
elseif data.microns_per_pixel>data.microns_per_pixel_orthos  
    newlength=data.xys_image*xresolution/zresolution;
    imagesize=[newlength data.zs_image];
else
    newlength=data.zs_image*zresolution/xresolution;
    imagesize=[data.xys_image newlength];
end

if isfield(inout,'ortho_thickness_in_microns')
    origin=data.region{1}.origin;

    x0=origin(1,1);
    y0=origin(1,2);

    width=inout.ortho_thickness_in_microns/(zresolution);
    data.ortho_planes{1}.selection=data.ortho{1}(:,:,round(x0-width/2):round(x0+width/2));
    data.ortho_planes{2}.selection=data.ortho{2}(:,:,round(y0-width/2):round(y0+width/2));
else
    data.ortho_planes{1}.selection=data.ortho{1}(:,:,:);
    data.ortho_planes{2}.selection=data.ortho{2}(:,:,:);
end

label={'ys_','xs_'};

for ii=1:2
    
    psum=sum(data.ortho_planes{ii}.selection,3);
    pmax=max(data.ortho_planes{ii}.selection,[],3);
    
    [DX,DY]=size(pmax);
    if ~and(DX==data.xys_image,DY==data.xys_image)
        psum=imresize(psum, imagesize);
        pmax=imresize(pmax, imagesize);
    end
    
    data.ortho_planes{ii}.sum=psum;    
    data.ortho_planes{ii}.max=pmax;

    % saving images
    name=strcat(strng,'_ortho_sum_',label{ii},'.tiff');
    outfilename=fullfile(inout.out_tiffs_orthos_sum_path,name);
    imwrite(data.ortho_planes{ii}.sum,outfilename);

    name=strcat(strng,'_ortho_max_',label{ii},'.tiff');
    outfilename=fullfile(inout.out_tiffs_orthos_max_path,name);
    imwrite(mat2gray(data.ortho_planes{ii}.max),outfilename);

    data.ortho_planes{ii}=rmfield(data.ortho_planes{ii},'selection');

end

data=rmfield(data,'ortho');

end
