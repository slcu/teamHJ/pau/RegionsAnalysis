function [x_cm y_cm]=find_CM_central_ROIs(r,image_size)

% finding center of mass of centroids that are in  a central region of the
% image

x_cm=0;y_cm=0;
TotalMass=0;
 for i=1:length(r)
    if r(i).Distcenter<(min(image_size)*0.95/2) % if primordia is in a central area, use primordia for computing CM
        x_cm=x_cm+r(i).Centroid(1)*r(i).Area;
        y_cm=y_cm+r(i).Centroid(2)*r(i).Area;
        TotalMass=TotalMass+r(i).Area;
     end
 end

if TotalMass~=0
    x_cm=x_cm/TotalMass;
    y_cm=y_cm/TotalMass;
else
    x_cm=image_size(2)/2;
    y_cm=image_size(1)/2;
end

end
