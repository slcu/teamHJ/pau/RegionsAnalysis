function add_regions_and_resume_analysis(datapath)

% This function imports inout and the data structure that was in process
% (in the inout.outdatapath), add new regions, and runs again
% RegionAnalysis pipeline, loading the full set of regions 

% note that it will work just if the inout.save_while_selecting_regions was
% previously set to 1, to properly save the data and inout structures

if(nargin < 1)
datapath = uigetdir('/','Select the data set location');
end

load(fullfile(datapath,'inout.mat'));

[data]=import_data(inout.outdatapath,inout.data_structure_filename);

[data]=add_regions_to_study(inout,data);
save(fullfile(data.datapath,inout.data_structure_filename),'data');

inout.get_polygon_from_previous_data=1;
RegionsAnalysis(inout);

end