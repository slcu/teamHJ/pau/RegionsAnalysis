function [data]=find_radial_distance(inout,data,imcell,imsig2,imsig3)
% Evaluate Meristem size by locating the 1st primordia with the top
% projection

if inout.work_on_orthos==0    
    [X]=find_certain_ortho(inout,imcell);   
    imcell=permute(imcell,X);
    imsig2=permute(imsig2,X);
    imsig3=permute(imsig3,X);
end

HomoRSize=size(imcell);  
if isfield(inout,'homogeneous_selection')             
    if inout.homogeneous_selection==0
        if data.original_resolution(1,1)>min(data.original_resolution) 
            HomoRSize(1)=HomoRSize(1)*data.original_resolution(1,1)/min(data.original_resolution);     
        end            
        if data.original_resolution(1,2)>min(data.original_resolution) 
            HomoRSize(2)=HomoRSize(2)*data.original_resolution(1,2)/min(data.original_resolution);
        end            
        if data.original_resolution(1,3)>min(data.original_resolution) 
            HomoRSize(3)=HomoRSize(3)*data.original_resolution(1,3)/min(data.original_resolution);
        end
        resolution=min(data.original_resolution);
    end
end
Grey_imcell=mat2gray(imcell);
Grey_imcell=imresize3(Grey_imcell, HomoRSize);
Grey_imsig2=imadd(mat2gray(imcell),mat2gray(imsig2));
Grey_imsig2=imresize3(Grey_imsig2, HomoRSize);
Grey_imsig3=imadd(mat2gray(imcell),mat2gray(imsig3));  
Grey_imsig3=imresize3(Grey_imsig3, HomoRSize);

auxrgb={Grey_imsig3,Grey_imsig2,Grey_imcell};                
imacol=cat(4,auxrgb{:});

% Select the time interval you wish to project
figure('Position',[800 2000 1080 1080])
imshow3Dfull(imacol);
disp('Select the section to project to obtain the meristem radius');
prompt = {'First z:','Last z:'};
dlg_title = 'Select z';
num_lines = 1;

if inout.work_on_orthos==1
    defaultanswer = {num2str(floor(0.03*data.zs_image)),num2str(floor(0.2*data.zs_image ))};                         
else
    defaultanswer = {num2str(floor(0.03*data.xys_image)),num2str(floor(0.2*data.xys_image))};   
end
options.WindowStyle='normal';
options.Interpreter='tex';
answer = inputdlg(prompt, dlg_title, num_lines, defaultanswer, options);
close all;
imcell=imcell(:,:,str2num(answer{1}):str2num(answer{2}));        
im3=max(imcell,[],3);
imsig2=imsig2(:,:,str2num(answer{1}):str2num(answer{2}));      
im2=max(imsig2,[],3);
imsig3=imsig3(:,:,str2num(answer{1}):str2num(answer{2}));      
im1=max(imsig3,[],3);
Grey_imcell=mat2gray(im3);
Grey_imsig2=imadd(mat2gray(im3),mat2gray(im2));
Grey_imsig3=imadd(mat2gray(im3),mat2gray(im1));  
auxrgb={Grey_imsig3,Grey_imsig2,Grey_imcell};                
imacol=cat(3,auxrgb{:});
    
figure(1); 
% imshow(histeq(imacol)); 
imshow(imacol);   
disp(['Please select the meristem central point.'])

[x0,y0] =ginputc(1,'Color', 'w', 'LineWidth', 2);
center=[x0,y0];

disp(['Please select the border with the first primordia.'])
[x1,y1] =ginputc(1,'Color', 'w', 'LineWidth', 2);
edge=[x1,y1];
radius=sqrt((x1-x0)^2+(y1-y0)^2);

circlemark=make_circlemark(radius,x0,y0,imacol);
auxrgb={max(Grey_imsig3,circlemark),max(Grey_imsig2,circlemark),max(Grey_imcell,circlemark)};                
Nimacol=cat(3,auxrgb{:});
close all;
h=figure(2);
imshow(Nimacol); 

strng=data.rootstring_first_image_name
name=strcat(strng,'_top','.jpeg');
outfilename=fullfile(inout.out_selected_regions_path,name);
saveas(h,outfilename)
namefig=strcat(strng,'_top','.fig');
outfilenamefig=fullfile(inout.out_selected_regions_path,'fig_format',namefig);
savefig(outfilenamefig)

data.meristem_radious_in_um=radius*resolution;








