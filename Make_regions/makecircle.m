function [circlePixels]=makecircle(radius,x0,y0,imagesize);

imagesize_x = imagesize(2);
imagesize_y = imagesize(1);

[x y] = meshgrid(1:imagesize_x, 1:imagesize_y);

circlePixels = (y - y0).^2 + (x - x0).^2 <= radius.^2;