function [r]=get_dist_cm(r,x_cm,y_cm);

for i=1:length(r)
r(i).DistCM=sqrt((x_cm-r(i).Centroid(1))^2.0+...
    (y_cm-r(i).Centroid(2))^2.0);
end

end
