function [data,saveregion]=select_region_to_study(inout,data,image,fanalysis,index)
% select_region_to_study enables us to tell the region of the image to
% study around the sending cell.
% Creating the directory, if necessary.

%%% Adapted to work also in Ortho and double parabola

if inout.ask_labelregions==1
    prompt = {'Enter the label of the data output for the next area selection '};
    dlg_title = 'Input image parameters';
    num_lines = 2;
    defaultanswer = {int2str(index)};
    options.WindowStyle='normal';
    options.Interpreter='tex';
    answer = inputdlg(prompt, dlg_title, num_lines, defaultanswer, options);
    regionnumber=str2num(answer{1});
else
    regionnumber=index;
end


switch inout.color_regions
    case 'blue'
        channelregion=3;
    case 'green'
        channelregion=2;        
    otherwise 
        channelregion=1;
end

if ndims(image)==2
    channelregion=1;
end

regionlabel=num2str(regionnumber);
diroutdata=strcat('region_',regionlabel);
fulldiroutdata=fullfile(data.outdatapath,diroutdata) ;

if(regionnumber<=inout.defaultnumberregions)
    ex=exist(fulldiroutdata);
    if ex==0
        mkdir(data.outdatapath,diroutdata);
    end
end


h=figure(fanalysis);
%
if inout.click_selecting_regions==1
    if inout.center_click==1
        disp('Please select the first point for your region.')
        [x0,y0] = ginputc(1,'Color', 'w', 'LineWidth', 2);
        origin=[x0,y0]; 
    end    
else
    if inout.get_center_and_radius_from_previous_data==1
        origin=inout.tmp_region_pars{index}.pos0;
        x0=origin(1,1);
        y0=origin(1,2);
    end
    
    if inout.get_center_from_cental_domain==1
        x0=data.central_domain.centroid(1);
        y0=data.central_domain.centroid(2);
        origin=[x0, y0];
    end
    
    if inout.get_centroids_from_found_domains==1
        x0=data.domain{index}.centroid(1);
        y0=data.domain{index}.centroid(2);
        origin=[x0, y0];
        data.region{regionnumber}.distcenter_in_um=data.domain{index}.distcenter_in_um;
    end
    
    if inout.get_radius_from_found_domains==1 
        radius0=inout.radius_prefactor*data.domain{index}.majoraxislength/2.0;
        if inout.work_on_orthos==1
            radius1=inout.minimal_radius_region_in_microns/data.microns_per_pixel_orthos;
        else 
            radius1=inout.minimal_radius_region_in_microns/data.microns_per_pixel;
        end
        radius=max(radius0,radius1);
    end

    if isfield(inout,'get_polygon_from_previous_data')
        if inout.get_polygon_from_previous_data==1
            data.region{regionnumber}.regionmask=inout.tmp_region_pars{index}.regionmask;
            
            if isfield(inout.tmp_region_pars{regionnumber},'class') % for class inheritance
                data.region{regionnumber}.class=inout.tmp_region_pars{regionnumber}.class;
            end
            switch inout.defaultregiontype
                case 'Rectangle' % I put this restriction here of being a rectangle just in case, because polygon pipelines have their own orgin saving method
                    origin=inout.tmp_region_pars{index}.pos0;
                    x0=origin(1,1);
                    y0=origin(1,2);
                    point1=inout.tmp_region_pars{index}.pos1;
                    point2=inout.tmp_region_pars{index}.pos2;
            end            
        end
    end    
end

if isfield(inout,'defaultregiontype')
    button=inout.defaultregiontype;
else    
    button = questdlg('Which kind of region do you want?','Region selection','Circle','Rectangle','Other','Circle'); 
end


% in case we are drawing a circle, check if we impose the radius
lb=size(button,2);
stringtocompare='Circle';
maxlenghth_to_compare=min(lb,6);
checkbutton=mean(button(1:maxlenghth_to_compare)==stringtocompare(1:maxlenghth_to_compare));

if and(inout.click_selecting_regions==1,inout.ispolygon==0)
    if inout.click_selecting_regions==1
        if and(checkbutton,isfield(inout,'radius_region_in_microns'))
            if inout.get_radius_from_found_domains==0
                if inout.work_on_orthos==1
                    radius=inout.radius_region_in_microns/data.microns_per_pixel_orthos;
                else 
                    radius=inout.radius_region_in_microns/data.microns_per_pixel;
                end 
            end
        else  
            disp('Please select the second point for your region.')

            [x1,y1] =ginputc(1,'Color', 'w', 'LineWidth', 2);
            point1=[x1,y1];
            radius=sqrt((x1-x0)^2+(y1-y0)^2);
        end
    end
else
    if inout.get_center_and_radius_from_previous_data==1 
        radius=inout.tmp_region_pars{index}.radius;
    end
    if and(or(inout.get_center_from_cental_domain==1,inout.get_centroids_from_found_domains==1),inout.get_radius_from_found_domains==0)
        if inout.work_on_orthos==1
            radius=inout.radius_region_in_microns/data.microns_per_pixel_orthos;
        else 
            radius=inout.radius_region_in_microns/data.microns_per_pixel;
        end 
    end          
end

switch button 
    case 'Circle'
        mx=max(max(double(image(:,:,3)))); % getting the maxima of channel 3, such that the circlemark can be visible afterwards
        circlemark=mx*make_circlemark(radius,x0,y0,image(:,:,3));
        imaux=image(:,:,3);
        image(:,:,3)=max(double(image(:,:,3)),circlemark);        
        is=imshow(image,'InitialMagnification',inout.magnification);
        
        disp([' Done, full Circle region selected.'])
        frac_angularregion=1 ;
        circlePixels=makecircle(radius,x0,y0,data.image_size);
        imshow(circlePixels)
        data.region{regionnumber}.regionmask=circlePixels;
        data.region{regionnumber}.frac_angularregion=1;
        
        type='Circle';

    case 'Rectangle'
        if and(inout.get_polygon_from_previous_data==0,inout.click_selecting_regions==1)
            linemark=makeline(x0,y0,x1,y0,size(image(:,:,3)));

            imaux=image(:,:,3);

            image(:,:,3)=max(double(image(:,:,3)),linemark);
            is=imshow(image,'InitialMagnification',inout.magnification);   

            disp([' Oks, please select a third point for limiting the rectangle width.'])
            [x2,y2] = ginputc(1,'Color', 'w', 'LineWidth', 2);
            point2=[x2 y2];

            image_size=size(image);
            [rectanglePixels,markrectanglePixels]=makerectangle(x0,y0,x1,y1,x2,y2,data.image_size);
            image(:,:,3)=max(double(image(:,:,3)),markrectanglePixels);

            is=imshow(image,'InitialMagnification',inout.magnification);
            hold on

            data.region{regionnumber}.regionmask=rectanglePixels;
        end
        data.region{regionnumber}.pos1=point1;
        data.region{regionnumber}.pos2=point2;

        type='Rectangle';
        
     case 'Polygon'
        if and(inout.get_polygon_from_previous_data==0,inout.click_selecting_regions==1)
            if inout.make_zoom==0
                if inout.draw_freehand_polygon==1 
                    disp('Draw the region continuously.');
                    ax=get(h,'CurrentAxes');
                    %h=images.roi.Freehand(ax,'Closed',true);
                    switch inout.polygontype
                        case 'opened'  
                            movePic=size(image);
                            if movePic(1)<movePic(2)   
                                movegui('south')
                            else
                                movegui('east') 
                            end
                            h=drawassisted(gca,'Closed',false);
                        case 'closed'
                            %h=drawfreehand;
                            h=drawassisted(gca,'Closed',true);
                    end
                    disp('To confirm the ROI, double click the left mouse button. For deleting the ROI, right click the mouse button and select Delete.');
                    wait(h);
                    if  isvalid(h)
                        [data.region{regionnumber}.regionmask]=createMask(h);
                    end
                else
                    [data.region{regionnumber}.regionmask]=roipoly;
                end
            elseif inout.make_zoom==1
                [regionmask]=make_zoom(inout,image);
                figure(fanalysis)  ;
                [data.region{regionnumber}.regionmask]=regionmask;
            end % make_zoom conditional
            
            regionmask=data.region{regionnumber}.regionmask;
            
            mx=max(max(double(image(:,:,channelregion))));
            image(:,:,channelregion)=max(double(image(:,:,channelregion)),mx*regionmask);

        end % Polygon conditional
        
        is=imshow(image,'InitialMagnification',inout.magnification);
        hold on             
          
        [saveregion]=save_region_check(data,regionnumber);
        if saveregion==1
            stregion=regionprops(data.region{regionnumber}.regionmask,'centroid');
            origin=stregion.Centroid;
            type='Polygon';
        end
     
     case 'Line'                     
        linePixels=makeline(x0,y0,x1,y1,data.image_size);

        imaux=image(:,:,3);

        image(:,:,3)=max(double(image(:,:,3)),linePixels);
        is=imshow(image,'InitialMagnification',inout.magnification);   

        disp(' Done, full line region selected.')

        data.region{regionnumber}.regionmask=linePixels;
        type='Line';  
        
    case 'Tilted rectangle'
        linemark=makeline(x0,y0,x1,y1,size(image(:,:,3)));

        imaux=image(:,:,3);

        image(:,:,3)=max(double(image(:,:,3)),linemark);
        is=imshow(image,'InitialMagnification',inout.magnification);   

        disp([' Oks, please select a third point for limiting the rectangle width.'])
        [x2,y2] = ginputc(1,'Color', 'w', 'LineWidth', 2);
        point2=[x2 y2];

        image_size=size(image);
        [rectanglePixels,markrectanglePixels]=maketiltedrectangle(x0,y0,x1,y1,x2,y2,data.image_size);
        image(:,:,3)=max(double(image(:,:,3)),markrectanglePixels);

        is=imshow(image,'InitialMagnification',inout.magnification);
        hold on

        data.region{regionnumber}.pos2=point2;
        data.region{regionnumber}.regionmask=rectanglePixels;
        type='Tilted rectangle';

    case 'Cake piece'
        circlemark=make_circlemark(radius,x0,y0,image(:,:,3));
        imaux=image(:,:,3);
        image(:,:,3)=max(double(image(:,:,3)),circlemark);        
        is=imshow(image,'InitialMagnification',inout.magnification);


        disp(' Oks, please select a third point for limiting the angular region (Counterclockwise direction).')
        [x2,y2] = ginputc(1,'Color', 'w', 'LineWidth', 2);
        point2=[x2 y2];

        image_size=size(image);
        [circularregionPixels,perimeterregion,frac_angularregion]=make_circular_region(origin,point1,point2,radius,image_size);
        image(:,:,3)=imaux;
        is=imshow(image,'InitialMagnification',inout.magnification);
        hold on
        plot(perimeterregion(:,1),perimeterregion(:,2),'b','LineWidth',2)

        data.region{regionnumber}.pos2=point2;
        data.region{regionnumber}.circularregionPixels=circularregionPixels;
        data.region{regionnumber}.perimeterregion=perimeterregion;
        data.region{regionnumber}.regionmask=circularregionPixels;
        data.region{regionnumber}.frac_angularregion=frac_angularregion;

        type='Cake piece';
end        


hold off

[saveregion]=save_region_check(data,regionnumber);

if saveregion==1
    
    data.region{regionnumber}.label=regionlabel;
    data.regionlist=unique([data.regionlist regionnumber]);
    data.regionlisttemp=unique([data.regionlisttemp regionnumber]);
    data.region{regionnumber}.type=type;
    data.region{regionnumber}.origin=origin;
    if and(isfield(inout,'double_parabola'),regionnumber==inout.defaultnumberregions*2)  
        if inout.double_parabola==1  
            data.image_with_regions_side=image;
        else
            data.image_with_regions=image;
        end
    else
        data.image_with_regions=image;        
    end
    
    if and(inout.ask_regionclass==1,inout.click_selecting_regions==1)
        if inout.class_number_of_digits==1
            disp('Type a one digit number');
            ct=waitforbuttonpress;
            cc=str2num(get(fanalysis,'currentcharacter'));
            data.region{regionnumber}.class=cc;     
        elseif inout.class_number_of_digits==2
            disp('Type a two digits number');
            ct=waitforbuttonpress;
            cc0=get(fanalysis,'currentcharacter');
            ct=waitforbuttonpress;
            cc1=get(fanalysis,'currentcharacter');      
            class_number=str2num([cc0 cc1]);
            data.region{regionnumber}.class=class_number;  
        end

        hnd1=text(origin(1)-10,origin(2),num2str(data.region{regionnumber}.class),'Color',inout.color_labels_while_selection);

    end
end

if inout.ispolygon==0
    if saveregion==1
        data.region{regionnumber}.pos0=[x0 y0];
        if exist('radius')
            data.region{regionnumber}.radius=[radius];
        end
    end
end








