function [data]=find_blobs(inout,data)    
% finding blobs with an otsu thresholding of the zmax projection
if data.zs_image>1
    im_forblobs=data.zmax;
else
    im_forblobs=mat2gray(data.selected_working_image);
end

xresolution=data.resolution(1);

finalsigma=inout.gauss_sigma_microns/xresolution ;

%data.xy_centroid=size(data.zsum)/2;

im_forblobs=imgaussfilt(im_forblobs,finalsigma);

%im_for_blobs=mat2gray(im_forblobs);

level_threshold = graythresh(im_forblobs);

name=strcat('zmax_bw_',data.rootstring_first_image_name,'.tiff');
outfilename=fullfile(inout.out_blobs_path,name);

threshold=level_threshold*inout.outsu_threshold_prefactor;
%threshold=100/max(max(im_forblobs));
BW = im2bw(im_forblobs,threshold);

imwrite(BW,outfilename);

image_size=size(im_forblobs);

BW=imclose(BW,strel('disk',2));
BW = imfill(BW,'holes');
L0 = bwlabel(BW);

watershed=0; % watershed 1 might erode too much the blobs...
if watershed==1
    LX=L0; 
    D = bwdist(~LX);
    D = -D;
    D(~LX) = -Inf;
    Lw = watershed(D);
    rgb = label2rgb(Lw,'jet',[.5 .5 .5]);
    %figure()
    %imshow(rgb,'InitialMagnification','fit')
    Lw(Lw==0)=-1; % zeros are the boundaries, we make them for the moment
    Lw(Lw==1)=0;
    Lw(Lw==-1)=1; % can I make it a different value? eg the value of the cell
    L=double(Lw);
else 
    L=L0;
end

r = regionprops(L,'Area');
flittle = find([r.Area]<inout.min_blob_area);

for i= 1:length(flittle)
% delete cell
L(L == flittle(i))= 0;
end;

h=figure();imshowlabel(transpose(L));h=figure();imshowlabel(transpose(L));


name=strcat('segmented_',data.rootstring_first_image_name,'.tiff');
fulloutfilename=fullfile(inout.out_blobs_path,name);

imwrite(transpose(L),fulloutfilename);

name=strcat('segmented_',data.rootstring_first_image_name);
fulloutfilename=fullfile(inout.out_blobs_path,name);
save2pdf(fulloutfilename,h);


% exporting another structure for schnitzcells

dataforschnitz.Lc=transpose(L);
dataforschnitz.LNsub=transpose(L);

if data.zs_image>1
    dataforschnitz.rreg=transpose(data.zsum);
    dataforschnitz.phsub=transpose(data.zsum);
else
    dataforschnitz.rreg=data.selected_working_image;
    dataforschnitz.phsub=data.selected_working_image;
end

dataforschnitz.rect=[1, 1, size(L,2), size(L,1)];

if isfield(inout,'marker_channel_index')
    if data.zs_image>1
        dataforschnitz.greg=transpose(data.zsummarker);
    else
        dataforschnitz.greg=data.marker_working_image;
    end
    
end

inout.blobs_filename=[data.rootstring_first_image_name '.mat']

if inout.isforschnitz==1
%'schnitz\0000-00-00\snap\segmentation'
%inout.blobs_filename='snapseg001.mat';
data.schnitzoutdatapath=fullfile(inout.datapath,'schnitz','0000-00-00',data.rootstring_first_image_name)
create_dir(data.schnitzoutdatapath);
save(fullfile(data.schnitzoutdatapath,inout.blobs_filename),'-struct','dataforschnitz');

else

save(fullfile(data.outdatapath,inout.blobs_filename),'-struct','dataforschnitz');
end

end