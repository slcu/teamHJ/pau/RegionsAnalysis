function [struct]=remove_empty_structfields(struct)

listfieldnames = fieldnames(struct);

%for i=1:size(listfieldnames,1)
%  field=listfieldnames{i}  
%  if isempty(struct.(field))
%      rmfield(struct,field)
%  end
%end

tf = cellfun(@(c) isempty(struct.(c)), listfieldnames);
struct = rmfield(struct, listfieldnames(tf));