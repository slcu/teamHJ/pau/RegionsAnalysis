
function [codefolder,datafolder]=check_computer

tmp = matlab.desktop.editor.getActive;
currentdir=fileparts(tmp.Filename);

[computer]=check_which_computer(currentdir);


    switch computer
        case 'Pau' 
            codefolder ='C:\MATLAB\RegionsAnalysis';
            datafolder='C:\Users\pau.formosajordan\Data_analysis_temp';
        case 'Benoit' 
            codefolder ='V:\benoit\RegionsAnalysis';
            datafolder='C:\Users\benoit.landrein\Desktop\Data_temp'; 
            
        case 'Pau laptop' 
            codefolder='/Users/pauformosa-jordan/Copy/Code/MATLAB/RegionsAnalysis';
            datafolder='/Users/pauformosa-jordan/Research/Data_temp/';
            datafolder='/Volumes/Pau_Drive/Data/EMM/'
        case 'Pau linux' 
            codefolder='/home/pau/MEGAsync/MATLAB/RegionsAnalysis';
            datafolder='/home/pau/home3/Data';            
        case 'Jose' 
            codefolder='/home/jose/Copy/RegionsAnalysis';
            datafolder='/home/jose/Projects/Single cell meristem';
            
        otherwise
            codefolder=currentdir;
            datafolder=' ';
    end
        
    addpath(genpath(codefolder));
end

function [computer_str]=check_which_computer(currentdir)
    len_currdir=size(currentdir,2);
    
    %%%%
    diri='U:\MATLAB\';
    len=min(len_currdir,size(diri,2));
    match0=sum(diri(1:len)==currentdir(1:len))/len; 
    %%%%
    diri='C:\Users\pau';
    len=min(len_currdir,size(diri,2));
    match1=sum(diri(1:len)==currentdir(1:len))/len;

    %%%%
    diri='C:\Users\benoit';
    len=min(len_currdir,size(diri,2));
    match2=sum(diri(1:len)==currentdir(1:len))/len; 
    
    %%%%
    diri='/Users/pauformosa-jordan';
    len=min(len_currdir,size(diri,2));
    match3=sum(diri(1:len)==currentdir(1:len))/len; 
    
    %%%%
    diri='/home/jose/Copy/Region';
    len=min(len_currdir,size(diri,2));
    match4=sum(diri(1:len)==currentdir(1:len))/len; 
    
    %%%%
    diri='/home/pau/MEGAsync';
    len=min(len_currdir,size(diri,2));
    match5=sum(diri(1:len)==currentdir(1:len))/len; 
    
    
    
    if or(match0==1,match1==1)
        computer_str='Pau';
    elseif (match2==1)
         computer_str='Benoit';
    end
    
    if match3==1
        computer_str='Pau laptop';
        elseif (match5==1)
        computer_str='Pau linux';
    end
    
    if match4==1
        computer_str='Jose';
    end
    
    aux=exist('computer_str');
    
    if aux==0 
        computer_str=' ';
    end
    
        
end

    
    
    
    
        
 

