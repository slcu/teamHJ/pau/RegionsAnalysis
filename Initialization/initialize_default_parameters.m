function [inout]=initialize_default_parameters

inout.normal_normalize=1;
inout.create_regions_subfolders=1; % set it to 1 to create regions related subfolders, otherwise set it to 0.
inout.number_tiff_channels=2;
inout.tiff_make_zmean_for_marker=0; % set to 1 to make z-mean in the marker (while z-max in the signal given that needed for this to work inout.tiff_max_int=1 )
inout.polygontype='closed';   % type of polygon; set either 'closed' or 'opened'
inout.enhance_contrast=0; % set to 1 to enhance contrast, othzerwise set it to 1

inout.rolling_ball_background_filter=0; % set 1 to perform the rolling ball background filtering in the first_image (just one channel)
inout.rolling_ball_background_filter_in_signal=0; % set 1 to perform the rolling ball background filtering in the signal as well

inout.save_backup_regions_step=20;
inout.zoom_magnification=100;
inout.save_while_selecting_regions=0;
inout.spatial_filtering=0; % double median filter for the fluorescence spatial analysis
inout.crop_images=0;
inout.make_kymo_region=0; % set it to 1 if kymos computation for rectangular regions, otherwise set it to 0.

inout.ndfilename='snap.nd'; % in case of reading the nd file
inout.ndsignal=1; % when reading nd file, it is assumed the first wavenumber is the signal, and the second is the marker
inout.brightfield_in_movie=0; % set to 1 when willing to see brightfield in movie generation, otherwise set to 0.
inout.overrideformat=0; % set to 0 such that the format is inferred from the filename; otherwise set it to 1 to provide the format thorough inout.format 

%if isfield(inout,'numfiles_to_open')
%    clear inout.numfiles_to_open;
%end
inout.compute_basic_properties_region=0;
inout.export_working_image_to_tiff=0;
inout.islif=0;
inout.marker_channel_index_lif=1; % default lif channel associated to the membrane marker
inout.tif_string_file_to_select='*w1*t1.tif';
inout.do_files_preselection=0; % option when files need to be selected. set to 0 when you do not want to preselect files, otherwise set to 1.
inout.overwriting_mode=1; % Set 1 to activate the overwriting mode, where all files will be analysed. Otherwise set 0, so that just non-analysed files will be analysed. For LSM files.
inout.color_labels='white';
inout.color_regions='blue';

inout.color_labels_while_selection='white';
inout.number_of_regions_setting='by_number'; % alternative: "on_the_fly"
inout.fontsize=18;
inout.plot_domains=0;

inout.min_blob_area=10; % for finding blobs 

% Parameter settings
inout.outsu_threshold_prefactor=1.5;
inout.make_zoom=0; % set to 1 if zoom needed for drawing ROI, and 0 otherwise. When set to 1, place cursor in the region where zoom is needed, and press click.  
inout.magnification=100; % Percentage of image magnification. Note that if this is higher than what the screen allows, it will be rest to the default value, which is 100% magnification.
inout.get_radius_from_found_domains=0; % for automatically found domains, if wanting to get region to study delimited by the size of those domains, set 1, otherwise set 0.
inout.radius_prefactor=1; % prefactor to delimit the size of the domains under study. If 1, the ROI will have a radius equal to the major axis length.

% general parameters
inout.data_structure_filename='data';
inout.make_snap_all_regions=1;
inout.analysis_type='Standard';
inout.plot_fluor_surf_2channels=0;
inout.reading_waitbar=1;
inout.show_im_figs=1;
inout.open_all_files_in_directory=0; % set to 1 for opening all the tiffs in a directory, 0 otherwise.
inout.max_normalised_level_to_show=1; % set a number between 0 to 1. 1 is a non enhanced contrast image.
inout.class_number_of_digits=1;

inout.open_all_positions=0;      % set to 1 for opening all the tiff positions in a directory, 0 otherwise.
inout.lsm_signal_channel_index=1;    % channel to look at if lsm files
inout.islsm=0;
inout.lsm_zsum=0;                    % if we want to do a zsum 
inout.gauss_sigma_microns=3;
inout.lsm_gauss_filter=0;            % if we want to do a gaussian filter, set it to 1, otherwise to 0.
inout.tiff_gauss_filter=0; % if we want to do a gaussian filter, set it to 1, otherwise to 0.
inout.fit_hole=0;                    % for fitting tcs, set it to 1, otherwise, to 0
inout.r_crit_thresh=0;              % set to 1 if you want to compute a threshold-based characteristic length, otherwise set it to 0
inout.find_orientation=0;
inout.find_semiaxis=0;
inout.init_file_to_open=1;


inout.select_region=0;              % set to 1 for selecting regions, 0 otherwise
inout.select_background_regions=0;   % set to 1 for selecting background regions, 0 otherwise
inout.make_orthos=0;
inout.analysis_in_zs=0; % set to 1 if a pipeline should be iterated over all z stacks, 0 otherwise.

inout.click_selecting_regions=1;                  % set to 1 for selecting regions by clicking, set to 0 to load it from previous data or it will find the origin of the central domain.
inout.get_center_and_radius_from_previous_data=0; % for loading from previous data. It just works for circular regions (it will load origin and radius used).
inout.get_center_from_cental_domain=0; %
inout.draw_freehand_polygon=0; % enables drawing of the polyigon in a freehand style. Otherwise set it to 0, and multiple clicks will be needed to plot the polygon.

inout.load_data_for_background_file=0; % set 1 to load data_for_background file, otherwise set it to 0. 

inout.select_z_stack=0;     % set to 1 for selecting the z-stack, otherwise set to 0.
inout.select_time_window=0 ;  % set to 1 for selecting the time window, otherwise set to 0.
inout.snap_regions_with_labels=0;   % set to 1 for drawing the regions labels in a figure
inout.export_subfolderdatapath=1;   % set to 1 for exporting files in a subfolder where the raw data is, 0 for exporting to the same folder as raw data 
inout.standard_format_filename=0;   % set to 0 to load TIFF files with arbitrary names
inout.export_data=1;                % set to 1 to export data structure into the output folder after reading data and do on
inout.num_channels=1;               % number of channels of your image. Please set to 1 for the moment.
inout.create_dirdata=1;             % if all files in a directory are explored, this gives the possibility to create another structure, called dirdata in the raw data directory. 
inout.defaultregiontype='Circle';   % type of region that is going to be analysed (current defaultregiontype options: 'Circle' and 'Cake piece'). Comment this line if different regions are needed to be explored.;
inout.defaultnumberregions=1;       % default number of regions to analyse in each file
inout.defaultnumberbackgroundregions=1; % default number of background regions to analyse in each file

% exporting to schnitzcells
inout.export_renamed_for_schnitzcells=0; % this is for tiff timeseries in a standard format
inout.export_working_image_to_tiff_for_schnitzcells=0; % this is for lsm files for the moment
inout.combine_signal_and_marker_for_schnitzcells=0; % if 1, it combines membrane and signal images to have a more optimised marker for segmentation

inout.imshow_in_fire=0;  % set 1 to show image in fire, 0 otherwise. 

inout.show_label_image=1 ; 
inout.make_snap_with_numbered_centroid_regions=0; % export the raw image with labels in the centroids of the regions
inout.make_snap_of_working_images=0;  % export the raw image

inout.focus_on_single_z_tiff=1;
inout.tiff_max_int=0; % set to 1 to do a max int proj in tiff, set to 0 otherwise
inout.smooth_in_z=0; 
inout.extract_features=0;         % set this parameter to 1 to extract dirdata information, otherwise set it to 0;  
inout.extract_features_stats=1;
% pop-up-related parameters

inout.ask_numberregions=1;          % set this parameter to 1 if you want matlab ask you how many regions to analyze per file, otherwise set it to 0. 
inout.ask_labelregions=1;           % set this parameter to 1 if you want matlab ask you the labels of the regions
inout.ok_boxes=1;                   % set this parameter to 1 if you want matlab poping-up 'oks' during the RegionAnalysis execution
inout.label_type_in_snap='label';  % set 'label' or 'class'
inout.ask_regionclass=0;  % set this parameter to 1 if wanting to class each region after selecting it (single key character), 0 otherwise
inout.ask_numberbackgroundregions=0;

inout.find_all_domains=0;
inout.exclude_central_domain=0;
inout.get_centroids_from_found_domains=0;
inout.find_blobs=0;

% data analysis parameters
inout.analysis_regions=0;           % set to 1 for analysing the different regions, 0 otherwise
inout.export_data_analysis=1;       % set to 1 for exporting data analysis into the data structure
inout.analysis_after_reading=0;     % set to 1 for analysing the data after doing all the files opening

inout.radiusstep=3;                 % set the dr for doing the circle integration.
inout.radiusstep_in_microns=1;      % just working for lsm, override the inout.radiusstep. 
inout.plot_fluor_surf=0;            % plot surface of the fluorescense. Caution: this takes too much time... check it!
inout.plotfits=0;                   % set to 1 to plot the resulting fittings, and to 0 otherwise.
inout.plotwinfit=0;                 % set to 1 to plot the resulting fittings, and to 0 otherwise.
inout.compute_int_vs_r=0;            % set to 1 to compute and plot the intensity vs r in the rectangle or circular region. Otherwise, set it to 0.
inout.crit_conc=10;                 % critical threshold fluorescense to define a threshold-based characteristic size
inout.fit_fixed_exponent=0;              
inout.post_analysis=0;

inout.make_ortos=0;                  % set to 1 if making ortogonal slices, otherwise set it to 0. 
inout.work_on_orthos=0;              % (just revised for lif) set to 1 to use one of the orthos as main image to work, otherwise leave it to 0. The rows in imgout.Info.Dimensions.

inout.make_fittings=0;              % set to 1 to fit the intensity vs r curves to generalised exponentials and hill functions. Otherwise, set to 0.
inout.make_basic_radial=0;
inout.make_historegion=1;

inout.show_label_image=0; % label in the image
inout.plot_ROIs_in_snap_all_regions=0; % set 1 to plot the ROIs in the file showing the ROI labels, otherwise set 0.

inout.scope=' ';

inout.find_central_domain=0; % set to 1 to find the central domain in the xy plane. For lsm files. Otherwise, set to 0.

inout.make_RGB_movie=0; % set 1 for making a movie from single TIFF files being RGB
inout.make_movie_justcombinedchannels=0; % parameter for make_2channels_movies_new

inout.make_movie_combinedchannels=0;

% newer parameters
inout.show_im_figs=1;
inout.number_of_channels_to_compute_intensity=1;
inout.plot_fluor_surf_2channels=0;
inout.extract_schnitz_features=0;

%Implemented for lifs (Casanova)
inout.correctZfocus=1;  % set to value obtained from the ImageJ pluggin: 
                        %https://static-content.springer.com/esm/art%3A10.1038%2Fs41596-020-0360-2/MediaObjects/41596_2020_360_MOESM3_ESM.zip
                        % More info in Diel et all. Nat Protoc 15, 2773–2784 (2020) https://doi.org/10.1038/s41596-020-0360-2
inout.NonEqual_XYResolution=0;

% other stuff
if not(isfield(inout,'timecourse'))
    inout.timecourse=1;
end
inout.background_subtraction=0;
inout.find_background_through_histogram_peak=0; % to extract the background from the histogram peak, set it to 1; otherwise set it to 0.
inout.find_timecourse_background_through_histogram_peak=0; % to extract the background from the histogram peak in a timecourse, set it to 1; otherwise set it to 0.

inout.make_movies=0;  % For doing movies of time lapse experiments. Set to 1 to make 2 channels combined movie, set it to 0 otherwise. make_2channels_movies_new.m function.
inout.plot_background_regions_stuff=1 ; % if select_background_regions, in case you want to see histograms and surface plots, set it to 1, otherwise to 0.
inout.make_2channels_movies=1;
inout.make_single_channel_movies=0;
% plot related 
inout.max_fluor_to_plot=255;

% The following parameters have just implemented for Tiff timecourses.
inout.threshold_backgroun_subtraction=0 ;
inout.median_filter=0 ;

inout.extract_regions_mean_features=0;

inout.open_several_files_in_directory=0;


inout.number_of_channels_to_compute_intensity=1;

