function makemovie_Int_vs_r(inout,data,regionnumber,data_set,namestr)

%ii=find(data.regionlist==regionnumber);
ii=regionnumber;
rs=data.region{ii}.rs;

ymax=1.1*max(max(data_set(:,:)));
%ymax=10;
if isfield(inout,'gradient_marker_max')
    %ymax=(inout.gradient_marker_max-inout.thresh_fluor_marker)*1.2;
    ymax=(inout.gradient_marker_max)*1.2;
end

%name=strcat(namestr,'_vs_r_','region',num2str(regionnumber),'_fracangle',num2str(frac_angularregion),'_',data.channels_string,'_rmax',num2str(maximumradius),'_dr_',num2str(radiusstep));
name=strcat(namestr,'_vs_r_','region',num2str(regionnumber),'_',data.selected_channel_string);

if 1==mean(data.region{ii}.type(1:3)=='Rec')
    wdth=data.region{ii}.pos2(2)-data.region{ii}.pos0(2);
    len=data.region{ii}.pos1(1)-data.region{ii}.pos0(1);
    pos0x=round(data.region{ii}.pos0(1));
    pos0y=round(data.region{ii}.pos1(1));

    name=strcat(namestr,'_vs_r_','region',num2str(regionnumber),'_',data.selected_channel_string,'_width',num2str(wdth),'_len',num2str(wdth),'_pos0_',num2str(pos0x),'_',num2str(pos0y));
end

ymin=0;


%frac_angularregion=data.region{ii}.frac_angularregion;
%maximumradius=data.region{ii}.radius;
%radiusstep=data.region{ii}.radiusstep;
%origin=data.region{regionnumber}.origin;
%x0=origin(1);y0=origin(2);

dirout=strcat('region_',num2str(regionnumber));

font=18;

outfilename=fullfile(data.outdatapath,dirout,name);

aviobj = VideoWriter(outfilename, 'Motion JPEG AVI'); aviobj.FrameRate = 5;
open(aviobj);

%
xmin=0;xmax=1.1*max(rs);


h=figure();
ind=0; hwait=waitbar(0,'% of progress for movie generation in studied region'); % generates a waitbar
%for k=1:10

for k=1:data.time_frames
    waitbar(1.0*ind/(data.time_frames-1)); ind=ind+1; 
    ys=data_set(:,k);
    plot(rs,ys,'-','linewidth',2) % plotting the intensity
    axis([xmin xmax ymin ymax])
    xlabel('r [um]','fontsize',font); ylabel('Fluorescense [A.U.]','fontsize',font)
    set(gca,'fontSize',font);
    text(0.85*xmax, 0.85*ymax,['t',num2str(k)],'FontSize',12);
    save2pdf([outfilename,'_',num2str(k)],h);
    F=getframe(h);
    writeVideo(aviobj, F);
end

close(hwait);
close(aviobj);



