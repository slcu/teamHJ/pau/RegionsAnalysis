function plot_fluor_surf_2channels(inout,data,region_string,regionnumber,namestr)


az=-37.5; el=55;
xresolution=data.resolution(1);
xalph=0.3;
fluor=data.first_image;
marker=data.marker_working_image;
%marker=data.marker_working_image_singlez;
temp_region=getfield(data,region_string);

ii=regionnumber;

regionmask=temp_region{ii}.regionmask;
[row, col]=find(regionmask);


dirout=strcat(region_string,'_',num2str(regionnumber));

font=18;
name=strcat(namestr,'_',data.selected_channel_string,'.pdf');
outfilename=fullfile(data.outdatapath,dirout,name);

name=strcat(namestr,'_marker.pdf');
outfilename_m=fullfile(data.outdatapath,dirout,name);

name=strcat(namestr,'_combined.pdf');
outfilename_sm=fullfile(data.outdatapath,dirout,name);

if ~isfield(data,'resolution')
%if data.microns_per_pixel==1
    xlab='x [A.U.]';
    ylab='y [A.U.]';
else
    xlab='x [\mum]';
    ylab='y [\mum]';

end
% for the moment I let it written in pixels...



ind=0; hwait=waitbar(0,'% of progress for plot generator'); % generates a waitbar

for k=1:data.time_frames
    
    
    xxs_in_um=[0,3,6,9];
    xxs_in_um_labels=[{'0','3','6','9'}]
    xxs=xxs_in_um/xresolution;
    
    waitbar(1.0*ind/(data.time_frames-1)); ind=ind+1; 
    
    regionmask=double(regionmask);
    regionmask(regionmask==0)=NaN;

    
    flourinmask=double(regionmask).*double(fluor); 
    markerinmask=double(regionmask).*double(marker); 

    
    boundregionmask=regionmask(min(row):max(row),min(col):max(col));
    boundflourinmask=flourinmask(min(row):max(row),min(col):max(col));
    boundmarkerinmask=markerinmask(min(row):max(row),min(col):max(col));
    
    reduce=1;

    regiontoplot=double(boundflourinmask(1:reduce:end,1:reduce:end));
    regiontoplot_marker=double(boundmarkerinmask(1:reduce:end,1:reduce:end));

    h=figure();

    ss=surf(regiontoplot)
    set(ss,'FaceColor',[0 0 1]);
    alpha(xalph)
    
    xlabel(xlab,'fontsize',font); ylabel(ylab,'fontsize',font); zlabel('Fluorescense [A.U.]','fontsize',font)
    zlim([0 inout.max_fluor_to_plot])
    view(az,el)

    set(gca,'XTick',xxs);
    set(gca,'xticklabels',xxs_in_um_labels)
    set(gca,'YTick',xxs);
    set(gca,'yticklabels',xxs_in_um_labels)
    
    set(gca,'fontSize',font);
    hh=gcf;
    
    imagewd = getframe(gcf); 

    %imwrite(imagewd.cdata, [outfilename_sm(1:length(outfilename)-3),'.tiff'],'Compression','none','Resolution',300);
    imwrite(imagewd.cdata, [outfilename(1:length(outfilename)-3),'.tiff']);

    %savefig(outfilename(1:length(outfilename)-3));
    save2pdf(outfilename,hh);
    close(h)
    
    
    h=figure();

    yy=surf(regiontoplot_marker)
    set(yy,'FaceColor',[1 0 0]);
    alpha(xalph)

    
    xlabel(xlab,'fontsize',font); ylabel(ylab,'fontsize',font); zlabel('Fluorescense [A.U.]','fontsize',font)
    zlim([0 inout.max_fluor_to_plot])
    view(az,el)

    
    set(gca,'XTick',xxs);
    set(gca,'xticklabels',xxs_in_um_labels)
    set(gca,'YTick',xxs);
    set(gca,'yticklabels',xxs_in_um_labels)
    
    
    set(gca,'fontSize',font);
    hh=gcf;
    imagewd = getframe(gcf); 

   % imwrite(imagewd.cdata, [outfilename_sm(1:length(outfilename_s)-3),'.tiff'],'Compression','none','Resolution',300);
    imwrite(imagewd.cdata, [outfilename_m(1:length(outfilename_m)-3),'.tiff']);

    %savefig(outfilename_m(1:length(outfilename_m)-3));
    save2pdf(outfilename_m,hh);
    close(h)

    h=figure();

    yy=surf(regiontoplot_marker+300)
    %colormap([0  1  0])
    set(yy,'FaceColor',[1 0 0]);
    alpha(yy,xalph)
    
    hold on
    ss=surf(regiontoplot)
    %colormap([0  0  1])
    set(ss,'FaceColor',[0 0 1]);
    alpha(ss,xalph)
    
    hold off
    xlabel(xlab,'fontsize',font); ylabel(ylab,'fontsize',font); zlabel('Fluorescense (A.U.)','fontsize',font)
    zlim([0 inout.max_fluor_to_plot+300])
    view(az,el)
    


    set(gca,'XTick',xxs);
    set(gca,'xticklabels',xxs_in_um_labels)
    set(gca,'YTick',xxs);
    set(gca,'yticklabels',xxs_in_um_labels)
    
    set(gca,'fontSize',font);
    hh=gcf;
    
    imagewd = getframe(gcf); 
   % imwrite(imagewd.cdata, [outfilename_sm(1:length(outfilename_sm)-3),'.tiff'],'Compression','none','Resolution',300);
    imwrite(imagewd.cdata, [outfilename_sm(1:length(outfilename_sm)-3),'.tiff']);
   %savefig(outfilename_sm(1:length(outfilename_sm)-3));
    save2pdf(outfilename_sm,hh);
    close(h)
    
%{    
    h=figure();

    yy=contourf(regiontoplot_marker)
    colormap([0  1  0])
    %set(yy,'FaceColor',[1 0 0]);
    alpha(xalph)
    
    hold on
    ss=contourf(regiontoplot)
    colormap([0  0  1])
    %set(ss,'FaceColor',[0 0 1]);
    alpha(ss,xalph)
    
    hold off
    xlabel(xlab,'fontsize',font); ylabel(ylab,'fontsize',font); zlabel('Fluorescense [A.U.]','fontsize',font)
    zlim([0 inout.max_fluor_to_plot])
    
    set(gca,'fontSize',font);
    hh=gcf;
    savefig(outfilename_sm(1:length(outfilename_sm)-3));
    save2pdf(outfilename_sm,hh);
    close(h)
    %}
    
  
end

close(hwait);




