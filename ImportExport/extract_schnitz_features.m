function [dirdata]=extract_schnitz_features(dirdata,inout)

dirdata.namefiles=[];

dirdata.features.namefiles=[];
dirdata.features.region_numer=[];
dirdata.features.intensity=[];
dirdata.features.mean_intensity=[];


if inout.make_fittings==1 
    dirdata.features.intensity_wo_basal_hill=[];
    dirdata.features.intensity_wo_basal_exp=[];
    dirdata.features.exp_exp=[];
    dirdata.features.hill_exp=[];
    dirdata.features.exp_charlength=[];
    dirdata.features.hill_charlength=[];
    dirdata.features.exp_basalintensity=[];
    dirdata.features.hill_basalintensity=[];

    dirdata.features.hill_rsquare=[];
    dirdata.features.exp_rsquare=[];
end


dirdata.features.centroid_x=[];
dirdata.features.centroid_y=[];

if inout.fit_fixed_exp==1
    dirdata.features.exp_charlength_fixed_exp=[];
    dirdata.features.hill_charlength_fixed_exp=[];
    dirdata.features.exp_basalintensity_fixed_exp=[];
    dirdata.features.hill_basalintensity_fixed_exp=[];
    dirdata.features.hill_rsquare_fixed_exp=[];
    dirdata.features.exp_rsquare_fixed_exp=[];
    dirdata.features.intensity_wo_basal_hill_fixed_exp=[];
    dirdata.features.intensity_wo_basal_exp_fixed_exp=[];
end

if inout.make_fittings==1 
dirdata.features_of_interest={'mean_intensity','intensity','exp_exp','hill_exp','exp_charlength','hill_charlength','exp_basalintensity','hill_basalintensity','centroid_x','centroid_y'};
dirdata.features_for_stats={'exp_exp','hill_exp'};
else
dirdata.features_of_interest={'mean_intensity','intensity','centroid_x','centroid_y'};
dirdata.features_for_stats={};
end

if inout.select_background_regions==1
    dirdata.features.backgroundlevel=[];
end

if inout.islsm==1
    dirdata.features.resolution=[];
    dirdata.features.area_in_um2=[];
end

%if inout.fit_hole==1;
%    dirdata.features.exp_2charlength=[];
%    dirdata.features.hill_2charlength=[];
%end

% Useful indices for ordering data when having more than one region of
% interest in a single image. 

row_index=1;
row_index_bis=1; % for islsm, resolution related
row_index_orth=1; % for orthogonal
row_index_central=1; % for central domain
row_index_fixed=1; % for fixed exponent fitting

row_index_back=1; % for background

num_datasets=size(dirdata.dataset,2);

for ii=1:num_datasets
    
    list_regions=dirdata.dataset{ii}.regionlist;
    num_regions=size(list_regions,2);
    
    % NOTE: jj is for sure ordered, kk not sure! So attention in the next
    % lines. kk is for the label of the region, and jj is for the new
    % features matrices somehow
    
    for jj=1:num_regions
        kk=dirdata.dataset{ii}.regionlist(jj); 
        dirdata.namefiles{ii}=dirdata.dataset{ii}.rootstring_first_image_name;
        dirdata.features.namefiles{row_index,1}=dirdata.namefiles{ii}; % NOTE HERE WW JUST WANT ONE SINGLE NAME FILE
       
        dirdata.features.region_numer{row_index,1}=kk ; % 
        dirdata.features.intensity(row_index,1)=dirdata.dataset{ii}.region{kk}.intensity;
        dirdata.features.mean_intensity(row_index,1)=dirdata.dataset{ii}.region{kk}.mean_intensity;

        if inout.make_fittings==1 
            basal_inferred_background_hill=dirdata.dataset{ii}.region{kk}.area_mask_in_microns_sq*dirdata.dataset{ii}.region{kk}.fithill_pars(4);
            basal_inferred_background_exp=dirdata.dataset{ii}.region{kk}.area_mask_in_microns_sq*dirdata.dataset{ii}.region{kk}.fitexp_pars(4);

            dirdata.features.intensity_wo_basal_hill(row_index,1)=dirdata.dataset{ii}.region{kk}.intensity-basal_inferred_background_hill;
            dirdata.features.intensity_wo_basal_exp(row_index,1)=dirdata.dataset{ii}.region{kk}.intensity-basal_inferred_background_exp;

            dirdata.features.exp_exp(row_index,1)=dirdata.dataset{ii}.region{kk}.fitexp_pars(3);
            dirdata.features.hill_exp(row_index,1)=dirdata.dataset{ii}.region{kk}.fithill_pars(3);
            dirdata.features.exp_charlength(row_index,1)=dirdata.dataset{ii}.region{kk}.fitexp_rcharacteristic;
            dirdata.features.hill_charlength(row_index,1)=dirdata.dataset{ii}.region{kk}.fithill_rcharacteristic;
            dirdata.features.exp_basalintensity(row_index,1)=dirdata.dataset{ii}.region{kk}.fitexp_pars(4);
            dirdata.features.hill_basalintensity(row_index,1)=dirdata.dataset{ii}.region{kk}.fithill_pars(4);        
            dirdata.features.hill_rsquare(row_index,1)=dirdata.dataset{ii}.region{kk}.fithill_rsquare;
            dirdata.features.exp_rsquare(row_index,1)=dirdata.dataset{ii}.region{kk}.fitexp_rsquare;
        end
        
        dirdata.features.centroid_x(row_index,1)=dirdata.dataset{ii}.region{kk}.pos0(1);
        dirdata.features.centroid_y(row_index,1)=dirdata.dataset{ii}.region{kk}.pos0(2);
      
        row_index=row_index+1;  
        
    end
    
    % For the fitted exponent bit
    if (inout.make_fittings==1 && inout.fit_fixed_exp==1)
        for jj=1:num_regions
            basal_inferred_background_hill_fixed_exp=dirdata.dataset{ii}.region{kk}.area_mask_in_microns_sq*dirdata.dataset{ii}.region{kk}.fithill_pars_fixed_exp(3);
            basal_inferred_background_exp_fixed_exp=dirdata.dataset{ii}.region{kk}.area_mask_in_microns_sq*dirdata.dataset{ii}.region{kk}.fitexp_pars_fixed_exp(3);

            kk=dirdata.dataset{ii}.regionlist(jj); 
            dirdata.features.exp_charlength_fixed_exp(row_index_fixed,1)=dirdata.dataset{ii}.region{kk}.fitexp_rcharacteristic_fixed_exp;
            dirdata.features.hill_charlength_fixed_exp(row_index_fixed,1)=dirdata.dataset{ii}.region{kk}.fithill_rcharacteristic_fixed_exp;
            dirdata.features.exp_basalintensity_fixed_exp(row_index_fixed,1)=dirdata.dataset{ii}.region{kk}.fitexp_pars_fixed_exp(3);
            dirdata.features.hill_basalintensity_fixed_exp(row_index_fixed,1)=dirdata.dataset{ii}.region{kk}.fithill_pars_fixed_exp(3);
            dirdata.features.exp_rsquare_fixed_exp(row_index_fixed,1)=dirdata.dataset{ii}.region{kk}.fitexp_rsquare_fixed_exp;
            dirdata.features.hill_rsquare_fixed_exp(row_index_fixed,1)=dirdata.dataset{ii}.region{kk}.fithill_rsquare_fixed_exp;

            dirdata.features.intensity_wo_basal_hill_fixed_exp(row_index_fixed,1)=dirdata.dataset{ii}.region{kk}.intensity-basal_inferred_background_hill_fixed_exp;
            dirdata.features.intensity_wo_basal_exp_fixed_exp(row_index_fixed,1)=dirdata.dataset{ii}.region{kk}.intensity-basal_inferred_background_exp_fixed_exp;
            row_index_fixed=row_index_fixed+1;
        end
    end

    %if inout.fit_hole==1;
    %    for jj=1:size(list_regions,2)
    %        dirdata.features.hill_charlength(row_index,1)=dirdata.dataset{ii}.region{regionnumber}.fithill_r2characteristic;
    %        dirdata.features.exp_charlength(row_index,1)=dirdata.dataset{ii}.region{regionnumber}.fitexp_r2characteristic;
    %    end
   % end
     
    if inout.select_background_regions==1
        list_backgroundregions=dirdata.dataset{ii}.backgroundregionlist;
        for jj=1:size(list_backgroundregions,2)
            kk=dirdata.dataset{ii}.backgroundregionlist(jj); 
            dirdata.features.backgroundlevel(row_index_back,1)=dirdata.dataset{ii}.regionbackground{kk}.backgroundlevel;
            row_index_back=row_index_back+1;
        end
    end
   

    
    if inout.islsm==1
         for jj=1:num_regions
            dirdata.features.resolution(row_index_bis,:)=dirdata.dataset{ii}.resolution;
            kk=dirdata.dataset{ii}.regionlist(jj); 
            dirdata.features.area_in_um2(row_index_bis,1)=dirdata.dataset{ii}.region{kk}.area_mask_in_microns_sq;
            %dirdata.dataset{ii}.region{kk}.area_mask*(dirdata.dataset{ii}.resolution(1)*dirdata.dataset{ii}.resolution(2));
            row_index_bis=row_index_bis+1;
         end
         
        if inout.make_orthos==1
            dirdata.features.MinorAxisLength_xz_in_um(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{1}.MinorAxisLength_in_um;
            dirdata.features.MajorAxisLength_xz_in_um(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{1}.MajorAxisLength_in_um;
            dirdata.features.area_xz_in_um(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{1}.area_in_um;
           
            dirdata.features.Intensity_xz_ortho_sum(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{1}.ortho_sum_intensity;
            dirdata.features.Intensity_xz_ortho_max(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{1}.ortho_max_intensity;

            dirdata.features.MinorAxisLength_yz_in_um(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{2}.MinorAxisLength_in_um;
            dirdata.features.MajorAxisLength_yz_in_um(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{2}.MajorAxisLength_in_um; 
            dirdata.features.area_yz_in_um(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{2}.area_in_um;
            
            
            dirdata.features.Intensity_yz_ortho_sum(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{2}.ortho_sum_intensity;
            dirdata.features.Intensity_yz_ortho_max(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{2}.ortho_max_intensity;            
            
            row_index_orth=row_index_orth+1;
            
        end
        if inout.find_central_domain==1
            dirdata.features.radius_central_domain_in_um(row_index_central,1)=dirdata.dataset{ii}.central_domain.radius_in_um;
            dirdata.features.area_central_domain_in_um(row_index_central,1)=dirdata.dataset{ii}.central_domain.area_in_um;
            dirdata.features.centroid_central_domain(row_index_central,:)=dirdata.dataset{ii}.central_domain.centroid;
            row_index_central=row_index_central+1;
        end

    end
    
end

dirdata.stats=[];
for uu=1:size(dirdata.features_for_stats,2)
    field_for_stats=dirdata.features_for_stats{uu};
    xx=getfield(dirdata.features,field_for_stats);
    if size(xx,2)==1 
        average=mean(xx);
    %elseif size(xx,2)>1  % this would not be right, cause if regions are
    %different, it would involve zeros!!
    %    average=mean(mean(xx)); 
        dirdata.stats = setfield(dirdata.stats,field_for_stats,average);
    end

end

dirdata.features.namefiles{num_datasets+1,1}='Averages';

if isfield(dirdata.stats,'exp_exp')

dirdata.features.exp_exp(num_datasets+1,1) = dirdata.stats.exp_exp;
end
if isfield(dirdata.stats,'hill_exp')

dirdata.features.hill_exp(num_datasets+1,1) = dirdata.stats.hill_exp;
end


end

