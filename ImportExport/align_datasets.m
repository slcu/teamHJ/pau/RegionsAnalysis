function [regionlist1,regionlist2]=align_datasets(inout,data1,data2)
% normally data1 is the manually selected one, and data2 is the
% data with automatically found regions

numregions1=size(data1.region,2);
numregions2=size(data2.region,2);
disp(['Aligning  ' data1.first_image_name ' regions'])

if numregions2==1
       msg=['The domains in ' data1.first_image_name ' were poorly automactically found.' ]; 
%   error(msg);
end
regionlist1=1:numregions1;
regionlist2=1:numregions2;

%data1.datapath
if numregions1>numregions2
   msg=['Less regions automatically detected than manually outlined.']; 
   disp(msg);
end

pairing=[];
minima=[]; % vector of the distances between the paired regions
for ii=1:numregions1  
    distances=[];
        for jj=1:numregions2
        dist=compute_distance(data1.region{ii}.origin,data2.region{jj}.origin);
        distances=[distances dist];
        end
    [minimum indmin]=min(distances);
    pairing=[pairing indmin];
    minima=[minima minimum];
end

%freq=histogram(pairing,'BinLimits',[0.5,max(regionlist2+0.5)]); 
%repeated_num=find(freq.Values>1); % find the data2 region that has been assigned more than once to data1.

freq=histcounts(pairing,'BinLimits',[0.5,max(regionlist2+0.5)]);
repeated_num=find(freq>1); % find the data2 region that has been assigned more than once to data1.

if freq>2
   %msg=['The domains in ' data1.first_image_name ' were poorly automatically found, please reset parameters to find them, or exclude this image from the analysis for now.' ]; 
   msg=['The domains in ' data1.first_image_name ' were poorly automatically found.' ]; 
   %   error(msg);
end

%if(numel(repeated_num)>2)
%   msg=['Revise aligning datasets function, it needs to be improved.']; 
%   error(msg);
%end

if ~isempty(repeated_num)
          indices_to_get=ones(1,numregions1); % default of vector of inidices to get
          
          for kk=1:numel(repeated_num)
                  [pos_repeated]=find(pairing==repeated_num(kk)); % and to which regions it happens in data1
                  good_one=find(minima==min(minima(pos_repeated))); % tell which data1 position is the good one
                  pos_to_drop_data1=pos_repeated(find(good_one~=pos_repeated));  % tell which data1 position is the one to drop
                  
                  %indices_to_drop=[regionlist1~=pos_to_drop_data1];
                  indices_to_drop=~ismember(regionlist1,pos_to_drop_data1);
                  indices_to_get=and(indices_to_get,indices_to_drop); % multiplying the indices to get from the previous iteration with the indices to drop
          end
          regionlist1=regionlist1(indices_to_get); % getting the subset of regions from regionslist1
          regionlist2=pairing(indices_to_get);
   
else
     regionlist2=pairing;
end

% preventing to align regions that are not close to each other
if exist('indices_to_get')
    minima=minima(indices_to_get);
end

inds_to_get=(minima<20/data1.microns_per_pixel);
regionlist1=regionlist1(inds_to_get);
regionlist2=regionlist2(inds_to_get);

close all

end
