function create_dirdata_and_export_cvs(inout)
[dirdata]=create_dirdata(inout);
export_cvs(inout,dirdata)
disp(['Dirdata created for ' inout.datapath]);
end
