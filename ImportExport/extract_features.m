function [dirdata]=extract_features(dirdata,inout)

% This file extract specific features of interest from the dirdata.dataset and set it
% to dirdata.features. This is done in this way to make it easier to handle 
% the features of interest. For instance, it allows RegionsAnalysis to export the features into a csv file.

% extract_features can compute certain feature statistics.

% extract_feactures function calls another function that makes the 'cumulative' indexing of the regions across the
% different datasets.

% Note that if the features are exported to csv, the fields of dirdata.features are going to be
% columns.

num_datasets=size(dirdata.dataset,2);
dirdata.num_datasets=num_datasets;

dirdata.namefiles=[];

% Initialising the features.
[dirdata]=initialise_features_fields(inout,dirdata);

% Specifying features of interest and features for stats
if inout.make_fittings==1 
    dirdata.features_of_interest={'mean_intensity','intensity','exp_max','hill_max','exp_exp','hill_exp','exp_charlength','hill_charlength','exp_basalintensity','hill_basalintensity','centroid_x','centroid_y'};
    dirdata.features_for_stats={'exp_exp','hill_exp'};
else
    if not(inout.polygontype=='opened');  
        dirdata.features_of_interest={'mean_intensity','intensity','centroid_x','centroid_y'};
    else
        dirdata.features_of_interest={};
    end
    dirdata.features_for_stats={};    
    if isfield(inout,'histomaxima')
        if inout.histomaxima==1
            dirdata.features_of_interest={'mean_intensity','intensity','centroid_x','centroid_y','histomaxima'};
        else
            dirdata.features_of_interest={'mean_intensity','intensity','centroid_x','centroid_y'};
        end
    end

end

if or(inout.find_background_through_histogram_peak==1,inout.find_timecourse_background_through_histogram_peak==1)
    dirdata.features_of_interest{end+1}='signal_background';
end

if isfield(dirdata.dataset{1},'region')
    switch dirdata.dataset{1}.region{1}.type
        case 'Polygon'
            if inout.polygontype=='opened'
                if inout.find_radial_distance==1
                    dirdata.features_of_interest=horzcat(dirdata.features_of_interest,{'parab_curvature_um-1','parab_height0_um','parab_dheight_um','parab_height0_man_um','parab_dheight_man_um','parab_index0_man','parab_index1_man','parab_radius0_um','parab_radius1_um','parab_rsquare','Zproj_meristem_radious_um'});
                else
                    dirdata.features_of_interest=horzcat(dirdata.features_of_interest,{'parab_curvature_um-1','parab_height0_um','parab_dheight_um','parab_height0_man_um','parab_dheight_man_um','parab_index0_man','parab_index1_man','parab_radius0_um','parab_radius1_um','parab_rsquare'});
                end
            end
    end
end

% Indices initialisation for ordering data when having more than one region of
% interest in a single image. 

% This is important for the following loop.

row_index=1;
row_index_bis=1; % for islsm, resolution related
row_index_orth=1; % for orthogonal
row_index_central=1; % for central domain
row_index_fixed=1; % for fixed exponent fitting
row_index_histomaxima=1;
row_index_back=1; % for background
row_index_orient=1;
row_index_class=1;
row_index_semiaxis=1;
row_index_background=1;

% Loop where the specific features are assigned to the dirdata.features
% structure

for ii=1:num_datasets
    
    list_regions=dirdata.dataset{ii}.regionlist;
    num_regions=size(list_regions,2);
    
    % NOTE: jj is for sure ordered, kk not sure! So attention in the next
    % lines. kk is for the label of the region, and jj is for the new
    % features matrices somehow
    
    if num_regions==0 
        dirdata.namefiles{ii}=dirdata.dataset{ii}.rootstring_first_image_name;
        dirdata.features.namefiles{ii,1}=dirdata.namefiles{ii}; % NOTE HERE WE JUST WANT ONE SINGLE NAME FILE   
    end
    
    for jj=1:num_regions
        kk=dirdata.dataset{ii}.regionlist(jj); 
        dirdata.namefiles{ii}=dirdata.dataset{ii}.rootstring_first_image_name;
        dirdata.features.namefiles{row_index,1}=dirdata.namefiles{ii}; % NOTE HERE WE JUST WANT ONE SINGLE NAME FILE
       
        dirdata.features.region_number{row_index,1}=kk ;  
        dirdata.features.selected_channel_string{row_index,1}=dirdata.dataset{ii}.selected_channel_string;
        
        switch inout.analysis_type
            case 'Standard'
                if isfield(dirdata.dataset{ii}.region{kk},'intensity')
                    if or(not(mean(inout.polygontype=='opened')),mean(inout.polygontype=='closed'))
                        dirdata.features.intensity(row_index,1)=dirdata.dataset{ii}.region{kk}.intensity;
                        dirdata.features.mean_intensity(row_index,1)=dirdata.dataset{ii}.region{kk}.mean_intensity;
                    end
                end
            case 'Multi-z'
                dirdata.features.mean_intensity_zs_sandwitch(row_index,1)=dirdata.dataset{ii}.region{kk}.zsum_mean_int_sandwich_optimal_z;
        end
        
        if inout.make_fittings==1 
            basal_inferred_background_hill=dirdata.dataset{ii}.region{kk}.area_mask_in_microns_sq*dirdata.dataset{ii}.region{kk}.fithill_pars(4);
            basal_inferred_background_exp=dirdata.dataset{ii}.region{kk}.area_mask_in_microns_sq*dirdata.dataset{ii}.region{kk}.fitexp_pars(4);

            dirdata.features.intensity_wo_basal_hill(row_index,1)=dirdata.dataset{ii}.region{kk}.intensity-basal_inferred_background_hill;
            dirdata.features.intensity_wo_basal_exp(row_index,1)=dirdata.dataset{ii}.region{kk}.intensity-basal_inferred_background_exp;

            dirdata.features.exp_max(row_index,1)=dirdata.dataset{ii}.region{kk}.fitexp_pars(1);
            dirdata.features.hill_max(row_index,1)=dirdata.dataset{ii}.region{kk}.fithill_pars(1);
            
            dirdata.features.exp_exp(row_index,1)=dirdata.dataset{ii}.region{kk}.fitexp_pars(3);
            dirdata.features.hill_exp(row_index,1)=dirdata.dataset{ii}.region{kk}.fithill_pars(3);
            
            dirdata.features.exp_charlength(row_index,1)=dirdata.dataset{ii}.region{kk}.fitexp_rcharacteristic;
            dirdata.features.hill_charlength(row_index,1)=dirdata.dataset{ii}.region{kk}.fithill_rcharacteristic;
            
            dirdata.features.exp_basalintensity(row_index,1)=dirdata.dataset{ii}.region{kk}.fitexp_pars(4);
            dirdata.features.hill_basalintensity(row_index,1)=dirdata.dataset{ii}.region{kk}.fithill_pars(4);        
            dirdata.features.hill_rsquare(row_index,1)=dirdata.dataset{ii}.region{kk}.fithill_rsquare;
            dirdata.features.exp_rsquare(row_index,1)=dirdata.dataset{ii}.region{kk}.fitexp_rsquare;
        end
 
        if inout.get_centroids_from_found_domains==1
            dirdata.features.distcenter_in_um(row_index,1)=dirdata.dataset{ii}.region{kk}.distcenter_in_um;
        end
        
        % this is the centroid for Polygon and circle regions; not forcely
        % for other ROIs ! see commented code below, maybe need to apply it
        % to rectangles, etc
        if not(inout.polygontype=='opened')        
            dirdata.features.centroid_x(row_index,1)=dirdata.dataset{ii}.region{kk}.origin(1);
            dirdata.features.centroid_y(row_index,1)=dirdata.dataset{ii}.region{kk}.origin(2);
        end
        
        switch dirdata.dataset{ii}.region{kk}.type
            case 'Polygon'
                if inout.polygontype=='opened'
                    dirdata.features.parab_rsquare(row_index,1)=dirdata.dataset{ii}.region{kk}.fitparabola_rsquare;                    
                    dirdata.features.parab_curvature_inv_um(row_index,1)=dirdata.dataset{ii}.region{kk}.fitparabola_curvature;
                    dirdata.features.parab_height0_um(row_index,1)=dirdata.dataset{ii}.region{kk}.fitparabola_height0;
                    dirdata.features.parab_height0_man_um(row_index,1)=dirdata.dataset{ii}.region{kk}.fitparabola_height0_man;
                    dirdata.features.parab_dheight_um(row_index,1)=dirdata.dataset{ii}.region{kk}.fitparabola_dheight;
                    dirdata.features.parab_dheight_man_um(row_index,1)=dirdata.dataset{ii}.region{kk}.fitparabola_dheight_man;
                    dirdata.features.parab_index0(row_index,1)=dirdata.dataset{ii}.region{kk}.fitparabola_index0;
                    dirdata.features.parab_index0_man(row_index,1)=dirdata.dataset{ii}.region{kk}.fitparabola_index0_man;
                    dirdata.features.parab_index1(row_index,1)=dirdata.dataset{ii}.region{kk}.fitparabola_index1;
                    dirdata.features.parab_index1_man(row_index,1)=dirdata.dataset{ii}.region{kk}.fitparabola_index1_man;
                    dirdata.features.parab_radius0_um(row_index,1)=dirdata.dataset{ii}.region{kk}.fitparabola_radius0;
                    dirdata.features.parab_radius1_um(row_index,1)=dirdata.dataset{ii}.region{kk}.fitparabola_radius1; 
                    
                    if inout.find_radial_distance==1
                        dirdata.features.Zproj_meristem_radious_um(row_index,1)=dirdata.dataset{ii}.meristem_radious_in_um;
                    end
                end
        end                

        %        dirdata.features.centroid_x(row_index,1)=dirdata.dataset{ii}.region{kk}.centroid(1);
        %        dirdata.features.centroid_y(row_index,1)=dirdata.dataset{ii}.region{kk}.centroid(2);
        %    otherwise
        %        dirdata.features.centroid_x(row_index,1)=dirdata.dataset{ii}.region{kk}.origin(1);
        %        dirdata.features.centroid_y(row_index,1)=dirdata.dataset{ii}.region{kk}.origin(2);

        row_index=row_index+1;  
        
    end % num_regions loop
    
    % For the fixed exponent bit
    if (inout.make_fittings==1 && inout.fit_fixed_exponent==1)
        for jj=1:num_regions
            basal_inferred_background_hill_fixed_exponent=dirdata.dataset{ii}.region{kk}.area_mask_in_microns_sq*dirdata.dataset{ii}.region{kk}.fithill_pars_fixed_exponent(3);
            basal_inferred_background_exp_fixed_exponent=dirdata.dataset{ii}.region{kk}.area_mask_in_microns_sq*dirdata.dataset{ii}.region{kk}.fitexp_pars_fixed_exponent(3);

            kk=dirdata.dataset{ii}.regionlist(jj); 
            dirdata.features.exp_charlength_fixed_exponent(row_index_fixed,1)=dirdata.dataset{ii}.region{kk}.fitexp_rcharacteristic_fixed_exponent;
            dirdata.features.hill_charlength_fixed_exponent(row_index_fixed,1)=dirdata.dataset{ii}.region{kk}.fithill_rcharacteristic_fixed_exponent;
            dirdata.features.exp_basalintensity_fixed_exponent(row_index_fixed,1)=dirdata.dataset{ii}.region{kk}.fitexp_pars_fixed_exponent(3);
            dirdata.features.hill_basalintensity_fixed_exponent(row_index_fixed,1)=dirdata.dataset{ii}.region{kk}.fithill_pars_fixed_exponent(3);
            dirdata.features.exp_rsquare_fixed_exponent(row_index_fixed,1)=dirdata.dataset{ii}.region{kk}.fitexp_rsquare_fixed_exponent;
            dirdata.features.hill_rsquare_fixed_exponent(row_index_fixed,1)=dirdata.dataset{ii}.region{kk}.fithill_rsquare_fixed_exponent;

            dirdata.features.intensity_wo_basal_hill_fixed_exponent(row_index_fixed,1)=dirdata.dataset{ii}.region{kk}.intensity-basal_inferred_background_hill_fixed_exponent;
            dirdata.features.intensity_wo_basal_exp_fixed_exponent(row_index_fixed,1)=dirdata.dataset{ii}.region{kk}.intensity-basal_inferred_background_exp_fixed_exponent;
            row_index_fixed=row_index_fixed+1;
        end
    end

    %if inout.fit_hole==1;
    %    for jj=1:size(list_regions,2)
    %        dirdata.features.hill_charlength(row_index,1)=dirdata.dataset{ii}.region{regionnumber}.fithill_r2characteristic;
    %        dirdata.features.exp_charlength(row_index,1)=dirdata.dataset{ii}.region{regionnumber}.fitexp_r2characteristic;
    %    end
   % end
     
    if inout.select_background_regions==1
        list_backgroundregions=dirdata.dataset{ii}.backgroundregionlist;
        for jj=1:size(list_backgroundregions,2)
            kk=dirdata.dataset{ii}.backgroundregionlist(jj); 
            dirdata.features.backgroundlevel(row_index_back,1)=dirdata.dataset{ii}.regionbackground{kk}.backgroundlevel;
            row_index_back=row_index_back+1;
        end
    end
    
    %if or(inout.islsm==1,inout.islif==1)
    switch inout.analysis_type
     case 'Standard'
         for jj=1:num_regions
            if and(isfield(inout,'double_parabola'),size(dirdata.dataset{ii}.regionlist,2)==inout.defaultnumberregions*2)  
                if and(inout.double_parabola==1,jj>inout.defaultnumberregions)
                    dirdata.features.resolution(row_index_bis,:)=[dirdata.dataset{ii}.microns_per_pixel_orthos,dirdata.dataset{ii}.microns_per_pixel_orthos,dirdata.dataset{ii}.microns_per_pixel];                    
                else
                    dirdata.features.resolution(row_index_bis,:)=dirdata.dataset{ii}.resolution;
                end
            else
                dirdata.features.resolution(row_index_bis,:)=dirdata.dataset{ii}.resolution;
            end 
            kk=dirdata.dataset{ii}.regionlist(jj); 
            dirdata.features.area_in_um2(row_index_bis,1)=dirdata.dataset{ii}.region{kk}.area_mask_in_microns_sq;
            %dirdata.dataset{ii}.region{kk}.area_mask*(dirdata.dataset{ii}.resolution(1)*dirdata.dataset{ii}.resolution(2));
            row_index_bis=row_index_bis+1;
         end
    end

    if inout.make_orthos==1
        if isfield(dirdata.dataset{ii},'ortho_planes') % this allows to extract features from mixed datasets, some in 2D and 3D
            dirdata.features.height_xz_in_um(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{1}.height_in_um;
            dirdata.features.width_xz_in_um(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{1}.width_in_um;
            dirdata.features.area_xz_in_um2(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{1}.area_in_um2;

            dirdata.features.Intensity_xz_ortho_sum(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{1}.ortho_sum_intensity;
            dirdata.features.Intensity_xz_ortho_max(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{1}.ortho_max_intensity;

            dirdata.features.height_yz_in_um(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{2}.height_in_um;
            dirdata.features.width_yz_in_um(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{2}.width_in_um; 
            dirdata.features.area_yz_in_um2(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{2}.area_in_um2;
       
            dirdata.features.Intensity_yz_ortho_sum(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{2}.ortho_sum_intensity;
            dirdata.features.Intensity_yz_ortho_max(row_index_orth,jj)=dirdata.dataset{ii}.ortho_planes{2}.ortho_max_intensity;            
        end
        row_index_orth=row_index_orth+1;

    end

    if inout.find_central_domain==1
         if isfield(dirdata.dataset{ii},'central_domain') % this allows to extract features from mixed datasets, some in 2D and 3D
            dirdata.features.radius_central_domain_in_um(row_index_central,1)=dirdata.dataset{ii}.central_domain.radius_in_um;
            dirdata.features.area_central_domain_in_um(row_index_central,1)=dirdata.dataset{ii}.central_domain.area_in_um2;
            dirdata.features.centroid_central_domain(row_index_central,:)=dirdata.dataset{ii}.central_domain.centroid;
            dirdata.features.aspect_ratio_central_domain(row_index_central,:)=dirdata.dataset{ii}.central_domain.aspect_ratio;
            dirdata.features.circularity(row_index_central,:)=dirdata.dataset{ii}.central_domain.circularity;
         end
        row_index_central=row_index_central+1;    
    end
    
    if inout.find_orientation==1
         for jj=1:num_regions
             kk=dirdata.dataset{ii}.regionlist(jj);
             dirdata.features.Orientation(row_index_orient,1)=dirdata.dataset{ii}.region{kk}.mask_orientation;
             row_index_orient=row_index_orient+1;
         end
    end

    if inout.find_semiaxis==1
         for jj=1:num_regions
             kk=dirdata.dataset{ii}.regionlist(jj);
             dirdata.features.minor_axis_in_um(row_index_semiaxis,1)=dirdata.dataset{ii}.region{kk}.minor_axis_in_um;
             dirdata.features.major_axis_in_um(row_index_semiaxis,1)=dirdata.dataset{ii}.region{kk}.major_axis_in_um;
             dirdata.features.aspect_ratio(row_index_semiaxis,1)=dirdata.dataset{ii}.region{kk}.aspect_ratio;
             row_index_semiaxis=row_index_semiaxis+1;
         end
    end

    if inout.ask_regionclass==1
         for jj=1:num_regions
             kk=dirdata.dataset{ii}.regionlist(jj);
             if isempty(dirdata.dataset{ii}.region{kk}.class)
                 disp(['Please rewrite regions number assignements for ' dirdata.dataset{ii}.first_image_name]);
                 %disp(num2str(ii));
                 return
             end
             dirdata.features.Class(row_index_class,1)=dirdata.dataset{ii}.region{kk}.class;
             row_index_class=row_index_class+1;
         end
    end
    %end
    
    if isfield(inout,'histomaxima')
         if inout.histomaxima==1
             for jj=1:num_regions
                kk=dirdata.dataset{ii}.regionlist(jj); 
                dirdata.features.histomaxima(row_index_histomaxima,:)=dirdata.dataset{ii}.region{kk}.histomaxima(:);
                row_index_histomaxima=row_index_histomaxima+1;
             end
          end
    end
    
    if or(inout.find_background_through_histogram_peak==1,inout.find_timecourse_background_through_histogram_peak==1)
        dirdata.features.signal_background(row_index_background,1)=dirdata.dataset{ii}.signal_background;
        row_index_background=row_index_background+1;
    end

end

% Statistics for some of the features
if inout.extract_features_stats==1
    [dirdata]=extract_features_stats(inout,dirdata);
end


% Getting the cumulative list of regions. 
% (Note: if moved above/below, make sure it will not affect the cumulative index computation)
[dirdata]=get_dirdata_comulative_numregions_indices(inout,dirdata);

[dirdata.features]=remove_empty_structfields(dirdata.features);

end

