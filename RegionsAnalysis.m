function [data]=RegionsAnalysis(inout)
close all;
% RegionsAnalysis is a function that reads a set of TIF, LSM, CZI or LIF images, 
% does some preprocessing steps and analysis. 

% An important output is a data structure that stores 
% the raw and preprocessed data, along with the performed analysis.

% For executing it with the default inout structure parameters, write 
% [data]=RegionsAnalysis; in the command line, 
% or just RegionsAnalysis; in case a whole directory is explored 
% (and hence inout.open_all_files_in_directory=1)

% NOTE: This code is working, but is still under construction. Pau will clean the code at some
% point to make it more readable and structured, so that people can also work on that easily.

if(nargin < 1)
    inout=quickdefault_inout; % get the default inout parameters if none provided
end

inout=check_inout(inout); % check some of the parameter values
if inout.return==1
    return
end

datafolder=inout.datafolder;

if ~isfield(inout,'datapath')
    datapath = uigetdir(datafolder,'Select the data set location');

    if ~datapath
    return;
    end

    disp('chosen directory is');
    disp(datapath)

    data.datapath=datapath;
    inout.datapath=data.datapath;
else
    data.datapath=inout.datapath;
    datapath=inout.datapath;
end 


Dtif_thumbs =dir(fullfile(datapath,'*thumb*.tif'));
if size(Dtif_thumbs,1)>0
    inout.out_thumbs_path=fullfile(datapath,'thumbs');
    create_dir(inout.out_thumbs_path)
    movefile(fullfile(datapath,'*thumb*.tif'),inout.out_thumbs_path);
end

if isfield(inout,'init_file_to_open')
    init_file_to_open=inout.init_file_to_open;
else
    init_file_to_open=1;
end

if inout.open_several_files_in_directory==1
    Dtif = dir(fullfile(data.datapath,'*.tif'));
    Dlsm = dir(fullfile(data.datapath,'*.lsm'));
    Dczi = dir(fullfile(data.datapath,'*.czi'));

    Dlif = dir(fullfile(data.datapath,'*.lif'));

    if inout.open_all_positions==1
        if inout.standard_format_filename==1
            Dtif =dir(fullfile(data.datapath,inout.tif_string_file_to_select)); % select all the positions at t1
        else
            Dtif =dir(fullfile(data.datapath,inout.tif_string_file_to_select)); % select positions with a specific name
        end
    end
    
    if inout.overwriting_mode==0
        Dlsm=find_non_analysed_files(inout,Dlsm);
        Dczi=find_non_analysed_files(inout,Dczi);
        Dtif=find_non_analysed_files(inout,Dtif);
    end    
    
    D=[Dlsm,Dtif,Dczi];
    if size(Dlif,1)>0
        D=Dlif;
    end    
    numfiles_to_open=length(D);
    
    if size(Dlif,1)>0
        fname=fullfile(datapath,Dlif(1).name);
        [imgout]=ci_loadLif(fname,'whatever1','whatever2');
        numfiles_to_open=imgout;
        if isfield(inout,'numfiles_to_open')
            numfiles_to_open=inout.numfiles_to_open;
        end
        if isfield(inout,'open_multiple_lifs')
            if inout.open_multiple_lifs==1
                numfiles_to_open=numfiles_to_open*size(Dlif,1);
            end
        end
    end
    
    if inout.create_regions_subfolders==1
        inout.out_tiffs_path=fullfile(datapath,'tiffs_with_numbered_regions');
        create_dir(inout.out_tiffs_path);
        inout.out_working_tiffs_path=fullfile(datapath,'working_tiffs');
        create_dir(inout.out_working_tiffs_path);

        inout.out_selected_regions_path=fullfile(datapath,'selected_regions');
        create_dir(inout.out_selected_regions_path);

        inout.out_selected_regions_fig_path=fullfile(datapath,'selected_regions','fig_format');
        create_dir(inout.out_selected_regions_fig_path);   
    end
    
    if inout.make_orthos==1
        inout.out_tiffs_orthos_sum_path=fullfile(datapath,'tiffs_orthos_sum');
        create_dir(inout.out_tiffs_orthos_sum_path);
        
        inout.out_tiffs_orthos_max_path=fullfile(datapath,'tiffs_orthos_max');
        create_dir(inout.out_tiffs_orthos_max_path)
        
        inout.out_tiffs_orthos_processed_path=fullfile(datapath,'tiffs_orthos_processed');
        create_dir(inout.out_tiffs_orthos_processed_path)

    end
    
    if or(inout.find_central_domain==1,inout.find_all_domains==1)
        inout.out_tiffs_xy_domain_processed_path=fullfile(datapath,'tiffs_xy_domain_processed');
        create_dir(inout.out_tiffs_xy_domain_processed_path);
    end
    if inout.find_blobs==1
        inout.out_blobs_path=fullfile(datapath,'blobs');
        create_dir(inout.out_blobs_path);
    end
    
else  
    if inout.do_files_preselection==1
        src = uigetfile(fullfile(datapath,inout.preselection_string), 'Select one TIF or LSM image file ');
    else
        src = uigetfile(fullfile(datapath,'*.*'), 'Select one TIF or LSM image file ');
    end
    
    if ~src
    return;
    end
    numfiles_to_open=1;  
    inout.out_working_tiffs_path=fullfile(datapath,'working_tiffs');
    create_dir(inout.out_working_tiffs_path);

    inout.out_tiffs_path=fullfile(datapath,'tiffs_with_numbered_regions');
    create_dir(inout.out_tiffs_path);
end

fin_file_to_open=(numfiles_to_open+init_file_to_open-1);
if isfield(inout,'open_multiple_lifs')
    if inout.open_multiple_lifs==1
        init_file_to_open=(inout.init_file_to_open-1)*inout.numfiles_to_open+1;
        fin_file_to_open=(numfiles_to_open);
    end
end

for ifiles=init_file_to_open:fin_file_to_open  % Starting the directory loop
disp(strcat('Getting regions in file number  ',num2str(ifiles)));
clear A

if inout.open_several_files_in_directory==1
    if size(Dlif,1)==0
        fullname=D(ifiles).name ;
        src=fullname;
    else        
        fullname=Dlif(1).name ;
        src=fullname; 
        if isfield(inout,'open_multiple_lifs')
            if inout.open_multiple_lifs==1 
                fullname=Dlif(ceil(ifiles/inout.numfiles_to_open)).name ;
                src=fullname;  
            end
        end
    end
end

    
% Name of the selected file
first_im_name=src;
data.first_image_name=first_im_name;
fin_rootstring=strfind(first_im_name,'.');
data.rootstring_first_image_name=first_im_name(1:fin_rootstring-1);


if inout.export_subfolderdatapath==1
     if ~exist('Dlif')
            outdatapath=fullfile(datapath,data.rootstring_first_image_name);
            data.outdatapath=outdatapath;
            create_dir(outdatapath)
     end
     if exist('Dlif')
            if size(Dlif,1)==0
                outdatapath=fullfile(datapath,data.rootstring_first_image_name);
                data.outdatapath=outdatapath;
                create_dir(outdatapath)
            end
     end
else
    outdatapath=datapath;
    data.outdatapath=outdatapath;
end

if inout.open_several_files_in_directory==0
    inout.out_selected_regions_path=data.outdatapath;
    create_dir(inout.out_selected_regions_path);
    
    inout.out_selected_regions_fig_path=fullfile(data.outdatapath,'fig_format');
    create_dir(inout.out_selected_regions_fig_path);  
end

if inout.timecourse==1
    fulldiroutdata=fullfile(outdatapath,'MOVS'); 
    create_dir(fulldiroutdata)
end

% Parse first_im_name to identify the string location of timepoint, color, stage
% position, and extension

fname=fullfile(datapath,first_im_name);

ini_ext_loc = regexp(first_im_name, '(\.tif|\.TIF|\.lsm|\.lif|\.czi)', 'start');
ext=first_im_name(ini_ext_loc+1:size(first_im_name,2));
inout.ext=ext;

if size(ext,2)>3
   ext=ext(end-2:end);
end

switch ext
    case 'lsm' % Extracting relevant image information if the image file if it is in lsm format
        islsm=1;    
        format='lsmczi';
        infolsm=lsminfo(fname);
        data.resolution=(infolsm.VOXELSIZES)*10^6;
        xresolution=data.resolution(1,1);
        data.numchannels_image=infolsm.NUMBER_OF_CHANNELS; % not the same as inout.numchannels
        data.time_frames_image=infolsm.TIMESTACKSIZE; % 
        data.zs_image=infolsm.DimensionZ;
        data.xys_image=infolsm.DimensionX;
        inout.islsm=islsm;
        inout.radiusstep=inout.radiusstep_in_microns/xresolution;
        data.radiusstep=inout.radiusstep;
        isczi=0;istif=0;islif=0;
    case 'lif'
        islif=1;
        format='lif';
        islsm=0;isczi=0;istif=0;
    case 'czi'
        format='lsmczi';
        islsm=0;isczi=1;istif=0;islif=0;
    case 'tif'
        istif=1;
        format='tif';
        data.resolution=[0.8 0.8]; % for 20x Air Spinning disk
        islsm=0;islif=0;isczi=0;
    case 'TIF'
        istif=1;
        format='tif';
        data.resolution=[0.8 0.8]; % for 20x Air Spinning disk
        islsm=0;islif=0;isczi=0;
end
    
inout.islif=islif;
inout.isczi=isczi;

if or(islsm==1,istif==1)
    info = imfinfo(fname);
    num_images = numel(info);
    data.num_images=num_images;
end

if inout.overrideformat==1
    format=inout.format;
else
    inout.format=format;
end

% This part of the code read the lsm or tiff selected image
switch format
    case 'tif'
        if isfield(info,'XResolution')
            auxx=(info.XResolution);
            auxy=(info.YResolution);

            xresolution=1/auxx;
            yresolution=1/auxy;

            a=info.ImageDescription;
            in=strfind(a,'spacing')+8;
            zresolution=str2double(a(in:in+5));
            data.resolution=[xresolution yresolution zresolution];
        end

        if num_images>1   % tiff z-stack
            for k = 1:num_images
                A(:,:,k) = imread(fname, k, 'Info', info);
            end

            if inout.focus_on_single_z_tiff==1
                if inout.select_z_stack==1 
                    imshow3D(mat2gray(A));

                    % Select the z interval you wish to focus on
                    prompt = {'z-stack'};
                    dlg_title = 'Select z';
                    num_lines = 1;
                    defaultanswer = {num2str(floor(num_images/2))};
                    options.WindowStyle='normal';
                    options.Interpreter='tex';
                    answer = inputdlg(prompt, dlg_title, num_lines, defaultanswer, options);
                    zstack=answer{1};
                    im=A(:,:,str2num(zstack));

                elseif inout.select_z_stack==0
                    zstack=num2str(floor(num_images/2));
                    im=A(:,:,str2num(zstack));
                end
                
                data.zstack=zstack;
                   
                if inout.smooth_in_z==1
                    numzs=inout.num_zs_for_smoothing;
                    bottomz=numzs/2-0.5;
                    for uy=1:numzs
                        Abis(:,:,uy)=A(:,:,str2num(zstack)-1-bottomz+uy);    
                    end
                    %im=max(Abis,[],3);
                    im=uint16(mean(Abis,3));
                end

            else % else of focus on particular z

                if inout.tiff_max_int==1
                    zstack='30';
                    im=max(A,[],3);  
                elseif inout.tiff_sum_int==1
                    im=sum(A,3);
                else
                    im=A;
                end
                
                if inout.tiff_gauss_filter==1
                    finalsigma=inout.gauss_sigma_microns/xresolution ;
                    im = imgaussfilt(im,finalsigma);
                end
            end % if between max int proj or single z

            if or(inout.find_all_domains==1,inout.find_blobs==1)    
                data.zmax=max(A,[],3);
                data.zsum=sum(A,3);
            end
            
            if inout.find_all_domains==1
                [data]=find_all_domains(inout,data);
            end
            if inout.find_blobs==1
                [data]=find_blobs(inout,data);
            end
            
            data.xys_image=size(im,1);

            data.selected_working_image=im;
        
        else % if it is not an lsm but a tiff
            im=imread(fname);
            %obj = Tiff(fname,'r')
            data.selected_working_image=im;
        end % tiff

    case 'tiffrgb'
        A=imread(fname);
        im=A;
        data.selected_working_image=im;
        %data.resolution=25400*[1/info(1).XResolution 1/info(1).YResolution];
        
        filename=fullfile(inout.datapath,'extras');
        if isfile([filename '.mat'])
            load(filename,'extras');
            data.resolution=extras.resolution;  
        else
            msg='Note this type of format requires the resolution input. No resolution file was found.'; 
            error(msg);
        end
        
%    case or('lsm','czi')   % lsm z-stack 
    case 'lsmczi'   % lsm z-stack 
        if islsm==1
            lsmdata=lsmread(fname);
            sq_data=squeeze(lsmdata(1,inout.signal_channel_index,:,:,:));
        elseif isczi==1
            aux=ReadImage6D(fname);
            czidata=aux{1};   
            data.zs_image=aux{2}.SizeZ;
            xresolution=aux{2}.ScaleX;
            yresolution=aux{2}.ScaleY;
            zresolution=aux{2}.ScaleZ;
            data.resolution=[xresolution yresolution zresolution];
            inout.radiusstep=inout.radiusstep_in_microns/xresolution;
            data.radiusstep=inout.radiusstep;
            sq_data=squeeze(czidata(1,1,:,inout.signal_channel_index,:,:));
            data.xys_image=aux{2}.SizeX;
        end
        
        num_zs=data.zs_image;
        
        if data.zs_image>1
            A=permute(sq_data,[2 3 1]); 
            data.zmax=max(A,[],3);
            data.zsum=sum(A,3);    
            
            if inout.make_orthos==1 
                data.ortho{1}=permute(A,[1 3 2]); 
                data.ortho{2}=permute(A,[2 3 1]);  
            end  
        else
            A=sq_data;         
        end
        
        % in case there is a second channel of interest
        if isfield(inout,'marker_channel_index')
            if islsm==1
                sq_data_marker=squeeze(lsmdata(1,inout.marker_channel_index,:,:,:));
            elseif isczi==1
                sq_data_marker=squeeze(czidata(1,1,:,inout.marker_channel_index,:,:));
            end
            if data.zs_image>1    
                sq_data_marker=permute(sq_data_marker,[2 3 1]);   
                data.zsummarker=sum(sq_data_marker,3);  
            end
        end

        % Managing different options for z-stacks: maximal intensity
        % projection, sum of the intensities or just deal with one z level.
 
        
        if num_zs>1  
            zproj=0;
            if and(inout.zsum==0,inout.zmax==1) 
                zproj=1;
            elseif and(inout.zsum==1,inout.zmax==0)
                zproj=2;
            else
                zproj=0;
            end

            if inout.analysis_in_zs==1
                data.temp_zstacks=A;
            end

            switch zproj 
                case 0   
                    imshow3D(mat2gray(A));
                    % Select the time interval you wish to focus on
                    prompt = {'z-stack'};
                    dlg_title = 'Select z';
                    num_lines = 1;

                    defaultanswer = {num2str(floor(num_zs/2))};
                    options.WindowStyle='normal';
                    options.Interpreter='tex';
                    answer = inputdlg(prompt, dlg_title, num_lines, defaultanswer, options);
                    zstack=answer{1};

                    im=A(:,:,str2num(zstack));
                    data.selected_z=zstack;
                case 1
                    im=data.zmax; 
                case 2
                    im=data.zsum;
            end % end of what to do with the stack projection

        else % if it is not a stack
            im=sq_data;
        end
        
        data.selected_working_image=im;
                
        if inout.lsm_gauss_filter==1
            finalsigma=inout.gauss_sigma_microns/xresolution ;
            im = imgaussfilt(im,finalsigma);
        end

        if isfield(inout,'marker_channel_index')
            data.marker_working_image=sq_data_marker;
            if num_zs>1 
                switch zproj
                    case 0
                        data.marker_working_image=data.marker_working_image(:,:,str2num(zstack));
                end
            end
        end
        
        if inout.find_central_domain==1
            [data]=find_central_domain(inout,data);
        end
        if inout.find_all_domains==1
            [data]=find_all_domains(inout,data);
        end
        if inout.find_blobs==1
            [data]=find_blobs(inout,data);
        end
        
    case 'lif'
            % trick to compute orthos and find central domain just in 3D
            % images, and store the inout parameter to reset it when 3D
            % images are found
            
            if isfield(inout,'auxmake_orthos')
                inout.make_orthos=inout.auxmake_orthos;
                inout.find_central_domain=inout.auxfind_central_domain;
                inout.select_region=inout.auxselect_region;
            end
            
            if isfield(inout,'open_multiple_lifs')
                if inout.open_multiple_lifs==1 
                    [imgout]=ci_loadLif(fname, inout.numfiles_to_open-mod(ifiles,inout.numfiles_to_open));
                else
                    [imgout]=ci_loadLif(fname, ifiles);
                end
            else
                [imgout]=ci_loadLif(fname, ifiles);
            end
      
            if isfield(inout,'correct_Zfocus')
                imgout.Info.Dimensions(3).Length=num2str(abs(str2num(imgout.Info.Dimensions(3).Length))*inout.correct_Zfocus);
            end
            data.original_resolution(1,1)=10^6*abs(str2num(imgout.Info.Dimensions(1).Length))/str2num(imgout.Info.Dimensions(1).NumberOfElements);
            data.original_resolution(1,2)=10^6*abs(str2num(imgout.Info.Dimensions(2).Length))/str2num(imgout.Info.Dimensions(2).NumberOfElements);
            data.original_resolution(1,3)=10^6*abs(str2num(imgout.Info.Dimensions(3).Length))/str2num(imgout.Info.Dimensions(3).NumberOfElements);
            
            if isfield(inout,'NonEqual_XYResolution')
                if and(inout.NonEqual_XYResolution==0,data.original_resolution(1,1)~=data.original_resolution(1,2)) 
                    msg="The default pipeline requires the same resolution for X and Y"; 
                    msg=msg+"\n"+"To process images with diferent resolution for X and Y set inout.NonEqual_XYResolution=1 in your script.";
                    msg=msg+"\n"+"This will homogenize all resolutions to the best one using cubic interpolation.";
                    msg=compose(msg);
                    error(msg);
                end
            else
                msg="The default pipeline requires the same resolution for X and Y"; 
                msg=msg+"\n"+"To process images with diferent resolution for X and Y set inout.NonEqual_XYResolution=1 in your script.";
                msg=msg+"\n"+"This will homogenize all resolutions to the best one using cubic interpolation.";
                msg=compose(msg);
                error(msg);
            end
                
            imaux=imgout.Image(inout.signal_channel_index);
            imauxaux=imaux{1};
            
%             imauxaux=permute(imauxaux,[2 1 3]); %To artificially check the other orientation (also uncomment line 586)
                                   
            %Full 3D Homogenization to the best resolution (ONLY USED WHEN XResolution~=YResolution)  
            if and(isfield(inout,'NonEqual_XYResolution'),data.original_resolution(1,1)~=data.original_resolution(1,2))
                if inout.NonEqual_XYResolution==1
                    data.resolution_PreHomogenization=data.original_resolution;
                    HomoRSize=size(imauxaux);  
                    if data.original_resolution(1,1)>min(data.original_resolution) 
                        HomoRSize(1)=HomoRSize(1)*data.original_resolution(1,1)/min(data.original_resolution) ;     
                    end            
                    if data.original_resolution(1,2)>min(data.original_resolution) 
                        HomoRSize(2)=HomoRSize(2)*data.original_resolution(1,2)/min(data.original_resolution) ;
                    end            
                    if data.original_resolution(1,3)>min(data.original_resolution) 
                        HomoRSize(3)=HomoRSize(3)*data.original_resolution(1,3)/min(data.original_resolution) ;
                    end
                    FinalRes=min(data.original_resolution);       
                    imauxaux=imresize3(imauxaux, HomoRSize);
                    data.original_resolution(1,1)=FinalRes;
                    data.original_resolution(1,2)=FinalRes;
                    data.original_resolution(1,3)=FinalRes;
                end
            end
            %--------------------------------------------------------------
                
            %Solve problematic slides
            if isfield(inout,'wrong_slides')
                for s=1:length(inout.wrong_slides)
%                     figure(1)
%                     imshow(mat2gray(imauxaux(:,:,inout.wrong_slides(s))))
                    if inout.wrong_slides(s)==1
                        imauxaux(:,:,inout.wrong_slides(s))=imauxaux(:,:,2);
                    elseif inout.wrong_slides(s)==ndims(imauxaux)
                        imauxaux(:,:,inout.wrong_slides(s))=imauxaux(:,:,ndims(imauxaux)-1);
                    else                     
                        imauxaux(:,:,inout.wrong_slides(s))=(imauxaux(:,:,inout.wrong_slides(s)-1)+imauxaux(:,:,inout.wrong_slides(s)+1))/2;
                    end
%                     figure(2)
%                     imshow(mat2gray(imauxaux(:,:,inout.wrong_slides(s))))
                end
            end
                       
            num_images=ndims(imauxaux);
            %Signal visualization for all channels: %%Ordered to get the
            %cell borders
            OrderedImage=imgout.Image;
            OrderedImage([inout.marker_channel_index_lif 1])=OrderedImage([1 inout.marker_channel_index_lif]);
%             check=OrderedImage(1);
%             see=check{1};
%             figure(1)
%             imshow3D(see);
%             figure(2)
%             imshow3D(imauxaux);
            extim=OrderedImage(1);
            imcell=extim{1}; 
%             imcell=permute(imcell,[2 1 3]); %To artificially check the other orientation  (also uncomment line 525) 
                                     
            HomoRSize=size(imcell);  
            if isfield(inout,'homogeneous_selection')             
                if inout.homogeneous_selection==1
                    if data.original_resolution(1,1)>data.original_resolution(1,3) 
                        HomoRSize(1)=HomoRSize(1)*data.original_resolution(1,1)/data.original_resolution(1,3);
                        HomoRSize(2)=HomoRSize(2)*data.original_resolution(1,1)/data.original_resolution(1,3);
                    else
                        HomoRSize(3)=HomoRSize(3)*data.original_resolution(1,3)/data.original_resolution(1,1);
                    end
                end
            end
            if isfield(inout,'number_of_channels_in_lif')             
                if inout.number_of_channels_in_lif>1
                    extim=OrderedImage(2);
                    imsig2=extim{1};
                else
                    imsig2 = zeros(size(imcell,1,2,3),class(imcell));           
                end
                if inout.number_of_channels_in_lif>2
                    extim=OrderedImage(3);
                    imsig3=extim{1};
                else
                    imsig3 = zeros(size(imcell,1,2,3),class(imcell));
                end
                Grey_imsig2=imadd(mat2gray(imcell),mat2gray(imsig2));
                Grey_imsig2=imresize3(Grey_imsig2, HomoRSize);
                Grey_imsig3=imadd(mat2gray(imcell),mat2gray(imsig3));  
                Grey_imsig3=imresize3(Grey_imsig3, HomoRSize);
            end
            Grey_imcell=mat2gray(imcell);
            Grey_imcell=imresize3(Grey_imcell, HomoRSize);
                       
            if isfield(inout,'create_control_paraboloid') 
                if inout.create_control_paraboloid==1                    
%                     data.original_resolution(1,1)=0.1;
%                     data.original_resolution(1,2)=0.1;
%                     data.original_resolution(1,3)=0.1;
%                     imgout.Info.Dimensions(1).NumberOfElements=num2str(ceil(((10^6)*abs(str2num(imgout.Info.Dimensions(1).Length))/data.original_resolution(1,1))));
%                     imgout.Info.Dimensions(2).NumberOfElements=num2str(ceil(((10^6)*abs(str2num(imgout.Info.Dimensions(2).Length))/data.original_resolution(1,2))));
%                     imgout.Info.Dimensions(3).NumberOfElements=num2str(ceil(((10^6)*abs(str2num(imgout.Info.Dimensions(3).Length))/data.original_resolution(1,3))));
                    PSize=[str2num(imgout.Info.Dimensions(1).NumberOfElements), str2num(imgout.Info.Dimensions(2).NumberOfElements),str2num(imgout.Info.Dimensions(3).NumberOfElements)];
                    [imauxaux]=create_control_paraboloid(inout,data,PSize,inout.control_paraboloid_curvature,inout.control_paraboloid_tilt);
                    imcell=imauxaux;
                    Grey_imcell=imauxaux;
                    Grey_imsig2=imauxaux;
                    Grey_imsig3=imauxaux;
                end
            end            
            
            if isfield(inout,'open_multiple_lifs')
                if inout.open_multiple_lifs==1 
                    data.rootstring_first_image_name=strcat(data.rootstring_first_image_name,'-',imgout.Info.Name);
                else
                    data.rootstring_first_image_name=imgout.Info.Name;   
                end               
            else
                data.rootstring_first_image_name=imgout.Info.Name;                
            end            
                
            if ndims(imauxaux)==2
                %im=imauxaux';  
                im=imauxaux;
                
                inout.auxmake_orthos=inout.make_orthos;
                inout.auxfind_central_domain=inout.find_central_domain;
                inout.auxselect_region=inout.select_region;

                inout.make_orthos=0;
                
                % to modify following parameter in the central domain
                % pipeline
                %if inout.find_central_domain==1 
                %   inout.select_region=0;
                %end
                %inout.find_central_domain=0;
            elseif  ndims(imauxaux)==3  
                num_xys=size(imauxaux,1);
                data.xys_image=num_xys;  
                num_zs=size(imauxaux,3);
                data.zs_image=num_zs;
                
                if inout.work_on_orthos==1
                    [imauxaux,imgout.Info]=work_on_orthos(inout,imcell,imauxaux,imgout.Info);
                    data.orientation=imgout.Info.Orientation;
                    
                    Grey_imcell=permute(Grey_imcell,[str2num(imgout.Info.Dimensions(1).DimID) str2num(imgout.Info.Dimensions(2).DimID) str2num(imgout.Info.Dimensions(3).DimID)]);
                    HomoRSize=size(Grey_imcell);
                    if isfield(inout,'number_of_channels_in_lif')  
                        Grey_imsig2=permute(Grey_imsig2,[str2num(imgout.Info.Dimensions(1).DimID) str2num(imgout.Info.Dimensions(2).DimID) str2num(imgout.Info.Dimensions(3).DimID)]);
                        Grey_imsig3=permute(Grey_imsig3,[str2num(imgout.Info.Dimensions(1).DimID) str2num(imgout.Info.Dimensions(2).DimID) str2num(imgout.Info.Dimensions(3).DimID)]);
                    end                          
                end                
                       
                if isfield(inout,'number_of_channels_in_lif')  
                    auxrgb={Grey_imsig3,Grey_imsig2,Grey_imcell};                
                    imacol=cat(4,auxrgb{:});
                end
                                
                if isfield(inout,'double_parabola')  
                    if inout.double_parabola==1 
                        if inout.work_on_orthos==1
                            X=[1 3 2];                 
                        else
                            [X]=find_certain_ortho(inout,imcell);
                            if(X(1)==3)           
                                data.orientation='Horizontal';
                                X=[3 2 1];
                            else
                                data.orientation='Vertical'; 
                                X=[1 3 2];       
                            end
                        end
                        ortho_side=permute(imauxaux,X);
                        Grey_imcell_side=permute(Grey_imcell,X);
                        if isfield(inout,'number_of_channels_in_lif')  
                            Grey_imsig2_side=permute(Grey_imsig2,X);
                            Grey_imsig3_side=permute(Grey_imsig3,X);                            
                            auxrgb_side={Grey_imsig3_side,Grey_imsig2_side,Grey_imcell_side};                
                            imacol_side=cat(4,auxrgb_side{:});
                        end                    
                    end  
                end                         
                
                inout.analysis_in_zs=1; % for current LIF pipeline    
                if and(inout.zsum==0,inout.zmax==1) 
                    zproj=1;
                elseif and(inout.zsum==1,inout.zmax==0)
                    zproj=2;
                else
                    zproj=0;
                end
                data.zmax=max(imauxaux,[],3);
                data.zsum=sum(imauxaux,3);  
                if inout.analysis_in_zs==1
                    data.temp_zstacks=imauxaux;
                end
                switch zproj 
                    case 0
                        auxx=0; % this is introduced in case previous data did not record the selected z
                        if inout.get_polygon_from_previous_data==1
                            if inout.export_subfolderdatapath==1
                                outdatapath=fullfile(datapath,data.rootstring_first_image_name);
                                data.outdatapath=deblank(outdatapath);
                                create_dir(data.outdatapath)
                            end 
                            ex=exist(fullfile(data.outdatapath,[inout.data_structure_filename '.mat']));
                            if ex==0 
                                msg=['No data.mat file found for ',data.outdatapath,', masks of your regions could not be reused.']; 
                                error(msg);
                            end
                            dataaux=import_data(data.outdatapath,inout.data_structure_filename);
                            if isfield(dataaux,'zstack')
                                data.zstack=dataaux.zstack;
                                auxx=1;
                            end
                            if inout.zmax_sandwich==1
                                im=imauxaux(:,:,str2num(extractBefore(data.zstack,'-')):str2num(extractAfter(data.zstack,'-'))); 
                                im=max(im,[],3);
                            else
                                im=imauxaux(:,:,str2num(data.zstack));   
                            end
                            
                            if isfield(dataaux,'zstack_ortho_side')
                                data.zstack_ortho_side=dataaux.zstack_ortho_side;
                                auxx=1;
                            end
                            if inout.zmax_sandwich==1
                                im_side=ortho_side(:,:,str2num(extractBefore(data.zstack_ortho_side,'-')):str2num(extractAfter(data.zstack_ortho_side,'-'))); 
                                im_side=max(im_side,[],3);
                            else
                                im_side=ortho_side(:,:,str2num(data.zstack_ortho_side));   
                            end
                        end
                        if or(inout.get_polygon_from_previous_data==0,auxx==0)
                            figure('Position',[800 2000 1080 1080])
                            imshow3Dfull(imacol);
                            if inout.zmax_sandwich==1
                                % Select the time interval you wish to
                                % project
                                disp('Select the time interval you wish to zmax project')
                                prompt = {'Select initial z:','Select last z:'};
                                dlg_title = 'Zproject';
                                
                                defaultanswer = {num2str(floor(0.45*HomoRSize(3))),num2str(floor(0.55*HomoRSize(3)))};
                                ResConv=size(imauxaux,3)/HomoRSize(3);
                                
                                options.WindowStyle='normal';
                                options.Interpreter='tex';   
                                answer = inputdlg(prompt, dlg_title,[1 27], defaultanswer, options);   
                                answer{1}=num2str(round(str2num(answer{1})*ResConv)); 
                                answer{2}=num2str(round(str2num(answer{2})*ResConv));
                                data.zstack=strcat(answer{1},'-',answer{2});
                                im=imauxaux(:,:,str2num(answer{1}):str2num(answer{2})); 
                                im=max(im,[],3);
%                                 figure();
%                                 imshow(im);
                                close all;
                                
                                if isfield(inout,'double_parabola')  
                                    if inout.double_parabola==1
                                        figure('Position',[800 2000 1080 1080])
                                        imshow3Dfull(imacol_side);
                                        % Select the time interval you wish to
                                        % project
                                        disp('Select the time interval you wish to zmax project on the ortho side')
                                        prompt = {'Select initial z:','Select last z:'};
                                        dlg_title = 'Zproject'; 
                                        
                                        if strcmp(data.orientation,'Horizontal') 
                                            defaultanswer = {num2str(floor(0.45*HomoRSize(1))),num2str(floor(0.55*HomoRSize(1)))};
                                            ResConv=size(imauxaux,1)/HomoRSize(1);
                                        else 
                                            defaultanswer = {num2str(floor(0.45*HomoRSize(2))),num2str(floor(0.55*HomoRSize(2)))};
                                            ResConv=size(imauxaux,2)/HomoRSize(2);
                                        end
                                        
                                        options.WindowStyle='normal';
                                        options.Interpreter='tex';  
                                        answer = inputdlg(prompt, dlg_title,[1 27], defaultanswer, options);  
                                        answer{1}=num2str(round(str2num(answer{1})*ResConv)); 
                                        answer{2}=num2str(round(str2num(answer{2})*ResConv));
                                        data.zstack_ortho_side=strcat(answer{1},'-',answer{2});
                                        im_side=ortho_side(:,:,str2num(answer{1}):str2num(answer{2})); 
                                        im_side=max(im_side,[],3);
%                                         figure();
%                                         imshow(im_side);
                                        close all;
                                    end
                                end                                        
                            else                                
                                % Select the time interval you wish to focus on
                                prompt = {'Choose slide'};
                                dlg_title = 'Select z';

                                defaultanswer = {num2str(floor(0.5*HomoRSize(3)))};
                                ResConv=size(imauxaux,3)/HomoRSize(3);
                                
                                options.WindowStyle='normal';
                                options.Interpreter='tex';        
                                answer = inputdlg(prompt, dlg_title,[1 27], defaultanswer, options); 
                                answer = num2str(round(str2num(answer)*ResConv)); 
                                data.zstack=answer{1};
                                im=imauxaux(:,:,str2num(data.zstack)); 
                                close all;
                                if isfield(inout,'double_parabola')  
                                    if inout.double_parabola==1
                                        figure('Position',[800 2000 1080 1080])
                                        imshow3Dfull(imacol_side);
                                        % Select the time interval you wish to
                                        % project
                                        prompt = {'Choose slide'};
                                        dlg_title = 'Select z';   
                                        
                                        if strcmp(data.orientation,'Horizontal') 
                                            defaultanswer = {num2str(floor(0.5*HomoRSize(1)))};
                                            ResConv=size(imauxaux,1)/HomoRSize(1);
                                        else 
                                            defaultanswer = {num2str(floor(0.5*HomoRSize(2)))};
                                            ResConv=size(imauxaux,2)/HomoRSize(2);
                                        end
                                        
                                        options.WindowStyle='normal';
                                        options.Interpreter='tex';     
                                        answer = inputdlg(prompt, dlg_title,[1 27], defaultanswer, options);  
                                        answer = num2str(round(str2num(answer)*ResConv)); 
                                        data.zstack_ortho_side=answer{1};
                                        im_side=ortho_side(:,:,str2num(data.zstack_ortho_side)); 
                                        close all;
                                    end
                                end
                            end
                        end
                    case 1
                        im=data.zmax; 
                    case 2
                        im=data.zsum;
                end % end of what to do with the stack projection
                if inout.make_orthos==1 
                    if inout.work_on_orthos==1
                        if strcmp(data.orientation,'Vertical')
                            data.ortho{1}=permute(imauxaux,[3 1 2]);
                            data.ortho{2}=permute(imauxaux,[3 2 1]); 
                        else
                            data.ortho{1}=permute(imauxaux,[2 3 1]);
                            data.ortho{2}=permute(imauxaux,[1 3 2]);
                        end
                    else
                        data.orientation='Horizontal';  
                        data.ortho{1}=permute(imauxaux,[1 3 2]); 
                        data.ortho{2}=permute(imauxaux,[2 3 1]);                        
                    end                                
                end       
            end  
                       
            if inout.work_on_orthos==1                     
                %Working in orthos Z is always in the 2nd position   

                data.resolution(1,1)=10^6*abs(str2num(imgout.Info.Dimensions(1).Length))/str2num(imgout.Info.Dimensions(1).NumberOfElements);
                data.resolution(1,2)=10^6*abs(str2num(imgout.Info.Dimensions(2).Length))/str2num(imgout.Info.Dimensions(2).NumberOfElements);
                data.resolution(1,3)=10^6*abs(str2num(imgout.Info.Dimensions(3).Length))/str2num(imgout.Info.Dimensions(3).NumberOfElements);

                % x/y may have different resolutions from z. In particular, z often has
                % lower resolution. Because of that, we adapt the directions with
                % the best resolution so that all directions have the same resolution.
            
                data.microns_per_pixel=data.original_resolution(1,1); 
                data.microns_square_per_pixel=data.original_resolution(1,1)*data.original_resolution(1,1);
                if data.original_resolution(1,1)>data.original_resolution(1,3)
                    data.microns_per_pixel_orthos=data.original_resolution(1,3); 
                    data.microns_square_per_pixel_orthos=data.original_resolution(1,3)*data.original_resolution(1,3);
                
                    newlength=data.xys_image*data.microns_per_pixel/data.microns_per_pixel_orthos;
                    if strcmp(inout.defaultregiontype,'Polygon') 
                        imagesize=[data.zs_image newlength];
                    else    
                        if strcmp(data.orientation,'Horizontal')
                            imagesize=[data.zs_image newlength];
                        else
                            imagesize=[newlength data.zs_image];
                        end
                    end
                else
                    data.microns_per_pixel_orthos=data.microns_per_pixel; 
                    data.microns_square_per_pixel_orthos=data.microns_square_per_pixel;
                
                    newlength=data.zs_image*data.original_resolution(1,3)/data.microns_per_pixel;
                    if strcmp(inout.defaultregiontype,'Polygon') 
                        imagesize=[newlength data.xys_image];
                    else    
                        if strcmp(data.orientation,'Horizontal')
                            imagesize=[newlength data.xys_image];
                        else
                            imagesize=[data.xys_image newlength];
                        end
                    end
                 end
                
                data.resolution(1,1)=data.microns_per_pixel_orthos;
                data.resolution(1,2)=data.microns_per_pixel_orthos;
                data.resolution(1,3)=data.microns_per_pixel;
                
                im=imresize(im, imagesize);
                if isfield(data,'zmax')
                    data.zmax=imresize(data.zmax, imagesize);
%                     inout.max_normalised_level_to_show=0.8;
                end
                if isfield(data,'zsum')
                    data.zsum=imresize(data.zsum, imagesize); 
%                     inout.max_normalised_level_to_show=0.8;                   
                end                
                if isfield(inout,'double_parabola')  
                    if inout.double_parabola==1  
                        im_side=imresize(im_side, imagesize);
                    end
                end                   
                    
                %pending: check the radiusstep is getting the right resolution
                %on the right plane
                inout.radiusstep=inout.radiusstep_in_microns/data.microns_per_pixel_orthos;
                data.radiusstep=inout.radiusstep; 
           else  
                data.resolution(1,1)=10^6*abs(str2num(imgout.Info.Dimensions(1).Length))/str2num(imgout.Info.Dimensions(1).NumberOfElements);
                data.resolution(1,2)=10^6*abs(str2num(imgout.Info.Dimensions(2).Length))/str2num(imgout.Info.Dimensions(2).NumberOfElements);

                if ndims(imauxaux)==3
                    data.resolution(1,3)=10^6*abs(str2num(imgout.Info.Dimensions(3).Length))/str2num(imgout.Info.Dimensions(3).NumberOfElements);
                else
                    data.resolution(1,3)=NaN;
                end
                
                data.microns_per_pixel=data.resolution(1,1);
                data.microns_square_per_pixel=data.resolution(1,1)*data.resolution(1,2);
                if size(data.resolution,2)==3
                    if data.resolution(1,1)>data.resolution(1,3)
                        data.microns_per_pixel_orthos=data.resolution(1,3); 
                        data.microns_square_per_pixel_orthos=data.resolution(1,3)*data.resolution(1,3);
                        newlength=data.xys_image*data.microns_per_pixel/data.microns_per_pixel_orthos;     
                        if strcmp(data.orientation,'Horizontal')
                            imagesize=[data.zs_image newlength];
                        else
                            imagesize=[newlength data.zs_image];
                        end
                    else
                        data.microns_per_pixel_orthos=data.microns_per_pixel; 
                        data.microns_square_per_pixel_orthos=data.microns_square_per_pixel;
                        newlength=data.zs_image*data.resolution(1,3)/data.microns_per_pixel;
                        if strcmp(data.orientation,'Horizontal')
                            imagesize=[newlength data.xys_image];
                        else
                            imagesize=[data.xys_image newlength];
                        end
                    end
                end  
                %pending: check the radiusstep is getting the right resolution
                %on the right plane
                inout.radiusstep=inout.radiusstep_in_microns/data.resolution(1,1);
                data.radiusstep=inout.radiusstep;
                                              
                if isfield(inout,'double_parabola')  
                    if inout.double_parabola==1   
                        im_side=imresize(im_side, imagesize);  
                    end
                end                     
            end
            data.selected_working_image=im;     
                                          
            if isfield(inout,'double_parabola')  
                if inout.double_parabola==1
                    data.selected_working_image_side=im_side;
                end
            end
          
            if inout.find_central_domain==1
                [data]=find_central_domain(inout,data);
                close all;
            end
            if inout.export_subfolderdatapath==1
                outdatapath=fullfile(datapath,data.rootstring_first_image_name);                    
                if outdatapath(:,end)==' '
                    outdatapath=outdatapath(:,1:end-1); % correcting those paths ending with a space
                end
                data.outdatapath=outdatapath;
                create_dir(outdatapath)
            end            
end % end format

switch  inout.standard_format_filename
    case 1
        ini_id = regexp(first_im_name,'_w[^_]*_s\d*_t\d*(\.tif|\.TIF)', 'match');
        ini_id_loc = regexp(first_im_name, '_w[^_]*_s\d*_t\d*(\.tif|\.TIF)', 'start');
        ini_t=strfind(ini_id{1},'_t') + ini_id_loc - 1;
        fin_t=strfind(ini_id{1},'.TIF') + ini_id_loc - 2;
        ini_w=strfind(ini_id{1},'_w') +  ini_id_loc - 1;
        ini_s=strfind(ini_id{1},'_s')  + ini_id_loc;
        stagepos=first_im_name(ini_s+1:ini_t-1);

        data.stagepos=str2num(stagepos);

        basename=first_im_name(1:ini_w-1);
        inout.basename=basename;

        twochannels_string=strcat(first_im_name(1:ini_w),first_im_name(ini_s:ini_t-1));

        data.twochannels_string=twochannels_string;

        selected_time=first_im_name(ini_t+2:fin_t);

        data.rootstring_wo_time_first_im_name=first_im_name(1:ini_t+1);

        ini_chan=ini_w+2;
        chosenchannel=first_im_name(ini_chan:ini_s-2);

        [signal_channel_index,marker_channel_index,otherchannel,data]=get_channels_info(inout,data,chosenchannel); 
        inout.marker_channel_index=marker_channel_index;
    %    if isfield(inout,'marker_channel_index')
        if inout.number_tiff_channels==2
            second_im_name=strcat(first_im_name(1:ini_chan-1),otherchannel,first_im_name(ini_s-1:end));

            fname2=fullfile(datapath,second_im_name);
            disp(fname2);

            if num_images>1   % tiff z-stack
                info = imfinfo(fname2);
                %info = imfinfo(fname);

                for k = 1:num_images
                    A(:,:,k) = imread(fname2, k, 'Info', info);
                end
                if inout.focus_on_single_z_tiff==0
                        if inout.tiff_max_int==1
                            imbis=max(A,[],3);
                        else
                               msg='Pipeline to finish when two channels are shown and not Max int projection is selected.'; 
                               error(msg);
                        end
                else
                        imbis=A(:,:,str2num(zstack));
                end

            else
                imbis=imread(fname2);
            end

            gimbis=mat2gray(imbis);
            if inout.show_im_figs==1
                f2=figure();
                grgimbis=imshow(gimbis,[],'InitialMagnification',inout.magnification);
            end 
        end

        if marker_channel_index==1
            if isfield(inout,'marker_channel_index')
                marker_image=im;
            end
            if inout.number_tiff_channels==2
                signal_image=imbis;
            end
        else
            if inout.number_tiff_channels==2
                marker_image=imbis;
            end
            signal_image=im;
        end

        imm=zeros(size(im,1),size(im,2),3);
        if inout.number_tiff_channels==2
            imm(:,:,1)=mat2gray(marker_image); 
        end
        imm(:,:,2)=mat2gray(signal_image);
        %imm(:,:,3)=mat2gray(marker_image);
        if inout.show_im_figs==1
            f3=figure();
            is=imshow(imm,'InitialMagnification',inout.magnification);
        end
        string_firstchannel=first_im_name(1:ini_t+1);

        if inout.number_tiff_channels==2
            ini_id2 = regexp(second_im_name, '_w[^_]*_s\d*_t\d*(\.tif|\.TIF)', 'match');
            ini_id_loc2 = regexp(second_im_name, '_w[^_]*_s\d*_t\d*(\.tif|\.TIF)', 'start');
            ini_t2=strfind(ini_id2{1},'_t') + ini_id_loc2 - 1;
            string_secondchannel=second_im_name(1:ini_t2+1);

            strings={string_firstchannel string_secondchannel};
            data.stringmarker=strings{marker_channel_index};

        else
            strings={string_firstchannel};

        end
      %  xx=length(chosenchannel)-length(otherchannel);
      %  string_secondchannel=second_im_name(1:ini_t+1-xx);

        data.stringsignal=strings{signal_channel_index};

    case 2
        ini_id = regexp(first_im_name,'_w*s*t*(\.tif|\.TIF)', 'match');
        ini_t=strfind(first_im_name,'_t');
        data.rootstring_wo_time_first_im_name=first_im_name(1:ini_t+1);

        strings={first_im_name(1:ini_t+1)};

    case 3
        % in process
        ini_id = regexp(first_im_name,'_w*(\.tif|\.TIF)', 'match');
        filenamelength=size(first_im_name,2);
        ini_w=strfind(first_im_name,'_w'); 
        basename=first_im_name(1:ini_w-1);
        inout.basename=basename;
        string_firstchannel=first_im_name(1:filenamelength-4);
        %first_im_name(ini_w+2:filenamelength-4)
        chosenchannel=first_im_name(ini_w+2:filenamelength-4);
        [signal_channel_index,marker_channel_index,otherchannel,data]=get_channels_info(inout,data,chosenchannel);         
         
        second_im_name=strcat(first_im_name(1:ini_w),'w',otherchannel,'.tif');
        fname2=fullfile(datapath,second_im_name);

        string_secondchannel=second_im_name(1:filenamelength-4);

        strings={string_firstchannel string_secondchannel};

        filestring=strings{marker_channel_index};
        name_marker=strcat(filestring,'.tif');

        info = imfinfo(fullfile(datapath,name_marker));
        num_images = numel(info);
        
        if num_images==1
            im=imread(fname);
            image_marker=im;
            if inout.number_tiff_channels==2
                imbis=imread(fname2);
                image_signal=imbis;
            end
        end
        
    otherwise
        data.selected_channel_string='default_channel';
end  % end standard filename 

% Note the following is also defined in analyse_data
if isfield(inout,'selected_channel_string')
    data.selected_channel_string=inout.selected_channel_string;
end

if isfield(inout,'find_radial_distance')
    if inout.find_radial_distance==1
        [data]=find_radial_distance(inout,data,imcell,imsig2,imsig3);
    end
end

% Note data.first_image is the image which is going to be analysed. 

% For LSM files, note this will probably not be raw data, but in meristem routine, 
% it will be a selected channel, projected or not, and gaussian filteres or
% not.find

% data.selected_working_image and im here are the same; or make sure they are

data.image_size=size(im);
data.first_image=im; 
if length(data.image_size)==2
    if inout.normal_normalize==1
        gim=mat2gray(im);
    else
        mxmx=max(max(im));
        gim=im/mxmx;
    end
else                   
    gim=mat2gray(max(im,[],3));
end


if inout.show_im_figs==1
    f1=figure();
    imshow(gim,'InitialMagnification',inout.magnification)
    
    if data.image_size(1)<data.image_size(2)  
        movegui('north')
    else
        movegui('west') 
    end
    if inout.show_label_image==1
        [HeightA,width,~] = size(gim);
        T1Pos = round(get(f1,'Position')); %// Get the position
        hT1_2 = text(T1Pos(1)+80,HeightA-20 ,data.rootstring_first_image_name,'HorizontalAlignment','center','Color','white'); %// Place the text
    end
end

if ~strcmp(format,'lif') %Already formated for lif files (breaks Work_in_Ortho)
    data.microns_per_pixel=1;   % Default 
    if isfield(data,'resolution')
        data.microns_per_pixel=data.resolution(1,1); 
        data.microns_square_per_pixel=data.resolution(1,1)*data.resolution(1,2);
        if size(data.resolution,2)==3
            data.microns_per_pixel_orthos=data.resolution(1,3);
            data.microns_square_per_pixel_orthos=data.resolution(1,1)*data.resolution(1,3); 
        end
    end
end

if inout.select_background_regions==1
    data.backgroundregionlist=[];
    if (exist('imm')==1)
        image=imm;
        fanalysis=f3;
    else
        image=zeros(size(im,1),size(im,2),3);
        if size(size(im))==2
            image(:,:,1)=mat2gray(im);
            image(:,:,2)=mat2gray(im);
        else
        	image(:,:,1)=mat2gray(max(im,[],3));
            image(:,:,2)=mat2gray(max(im,[],3));
        end
        
        fanalysis=figure();
%         is=imshow(image,'InitialMagnification',inout.magnification);
    end

    if (inout.click_selecting_regions==0)
        if inout.get_center_and_radius_from_previous_data==1 %            
            ex=exist(fullfile(data.outdatapath,[inout.data_structure_filename '.mat']));
            if ex==0 
               msg=['No data.mat file found for',data.outdatapath,', the origin and radius of your regions could not be reused.']; 
               error(msg);
            end
            dataaux=import_data(data.outdatapath,inout.data_structure_filename);
            for ij=1:size(dataaux.region,2)
                inout.tmp_background_region_pars{ij}.pos0=dataaux.regionbackground{ij}.origin;
                inout.tmp_background_region_pars{ij}.radius=dataaux.regionbackground{ij}.radius;
            end
        end        
    end    
    
    [data]=selecting_regions_for_background(inout,data,image,fanalysis);
    data.first_image_backgr_sub=data.first_image-data.regionbackground{1}.backgroundlevel;    
end


if isfield(data,'regionlist')==0
    data.regionlist=[];
end

if inout.select_region==1
    data.regionlist=[];
    if (exist('imm')==1)
        image=imm;
        fanalysis=f3;
    else
        image=zeros(size(im,1),size(im,2),3);
        auxs=min(size(inout.format,2),size('tiffrgb',2));
        format2='tiffrgb';
        if mean(inout.format(1:auxs)==format2(1:auxs))==1
            image=im;
        else               
            image(:,:,1)=mat2gray(im);
            image(:,:,2)=mat2gray(im);
            if isfield(data,'marker_working_image')
                if size(data.marker_working_image,3)==1
                 image(:,:,1)=mat2gray(data.marker_working_image);
                end
            else
                image(:,:,1)=mat2gray(im);                
            end
        end
        if ~isfield(inout,'min_normalised_level_to_show')
            inout.min_normalised_level_to_show=0;
        end    
               
        
        fanalysis=figure();
        if inout.imshow_in_fire==0
            is=imshow(image,[inout.min_normalised_level_to_show,inout.max_normalised_level_to_show],'InitialMagnification',inout.magnification);   %title(data.rootstring_first_image_name);
        elseif inout.imshow_in_fire==1
            %is=imshow(image(:,:,1),'Colormap',morgenstemning);
            %is=imshow(imadjust(image(:,:,1)),[0,inout.max_normalised_level_to_show],'Colormap',morgenstemning,'InitialMagnification',inout.magnification);
            if inout.enhance_contrast==1     
                if inout.zmax_sandwich==1           
                    image(:,:,1)=mat2gray(immultiply(im,2)); 
                else
                    image(:,:,1)=mat2gray(immultiply(im,3)); 
                end                    
                is=imshow(histeq(image(:,:,1)),[inout.min_normalised_level_to_show,inout.max_normalised_level_to_show],'Colormap',ametrine,'InitialMagnification',inout.magnification);       
            else
                if max(max(image))>1
                    is=imshow(mat2gray(image(:,:,1)),[inout.min_normalised_level_to_show,inout.max_normalised_level_to_show],'Colormap',ametrine,'InitialMagnification',inout.magnification);

                else
                    is=imshow(image(:,:,1),[inout.min_normalised_level_to_show,inout.max_normalised_level_to_show],'Colormap',ametrine,'InitialMagnification',inout.magnification);
                end
            end            
        end        
        if inout.show_label_image==1
            [HeightA,width,~] = size(image);
            T1Pos = round(get(fanalysis,'Position')); %// Get the position
            hT1_2 = text(T1Pos(1)+80,HeightA-20 ,data.rootstring_first_image_name,'HorizontalAlignment','center','Color','white'); %// Place the text
        end
    end
    
    if (inout.click_selecting_regions==0)
        if inout.get_center_and_radius_from_previous_data==1 %
            ex=exist(fullfile(data.outdatapath,[inout.data_structure_filename '.mat']));
            if ex==0 
               msg=['No data.mat file found for';data.outdatapath,', the origin and radius of your regions could not be reused.']; 
               error(msg);
            end
            dataaux=import_data(data.outdatapath,inout.data_structure_filename);
            inout.defaultnumberregions=size(dataaux.region,2); % getting number of regions
            
            for ij=1:size(dataaux.region,2)
                inout.tmp_region_pars{ij}.pos0=dataaux.region{ij}.pos0;
                inout.tmp_region_pars{ij}.radius=dataaux.region{ij}.radius;
            end
        end
        
        if isfield(inout,'get_polygon_from_previous_data')
            if inout.get_polygon_from_previous_data==1 %
                ex=exist(fullfile(data.outdatapath,[inout.data_structure_filename '.mat']));
                if ex==0 
                   msg=['No data.mat file found for ',data.outdatapath,', masks of your regions could not be reused.']; 
                   error(msg);
                end
                dataaux=import_data(data.outdatapath,inout.data_structure_filename);
                for ij=1:size(dataaux.region,2)
                    inout.tmp_region_pars{ij}.regionmask=dataaux.region{ij}.regionmask;
                end
                if isfield(dataaux.region{1},'class')
                    for ij=1:size(dataaux.region,2)
                           inout.tmp_region_pars{ij}.class=dataaux.region{ij}.class;                  
                    end
                end               
                if isfield(dataaux.region{1},'pos0')
                    for ij=1:size(dataaux.region,2)
                           inout.tmp_region_pars{ij}.pos0=dataaux.region{ij}.pos0;                  
                    end
                end
                if isfield(dataaux.region{1},'pos1')
                    for ij=1:size(dataaux.region,2)
                           inout.tmp_region_pars{ij}.pos1=dataaux.region{ij}.pos1;                  
                    end
                end
                if isfield(dataaux.region{1},'pos2')
                    for ij=1:size(dataaux.region,2)
                           inout.tmp_region_pars{ij}.pos2=dataaux.region{ij}.pos2;                  
                    end
                end

            end
        end
        
        if inout.get_center_from_cental_domain==1
            disp('The origin of the central domain is going to be set automatically')
        end
    end
    
    % fanalysis is the figure over which we will select the ROIs
    [data]=selecting_regions(inout,data,image,fanalysis);
    close all;
    if isfield(inout,'double_parabola')  
        if inout.double_parabola==1 
            data.image_side_size=size(im_side);
            data.first_image_side=im_side; 
            if inout.show_im_figs==1
                f1=figure();
                imshow(mat2gray(im_side),'InitialMagnification',inout.magnification); 
                if data.image_side_size(1)<data.image_side_size(2)  
                    movegui('north')
                else
                    movegui('west') 
                end
                if inout.show_label_image==1
                    [HeightA,width,~] = size(im_side);
                    T1Pos = round(get(f1,'Position')); %// Get the position
                    hT1_2 = text(T1Pos(1)+80,HeightA-20 ,[data.rootstring_first_image_name '\_ortho\_side'],'HorizontalAlignment','center','Color','white'); %// Place the text
                end
            end            
            image_side=zeros(size(im_side,1),size(im_side,2),3);
            if mean(inout.format(1:auxs)==format2(1:auxs))==1
                image_side=im_side;
            else               
                image_side(:,:,1)=mat2gray(im_side);
                image_side(:,:,2)=mat2gray(im_side);
                if isfield(data,'marker_working_image')
                    if size(data.marker_working_image,3)==1
                     image_side(:,:,1)=mat2gray(data.marker_working_image);
                    end
                else
                    image_side(:,:,1)=mat2gray(im_side);                
                end
            end
            fanalysis=figure();
            if inout.imshow_in_fire==0
                is=imshow(image_side,[inout.min_normalised_level_to_show,inout.max_normalised_level_to_show],'InitialMagnification',inout.magnification);   %title(data.rootstring_first_image_name);
            elseif inout.imshow_in_fire==1
                %is=imshow(image(:,:,1),'Colormap',morgenstemning);
                %is=imshow(imadjust(image(:,:,1)),[0,inout.max_normalised_level_to_show],'Colormap',morgenstemning,'InitialMagnification',inout.magnification);
                if inout.enhance_contrast==1                
                    if inout.zmax_sandwich==1           
                        image_side(:,:,1)=mat2gray(immultiply(im_side,2));   
                    else
                        image_side(:,:,1)=mat2gray(immultiply(im_side,3));  
                    end
                    is=imshow(histeq(image_side(:,:,1)),[inout.min_normalised_level_to_show,inout.max_normalised_level_to_show],'Colormap',ametrine,'InitialMagnification',inout.magnification);       
                else
                    is=imshow(image_side(:,:,1),[inout.min_normalised_level_to_show,inout.max_normalised_level_to_show],'Colormap',ametrine,'InitialMagnification',inout.magnification);
                end            
            end        
            if inout.show_label_image==1
                [HeightA,width,~] = size(image_side);
                T1Pos = round(get(fanalysis,'Position')); %// Get the position
                hT1_2 = text(T1Pos(1)+80,HeightA-20 ,data.rootstring_first_image_name,'HorizontalAlignment','center','Color','white'); %// Place the text
            end
            [data]=selecting_regions(inout,data,image_side,fanalysis);
            close all;           
        end
    end
    

    if inout.make_snap_all_regions==1
        make_snap_all_regions(inout,data);
        close all;
    end
    if inout.make_snap_with_numbered_centroid_regions==1 
        make_snap_with_numbered_centroid_regions_of_working_image(inout,data);
        close all;
    end

    if inout.make_orthos==1
        [data]=make_orthos(inout,data);
        [data]=process_orthos(inout,data);
    end    
end

if inout.select_background_regions==1
    if inout.make_orthos==1
        for ii=1:2
            data.ortho_planes{ii}.sum_backgr_sub=data.ortho_planes{ii}.sum-data.regionbackground{1}.backgroundlevel_zsum;
            data.ortho_planes{ii}.max_backgr_sub=data.ortho_planes{ii}.max-data.regionbackground{1}.backgroundlevel_zmax;
        end      
    end
end

if inout.make_snap_of_working_images==1 
    make_snap_of_working_image(inout,data);
    close all;
end
    
if inout.export_working_image_to_tiff==1 
    export_working_image_to_tiff(inout,data);
end

if inout.make_orthos==1
    [data]=intensity_in_orthos(inout,data);
end

if inout.find_background_through_histogram_peak==1
    [inout,data]=get_background_through_histogram_peak(inout,data); % note it will override inout.thresh_fluor_marker and inout.thresh_fluor_signal
end

if inout.timecourse==1
    if inout.select_time_window==1
        % Select the time interval you wish to focus on
        if and(isfield(inout,'ini'),isfield(inout,'fin'))
            t0=num2str(inout.ini);
            tfin=num2str(inout.fin);
        else
            prompt = {'Initial time', 'Final time'};
            dlg_title = 'Select time interval';
            num_lines = 2;
            defaultanswer = {'1',selected_time};
            options.WindowStyle='normal';
            options.Interpreter='tex';
            answer = inputdlg(prompt, dlg_title, num_lines, defaultanswer, options);
            t0=answer{1};
            tfin=answer{2};
        end
    else
        Dtif_times =dir(fullfile(datapath,strcat(data.rootstring_wo_time_first_im_name,'*.tif')));
        tfin=num2str(length(Dtif_times));
        t0='1';
    end
    
    ts=str2num(t0):str2num(tfin);
    time_frames=length(ts);
    data.time_frames=time_frames;
else
    time_frames=1;
    ts='0';
end

size_data=size(im);

if inout.timecourse==1
    ind=0; 
    if inout.reading_waitbar==1
        h= waitbar(0,'Reading data'); % generates a waitbar
    end

    if exist('fluor_marker_timecourse') % in case of exploring the whole directory, delete the following variables so that for each position a new variable is created
        clear('fluor_marker_timecourse');
        clear('fluor_signal_timecourse');
    end

    for k=1:time_frames
        if inout.reading_waitbar==1
           waitbar(1.0*ind/time_frames); ind=ind+1; 
        end
        timepoint=num2str(ts(k));

        if num_images>1 
            if inout.focus_on_single_z_tiff==1
                if inout.smooth_in_z==1
                    filestring=strings{marker_channel_index};
                    name_marker=strcat(filestring,timepoint,'.TIF');
                    info = imfinfo(fullfile(datapath,name_marker));

                    numzs=inout.num_zs_for_smoothing;
                    bottomz=numzs/2-0.5;
                    clear('A')
                    for uy = 1:numzs
                        A(:,:,uy) = imread(fullfile(datapath,name_marker), str2num(zstack)-1-bottomz+uy, 'Info', info);
                    end
                    %image_marker=max(A,[],3);
                    image_marker=uint16(mean(A,3));

                    filestring=strings{signal_channel_index};  
                    name_signal=strcat(filestring,timepoint,'.TIF');
                    info = imfinfo(fullfile(datapath,name_signal));

                    clear('A')
                    for uy = 1:numzs
                        A(:,:,uy) = imread(fullfile(datapath,name_signal), str2num(zstack)-1-bottomz+uy, 'Info', info);
                    end
                    %image_signal=max(A,[],3);
                    image_signal=uint16(mean(A,3));

                else
                    filestring=strings{marker_channel_index};
                    name_marker=strcat(filestring,timepoint,'.TIF');
                    info = imfinfo(fullfile(datapath,name_marker));
                    image_marker=imread(fullfile(datapath,name_marker),str2num(zstack), 'Info', info);

                    filestring=strings{signal_channel_index};  
                    name_signal=strcat(filestring,timepoint,'.TIF');
                    info = imfinfo(fullfile(datapath,name_signal));
                    image_signal=imread(fullfile(datapath,name_signal), str2num(zstack), 'Info', info);
                end
            else
                if inout.tiff_max_int==1
                    filestring=strings{signal_channel_index};  
                    name_signal=strcat(filestring,timepoint,'.TIF');
                    info = imfinfo(fullfile(datapath,name_signal));

                    for kk = 1:num_images
                        A(:,:,kk) = imread(fullfile(datapath,name_signal), kk, 'Info', info);
                    end
                    image_signal=max(A,[],3);

                    if inout.number_tiff_channels==2
                        filestring=strings{marker_channel_index};
                        name_marker=strcat(filestring,timepoint,'.TIF');
                        info = imfinfo(fullfile(datapath,name_marker));

                        for kk = 1:num_images
                            A(:,:,kk) = imread(fullfile(datapath,name_marker), kk, 'Info', info);
                        end

                        image_marker=max(A,[],3);

                        if inout.tiff_make_zmean_for_marker==1
                            image_marker=mean(A,3);
                        end
                    end
                end
            end % end for a single particular z-stack vs max int proj
        else % in case we don't have z-stacks
            if inout.standard_format_filename==1
                filestring=strings{signal_channel_index};  
                name_signal=strcat(filestring,timepoint,'.TIF');
                image_signal=imread(fullfile(datapath,name_signal));

                if isfield(inout,'marker_channel_index')
                    filestring=strings{marker_channel_index};
                    name_marker=strcat(filestring,timepoint,'.TIF');
                    image_marker=imread(fullfile(datapath,name_marker));
                end

            elseif inout.standard_format_filename==2
                filestring=strings{1};
                name_signal=strcat(filestring,timepoint,'.TIF');
                image_signal=imread(fullfile(datapath,name_signal));
            end
        end
        if inout.standard_format_filename==1
            fluor_signal_timecourse(:,:,1,k)=image_signal(:,:);
            if inout.number_tiff_channels==2
                fluor_marker_timecourse(:,:,1,k)=image_marker(:,:);
            end
        else
            fluor_signal_timecourse(:,:,:,k)=image_signal(:,:,:);
        end
    end % end of the time loop

    % for non time-course tiffs with format 3
    if inout.median_filter==1
        for k=1:time_frames
            fluor_signal_timecourse(:,:,1,k)=medfilt2(fluor_signal_timecourse(:,:,1,k),'symmetric',[5 5]);
            if isfield(inout,'marker_channel_index')
                fluor_marker_timecourse(:,:,1,k)=medfilt2(fluor_marker_timecourse(:,:,1,k),'symmetric',[5 5]);
            end
        end
    end

    if inout.standard_format_filename==1       
        data.fluor_signal_timecourse=fluor_signal_timecourse;
        if inout.number_tiff_channels==2
            data.fluor_marker_timecourse=fluor_marker_timecourse;
        end
    else
        data.fluor_signal_timecourse=fluor_signal_timecourse;
    end

    if inout.find_timecourse_background_through_histogram_peak==1
        [inout,data]=get_timecourse_background_through_histogram_peak(inout,data); % note it will override inout.thresh_fluor_marker and inout.thresh_fluor_signal
    end

    if inout.threshold_backgroun_subtraction==1
        %data_for_background=load(fullfile(data.datapath,'data_for_background.mat'));
        if inout.load_data_for_background_file==1 
            auxdata=load(fullfile(data.datapath,'data_for_background.mat'));
            data_for_background=auxdata.data;
            inout.thresh_fluor_marker=data_for_background.allregions_average_intensity;
        end
        data.fluor_signal_timecourse=data.fluor_signal_timecourse-inout.thresh_fluor_signal;
        if inout.number_tiff_channels==2 
            data.fluor_marker_timecourse=data.fluor_marker_timecourse-inout.thresh_fluor_marker;
        end
    end

    if inout.crop_images==1
        [data]=crop_images(inout,data);
    end

    if inout.rolling_ball_background_filter==1
        [data]=rollingball_background_subtraction(inout,data);
    end

    if inout.export_renamed_for_schnitzcells==1 % rather for tiff timeseries. Look below to LSM files and other tiffs
        [data]=export_images_for_schnitzcells(inout,data);
    end % end of renaming for schnitzcells

    if inout.reading_waitbar==1
        close(h);
    end

else  % if not timecourse

    if inout.standard_format_filename==3
        data.fluor_marker_timecourse(:,:)=image_marker(:,:);
        if inout.number_tiff_channels==2
            % pending to check other loops for the other formats; note it is assigned the flour marker for the 
            % other cases
            data.fluor_signal_timecourse(:,:)=image_signal(:,:);
        end
    end

    if inout.crop_images==1
        [data]=crop_images(inout,data);
    end

    if inout.rolling_ball_background_filter==1
        [data]=rollingball_background_subtraction(inout,data);
    end

    % for LSM files or tiffs in non-standard format. Look above for standard format tiffs
    if inout.export_working_image_to_tiff_for_schnitzcells==1 
        export_working_image_to_tiff_for_schnitzcells(inout,data);
    end    
end %end of timecourse



data.ts=ts;
if isfield(inout,'hours_per_frame')
    data.hours_per_frame=inout.hours_per_frame;
else
    data.hours_per_frame=1;
end
data.time_points_in_hours=(data.ts-1)*data.hours_per_frame;
data.time_frames=time_frames; % this is the number of time points

%[back_sub_fluor_timecourse]=get_backgr_sub_data(raw_data,background)

if inout.make_movies==1
    if inout.make_2channels_movies==1
        make_2channels_movies_new(inout,data);
    end
    if inout.make_single_channel_movies==1
        make_single_channel_movies(inout,data)
    end
end
if inout.make_RGB_movie==1
    make_RGB_movie(inout,data) % uncomment for the RGB movie
end

if and(inout.analysis_regions==1,inout.analysis_after_reading==0)
    switch inout.analysis_type
        case 'Standard'
            [data]=analysis_regions(inout,data,data.regionlist);                       
        case 'Multi-z'
            [data]=analysis_regions_basic_with_zs(inout,data,data.regionlist);
    end
end


if and(inout.open_several_files_in_directory==0,inout.extract_regions_mean_features==1)
    if inout.number_of_channels_to_compute_intensity==1
        [data]=extract_regions_mean_features(inout,data,data.regionlist);
        plot_regions_mean_features(inout,data,data.regionlist);
    end
end

if inout.export_data==1
    save(fullfile(outdatapath,inout.data_structure_filename),'data');
    if inout.ok_boxes==1
        msgbox('Successfully exported data to the raw images folder.');
    end
end

close all

% To clean non-necessiart bits to save
if isfield(data,'zsum')
    data=rmfield(data,'zsum');
end
if isfield(data,'zmax')
    data=rmfield(data,'zmax');
end

if and(inout.open_several_files_in_directory==1,inout.create_dirdata==1)
    %auxdata=data;
    disp('Creating dirdata');
    index=ifiles-init_file_to_open+1;
    dirdata.dataset{index}=data;
end

% Make sure we start with an empty data structure when doing the directory
% systematic analysis; data structure is deleted from the memory to start building another data
% structure.

if and(inout.open_several_files_in_directory==1,exist('data','var'))
    clear data
end

% End of the directory reading loop
end

% if preferred to do analysis after reading
if and(inout.analysis_regions==1,inout.analysis_after_reading==1)
    for ifiles=init_file_to_open:fin_file_to_open  % Starting the directory loop
        index=ifiles-init_file_to_open+1;
        outdatapath=dirdata.dataset{index}.outdatapath;
        [data]=import_data(outdatapath,inout.data_structure_filename);
        
        switch inout.analysis_type
            case 'Standard'
                [data]=analysis_regions(inout,data,data.regionlist);
            case 'Multi-z'
                [data]=analysis_regions_basic_with_zs(inout,data,data.regionlist);
        end
    
        save(fullfile(outdatapath,inout.data_structure_filename),'data');
        
        if and(inout.open_several_files_in_directory==1,inout.create_dirdata==1)
            %auxdata=data;
            dirdata.dataset{index}=data;
        end
        
        if and(inout.open_several_files_in_directory==1,exist('data','var'))
            clear data
        end         
    end    
end

 if and(inout.open_several_files_in_directory==1,inout.create_dirdata==1)
    if inout.extract_features==1
        disp('Extracting features.');
        inout.fit_fixed_exponent=0;    
        [dirdata]=extract_features(dirdata,inout);
        save(fullfile(datapath, ['dir' inout.data_structure_filename]),'dirdata','-v7.3');
        %save(fullfile(datapath,['dir' inout.data_structure_filename]),'dirdata');
    end
    
% Checking the numbers of determined regions in all data structures

multiple_regions=0;
number_of_regions_list=[];

if inout.select_region==1
    for yy=1:size(dirdata.dataset,2)
        num_reg=size(dirdata.dataset{1}.regionlist,2);
        number_of_regions_list=[number_of_regions_list, num_reg];
        if num_reg>1
            multiple_regions=1;
        end
    end

    % Post analysis implemented for meristem data

    if multiple_regions==0
        inout.fit_fixed_exponent=1;    
        if inout.fit_fixed_exponent==1 && inout.post_analysis==1
            [dirdata]=postanalysis_regions(inout);
        end
    end
end

if isfield(dirdata,'features')
    % Exporting to features to a csv file
    csv_file=fullfile(datapath,['dir' inout.data_structure_filename '_features.csv']);
    struct2csv(dirdata.features,csv_file);
end


end % end of post-analysis loop for extracting data features if opening files in all dir 


end 

function [inout]=quickdefault_inout

[inout.codefolder,inout.datafolder]=check_computer;

inout.numchannels=1;
inout.standard_format_filename=0;
inout.timecourse=0;

inout.export_data=1;
inout.make_movie=0;
inout.select_region=1;
inout.analysis_regions=0;

end
