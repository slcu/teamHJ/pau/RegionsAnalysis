%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Matlab script for producing compiled movies from tiff microscopy files.

% This script is part of the RegionsAnalysis code.

% RegionsAnalysis code mainly have three structure variables:
% inout structure : provides parameters and strings in relation to input
% and output
% data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.
% dirdata structure: structure compiling the different data structures
% within a folder and some analysis of key features of the different data
% sets.
%
% Contact: pau.formosajordan@slcu.cam.ac.uk
%
% See README.md document for further explanations about the code.
%
% Last version: 10/01/2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialize parameters

% To produce movies, make sure there are no thumbs in your directory

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

[inout]=initialize_movie_generator;

%%

% For tile scans:
inout.scope='spiscan';

inout.focus_on_single_z_tiff=1;

inout.open_several_files_in_directory=1;
inout.open_all_positions=1; % For opening all the positions in a directory.
inout.open_all_files_in_directory=0;

inout.select_region=0;              % set to 1 for selecting regions, 0 otherwise.
inout.analysis_regions=0;

inout.select_z_stack=0;
inout.smooth_timecourse=0;
inout.smooth_in_z=0; % Smoothing option when focusing in a particular z, when doing a z-stack.  
inout.defaultnumberregions=5;

inout.max_fluor_to_plot=2500;

inout.defaultregiontype='Circle'
inout.make_basic_radial=0
inout.plot_timecourse_signal=0; % set 1 for signal, 0 for the marker
inout.plot_time_in_frames=0;
inout.hours_per_frame=0.5;
inout.median_filter=0;
inout.tiff_max_int=1;

inout.make_movie=1 % this is the important bit for deciding to make a movie or not
inout.make_movie_combinedchannels=1; 
inout.make_movie_juxtchannels=1;

inout.number_of_channels_to_compute_intensity=1; %put 2 if I want to make it active

%%
% Initialization for standard formats
inout.number_tiff_channels=2;

%inout.tif_string_file_to_select='*s15*w2*t1.tif';

inout.scope='bacillus1'; % Spinning disk settings for clock movie
inout.scope='spiscan';
inout.scope='spo'; % Spinning disk settings for clock movie
inout.scope='spi'; % Spinning disk settings for clock movie, actin, etc
inout.scope='spy'; % spinning disk settings for H2B movie
inout.scope='cy2'; 

inout.scope='readnb'; %to extract the channel from the nd file
inout.ndfilename='Proto_pau.nd'; % in case of reading the nd file
inout.ndfilename='PIN.nd'; % in case of reading the nd file
inout.ndfilename='ProtoplastOne2.nd'; % in case of reading the nd file
inout.ndfilename='Bacillus1.nd'; % in case of reading the nd file
inout.ndfilename='snap1.nd'; % in case of reading the nd file

inout.ndsignal=1; % when reading nd file, it is assumed the first wavenumber is the signal, and the second is the marker

inout.tif_string_file_to_select='snap_*w1*t1.tif';
inout.tif_string_file_to_select=[inout.ndfilename(1:end-3) '*_*w1*t1.tif']; % this is going to work for all nd files

inout.tif_string_file_to_select=[inout.ndfilename(1:end-3) '_*w1*t1.tif']; % this is going to work for all nd files

inout.select_region=0;              % set to 1 for selecting regions, 0 otherwise.
inout.analysis_regions=0;

inout.thresh_fluor_marker=0;  % from snap1 s1, t1.
inout.thresh_fluor_signal=0;


inout.threshold_backgroun_subtraction=1;
inout.load_data_for_background_file=0;
inout.extract_regions_mean_features=0;

% Execute read_rawadata for first reading tiff files, etc, and creating data structure
inout.focus_on_single_z_tiff=0;
inout.select_z_stack=0;
inout.smooth_timecourse=0;
inout.smooth_in_z=0; % Smoothing option when focusing in a particular z, when doing a z-stack.  
inout.defaultnumberregions=5;

inout.max_fluor_to_plot=2500;

inout.open_several_files_in_directory=1;
inout.open_all_positions=1; % For opening all the positions in a directory.
inout.open_all_files_in_directory=1;

inout.defaultregiontype='Circle'
inout.make_basic_radial=0;
inout.plot_timecourse_signal=0; % set 1 for signal, 0 for the marker
inout.plot_time_in_frames=0;
inout.hours_per_frame=0.5;
inout.median_filter=0;
inout.tiff_max_int=1;

inout.make_movie=1; % this is the important bit for deciding to make a movie or not
inout.make_movie_combinedchannels=1; 
inout.make_movie_juxtchannels=0;

inout.number_of_channels_to_compute_intensity=1; %put 2 if I want to make it active
inout.find_background_through_histogram_peak=1;

inout.make_signal_movie=1;
inout.make_marker_movie=1;
inout.make_2channels_movies=1;
inout.make_single_channel_movies=1;
%%
%inout.tif_string_file_to_select='*H4*w2*t1.tif';
inout.brightfield_in_movie=1; % set to 1 when willing to see brightfield in movie generation, otherwise set to 0.

inout.make_signal_movie=1;
inout.make_marker_movie=0;
inout.make_2channels_movies=0;
inout.make_single_channel_movies=1;
%inout=rmfield(inout,'marker_channel_index')
%inout.number_tiff_channels=1
%%
% To focus on one position
inout.open_all_positions=1; % For opening all the positions in a directory.
inout.open_several_files_in_directory=0;
%inout.open_all_files_in_directory=0;

inout.make_signal_movie=1;
inout.make_marker_movie=1;

inout.make_single_channel_movies=1;

inout.make_2channels_movies=1;
inout.make_movie_combinedchannels=1; 
inout.make_movie_juxtchannels=0;

inout.tif_string_file_to_select=[inout.ndfilename(1:end-3) '_*w1*s12_*t1.tif'];

%inout.do_files_preselection
%%
inout.thresh_fluor_marker=200;  % from snap1 s1, t1.
inout.thresh_fluor_signal=250;
inout.make_movie_juxtchannels=0;
%%
% best option for H4 and CycB movies
inout.make_signal_movie=1;
inout.make_marker_movie=1;
inout.make_2channels_movies=0;
inout.make_single_channel_movies=1;
%%
inout.overwriting_mode=1 % set it to 1 by default; set it to zero for just doing movies for those cases in which avi movies have not been generated 
inout.find_background_through_histogram_peak=1;
clear data
if inout.open_several_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end
%%
% not fully working, revise
run_RegionsAnalysis_in_subdirectories(inout)

%%
% Execute create_dirdata for creating and loading a particular dirdata structure and playing with it
% To extract dirdata set, write 
% e.g. dirdata.dataset{number}

[dirdata,datapath]=create_dirdata;

%%
% Execute import_data loading a particular data structure and playing with it
% To extract data set, write data.featureofinterest
% e.g. data.region{number}
% e.g. data.region{1}.regionmask shows the mask of the selected region
[data]=import_data;

%%
% Given a list of regions to analize in a data structure, analysis_regions
% provide differerent tools to analyze the data.
% 3 main options for passing a lis:
% 1.- Using the list in data.regionlist, what contains all regions analysed
% for a data set.
% 2.- Using the temporary list data.regionlisttemp (updated every time one
% adds new regions)
% 3.- Using a particular list set by you, e.g. list_of_regions=[2 1]

list_of_regions=data.regionlist;
%list_of_regions=data.regionlisttemp;  % Note data.regionlisttemp 
%list_of_regions=[2,1];
[data]=analysis_regions(inout,data,list_of_regions);
%%
plot_regions_mean_features(inout,data,data.regionlist);
%%

% Use add_regions in case you have already a data structure and you want to keep it but add new regions.
% Note that add_regions function calls import_data function

[data]=add_regions_to_study(data);


%%

make_snap_all_regions(inout,data)

