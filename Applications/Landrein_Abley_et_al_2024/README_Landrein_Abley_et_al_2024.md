Landrein_Abley_et_al_2024 code: Analysis of fluorescence expression domains in the Arabidopsis shoot inflorescence and floral meristems.
-----------------------------------------------------------------------------------------------------------------------------------------

This Code has been used for a manuscript:

Landrein, B., Abley, K., Formosa-Jordan, P., Meyerowitz, E.M., Jönsson, H. and Locke, J.C.W. (2024)
Robustness and plasticity of Arabidopsis inflorescence and floral shoot apical meristems in response to mineral nutrients

Contact regarding the scripts within this repository:
Pau Formosa-Jordan - pformosa@mpipz.mpg.de

Further details about the used pipelines in these scripts can be found in this book chapter:
Formosa-Jordan and Landrein (2023) Quantifying gene expression domains in plant shoot apical meristems. In: Riechmann JL, Ferrandiz C (eds) Flower Development: Methods and Protocols. Methods in Molecular Biology, Second Edi. Springer.

INSTALLATION NOTES
---------------------------

This code has been run using MATLAB 2020b in Mac (OS Catalina). To use the code, just open and execute any of the main_scripts in the present subfolder.  

For further details about the installation, see the main Readme file of the RegionsAnalysis repository.   

SUMMARY
------------

These scripts are supporting the pipelines explained in the above mentioned manuscript to characterize the fluorescence domains of inflorescence and floral meristems.

Main scripts:

main_script_inflorescense_meristem_Landrein_Abley_et_al_2024.m  : this script can automatically find the central fluorescent domain of the shoot apical meristem and perform its analysis. This script is very similar to the RegionsAnalysis script main_script_meristem_Landrein_etal_2018.  
main_script_floral_meristems_Landrein_Abley_et_al_2024.m  : script for computing the basic features of the fluorescence domains in both the inflorescence and floral meristems.

These scripts are part of the RegionsAnalysis package.

EXPLANATION OF THE CODE AT A GLANCE
------------------------------------

(Need to be updated, in process)  

INPUT:

Folder containing just the LSM Images to analyse, see the provided Landrein_et_al dataset. To use this dataset, you will first need to unzip it. When running the different scripts, you will be asked to select the folder containing the images.

Note that some parameters might need to be set/modified to do your analysis (mostly stored in the inout structure).

OUTPUT:

The outcome will be dependent on the initialization parameters.

These are some of the expected outputs you should get given the default parameters.

dirdata_features.csv : CSV file where the final features are stored.

subfolders named as each input image containing
- data.mat structure, compiling different information about the image, including its analysis.
- subfolders containing the performed analysis (e.g. intensity profile fit)
- matlab figure showing the selected ROIs, or a label in the centroid in each the ROI

dirdata.mat: matlab structure compiling different information (eg all the data.mat structures from all images)

selected_regions: folder showing the processed images with the different selected regions (i.e. the central fluorescent domain)
tiffs_orthos_max: maximal intensity orthogonal projections of the images
tiffs_with_numbered_regions

Note: this current version has improved the pipeline to find the central expression domain. Specifically, the algorithm now uses the domain that is closest to the center of mass of all the expression domains found in the image of interest (see find_central_domain function). Still, it is recommended to visually assess the outcome of the central domain.
