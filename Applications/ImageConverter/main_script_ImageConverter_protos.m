%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Matlab script for preprocessing and 'converting' tiff images. 
% Note its current main function is to change the names so that they can be
% segmented by Shcnitzcells software.

% This script is part of the RegionsAnalysis code.

% RegionsAnalysis code mainly have three structure variables:
% inout structure : provides parameters and strings in relation to input
% and output
% data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.
% dirdata structure: structure compiling the different data structures
% within a folder and some analysis of key features of the different data
% sets.
%
% Contact: pau.formosajordan@slcu.cam.ac.uk
%
% See README.md document for further explanations about the code.
%
% Last version: 07/06/2020
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialize parameters

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

% Initialization for ImageConverter

[inout]=initialize_for_ImageConverter;
inout.timecourse=auxinout.timecourse;
inout.number_tiff_channels=auxinout.number_tiff_channels;
inout.crop_images=auxinout.crop_images;
inout.ndfilename=auxinout.ndfilename; % in case of reading the nd file
inout.tif_string_file_to_select=auxinout.tif_string_file_to_select;
inout.ini=auxinout.ini;
inout.fin=auxinout.fin;

%inout.overrideformat=1
%inout.format='tiffrgb';
%%
inout.select_time_window=1
inout.ndsignal=1;
inout.export_renamed_for_schnitzcells=1;
%inout.export_working_image_to_tiff_for_schnitzcells=1;
inout.standard_format_filename=1;
inout.scope='readnb'; %to extract the channel from the nd file
inout.open_all_positions=1;
inout.ballsize=20;

% Execute read_rawadata for first reading tiff files, etc, and creating data structure

inout.open_several_files_in_directory=1;
inout.open_all_positions=1; % set it to 1 to make the full directory, and to 0 to restrict yourself to a position
inout.make_basic_radial=0;

if isfield(auxinout,'signal_channel_index')
    inout.signal_channel_index=auxinout.signal_channel_index;
    inout.marker_channel_index=auxinout.marker_channel_index;
    inout.zsum=auxinout.zsum;
    inout.zmax=auxinout.zmax;
    inout.standard_format_filename=0;
    inout.rolling_ball_background_filter=0;
end

%%
if inout.timecourse==1
% for timecourse
    inout.timecourse=1;
    inout.standard_format_filename=1;
    inout.export_renamed_for_schnitzcells=1;
    inout.export_working_image_to_tiff_for_schnitzcells=0;
end

inout.focus_on_single_z_tiff=0;
inout.tiff_max_int=1;

inout.rolling_ball_background_filter=1; % set 1 to perform the rolling ball background filtering in the first_image (just one channel)
inout.ballsize=20;
inout.rolling_ball_background_filter_in_signal=1; % set 1 to perform the rolling ball background filtering in the signal as well
inout.combine_signal_and_marker_for_schnitzcells=0; % if 1, it combines membrane and signal images to have a more optimised marker for segmentation
inout.tiff_make_zmean_for_marker=0; % set to 1 to make z-mean in the marker (while z-max in the signal given that needed for this to work inout.tiff_max_int=1 )

%%
tic
clear data
if inout.open_several_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end
toc

