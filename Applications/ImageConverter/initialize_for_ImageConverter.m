function [inout]=initialize_for_ImageConverter

% This initialization enables to select a z-stack, and rename it for
% schnitzcells

[inout]=initialize_default_parameters;

inout.signal_schnitzcells='-g-';
inout.marker_schnitzcells='-r-';
    
inout.show_im_figs=0;
inout.reading_waitbar=0;

inout.export_renamed_for_schnitzcells=1;
inout.standard_format_filename=1;   % set to 0 to load files with arbitrary names
% inout.scope='cyano1';
inout.scope='spin';
inout.open_all_positions=1;
inout.open_several_files_in_directory=1;

% End of the parameters definition

currentdir=pwd; 
str = which(check_computer);
[inout.codefolder,inout.datafolder]=check_computer;

datafolder=inout.datafolder;
datapath = uigetdir(datafolder,'Select the data set location');

if ~datapath
return;
end

disp('chosen directory is');
disp(datapath)

inout.datapath=datapath;

% useful stuff

inout.ok_boxes=0;            % set this parameter to 1 if you want matlab poping-up 'oks' during the RegionAnalysis execution

% Schnitzcells specific inout. 
inout.select_z_stack=0;      % set to 1 for selecting the z-stack, otherwise set to 0.
inout.select_time_window=0 ; % set to 1 for selecting the time window, otherwise set to 0.


inout.num_anglebins=32;
inout.xy_pixel_length_in_um=0.8;
inout.xy_pixel_area_in_um2=(inout.xy_pixel_length_in_um)^2;

% Plot related
control_string='TdTomato';
control_string='TdTomato';

signal_string='YFP';
signal_string='GFP';

control_string='AutoFluorescence';

inout.fontsize=18;

inout.cells_with_labels=1;

inout.eccentricity.axislabel={'Eccentricity'};

inout.avs_g.axislabel=['Average' signal_string ' Fluorescence']
inout.avs_r.axislabel={'Average TdTomato'; 'Fluorescence'};
inout.avs_r.max_plot=15000;
inout.avs_g.max_plot=20000; % GFP
%inout.avs_g.max_plot=1000; % GFP

inout.avs_g.max_plot=5*10^4; % citrine

inout.avs_c_g.axislabel={'Average Membrane' ; [ signal_string ' Fluorescence']};
inout.avs_c_r.axislabel={'Average Membrane' ; ' TdTomato Fluorescence'};

inout.avs_c_g.dirout='avs_contour_g';
inout.avs_c_r.dirout='avs_contour_r';

inout.tot_c_g.axislabel={'Total Membrane' ; [ signal_string ' Fluorescence']};
inout.tot_c_r.axislabel={'Total Membrane' ; ' TdTomato Fluorescence'};

inout.tot_g.axislabel=['Total ' signal_string ' Fluorescence'];
inout.tot_r.axislabel={'Total TdTomato'; 'Fluorescence'};

inout.avs_c_r.max_plot=20000;
inout.avs_c_g.max_plot=20000;
inout.avs_c_g.max_plot=200;

inout.avs_i_g.axislabel={'Average Cytosolic' ; [ signal_string ' Fluorescence']};
inout.avs_i_r.axislabel={'Average Cytosolic' ; ' TdTomato Fluorescence'};

inout.tot_i_g.axislabel={'Total Cytosolic' ; [ signal_string ' Fluorescence']};
inout.tot_i_r.axislabel={'Total Cytosolic' ; ' TdTomato Fluorescence'};

inout.frac_ic_g.axislabel={' Cytosolic to Membrane Fraction of' ; ['Average ' signal_string ' Fluorescence']};
inout.frac_ic_r.axislabel={' Cytosolic to Membrane Fraction of' ; 'Average TdTomato Fluorescence'};

inout.frac_ic_r.max_plot=1.7;
inout.frac_ic_g.max_plot=3;

inout.frac_tot_ic_g.axislabel={' Cytosolic to Membrane Fraction of' ; ['Total ' signal_string ' Fluorescence']};
inout.frac_tot_ic_r.axislabel={' Cytosolic to Membrane Fraction of' ; 'Total TdTomato Fluorescence'};

inout.frac_tot_ic_r.max_plot=10;
inout.frac_tot_ic_g.max_plot=15;

inout.cl_c_g.axislabel=['Membrane ' signal_string ' CV'];
inout.cl_c_g.dirout='CV_contour_g';
inout.cl_c_r.axislabel='Membrane TdTomato CV';
inout.cl_c_r.dirout='CV_countour_r';

inout.cl_c_g.max_plot=1.5;
inout.cl_c_r.max_plot=1.5;

inout.areas.axislabel='Cell area [A.U.]';
inout.areas.dirout='areas';
inout.areas.max_plot=8000;

inout.aspect_ratio.axislabel='Aspect ratio';
inout.aspect_ratio.dirout='aspect_ratio';
inout.aspect_ratio.max_plot=5;

inout.areas_in_um2.axislabel='Cell area [um^2]';
inout.radii_in_um.axislabel='Cell radius [um]';

inout.perimeters.axislabel='Perimeter per cell [A.U.]';

inout.centroid_x.axislabel='x coordinate [A.U.]';

inout.polx_c_r.axislabel={'Membrane x-Polarity' ;' TdTomato [A.U.]'};
inout.poly_c_r.axislabel={'Membrane y-Polarity' ;' TdTomato [A.U.]'};
inout.pol_c_r.axislabel={'Membrane Polarity' ;' TdTomato [A.U.]'};
inout.pol_c_r.max_plot=5*10^7;
inout.pol_c_r_means.max_plot=25;

inout.pol_c_r.dirout='polarity_r';

inout.pol_c_r_means.axislabel={'Membrane Polarity' ;' TdTomato [A.U.]'};
inout.pol_c_r_means.dirout='polarity_r_means';

inout.polx_c_g.axislabel={'Membrane x-Polarity' ;[ signal_string ' [A.U.]']};
inout.poly_c_g.axislabel={'Membrane y-Polarity' ;[ signal_string '[A.U.]']};
inout.pol_c_g.axislabel={'Membrane Polarity' ;[ signal_string ' [A.U.]']};


inout.pol_c_g.max_plot=5*10^7;
inout.pol_c_g.dirout='polarity_g';
inout.pol_c_g_means.max_plot=25;

inout.pol_c_g_means.axislabel={'Membrane Polarity' ;[ signal_string ' [A.U.]']};
inout.pol_c_g_means.dirout='polarity_g_means';

inout.pol_c_shape.axislabel={'Membrane Polarity' ;' Shape [A.U.]'};

inout.angle_c_g.axislabel={[ signal_string ' Polarity Angle (rad)']};
inout.angle_c_g_means.axislabel={[ signal_string ' Polarity Angle (rad)']};

inout.angle_c_r.axislabel={'TdTomato Polarity Angle (rad)'};
inout.angle_c_shape.axislabel={'Shape Polarity Angle (rad)'};
inout.angle_c_r_means.axislabel={'TdTomato Polarity Angle (rad)'};

inout.mean_shape_sig.axislabel={'Mean number of pixels per angular bin'};


inout.g_pol_score_chau.axislabel={[signal_string ' Polarity Score']};
inout.r_pol_score_chau.axislabel={'TdTomato Polarity Score'};

inout.g_powmax.axislabel={[signal_string ' Power Spectra Maxima']};
inout.r_powmax.axislabel={'TdTomato Power Spectra Maxima'};
inout.g_powmax.max_plot=25;
inout.r_powmax.max_plot=25;

inout.angle_c_r_means.max_plot=2*pi;
inout.angle_c_g_means.max_plot=2*pi;

inout.g_power_cv.axislabel={['CV of ' signal_string ' Power Spectra']};
inout.r_power_cv.axislabel={'CV of TdTomato Power Spectra'};

inout.g_means_power_cv.axislabel={['CV of Average ' signal_string ' Power Spectra']};
inout.r_means_power_cv.axislabel={'CV of Average TdTomato Power Spectra'};

inout.g_power_cv.max_plot=5;
inout.r_power_cv.max_plot=5;

inout.g_means_power_mean.axislabel={['Mean of Average ' signal_string ' Power Spectra']};
inout.r_means_power_mean.axislabel={'Mean of Average TdTomato Power Spectra'};

inout.g_powermax_reldev.axislabel={'Relative deviation of';[  signal_string ' Power Spectra maxima']};
inout.r_powermax_reldev.axislabel={'Relative deviation of';' TdTomato Power Spectra maxima'};

inout.g_powermax_reldev.axislabel={'Relative deviation of';[  signal_string ' Power Spectra maxima']};
inout.r_powermax_reldev.axislabel={'Relative deviation of';' TdTomato Power Spectra maxima'};

inout.g_powf_amp.axislabel={[signal_string ' Power Spectra'];'first mode amplitude'};
inout.r_powf_amp.axislabel={' TdTomato Power Spectra';'first mode amplitude'};
inout.g_means_powf_amp.axislabel={[' Average '  signal_string ' Power Spectra'];'first mode amplitude'};
inout.r_means_powf_amp.axislabel={' Average TdTomato Power Spectra';'first mode amplitude'};



inout.g_means_powf_amp.max_plot=22;
inout.r_means_powf_amp.max_plot=22;  % if std normaliz
inout.g_means_powf_amp.max_plot=10; 
inout.r_means_powf_amp.max_plot=10; % if means normaliz
inout.g_powf_amp.max_plot=22;
inout.r_powf_amp.max_plot=22;

inout.perimeters.max_plot=450;
inout.radii_in_um.max_plot=0.8*450/(2*pi);
inout.areas_in_um2.max_plot=6000;

inout.tot_r.max_plot=7*10^7;
inout.tot_g.max_plot=7*10^7;
inout.tot_c_r.max_plot=8*10^6;
inout.tot_c_g.max_plot=7*10^6;
inout.tot_i_r.max_plot=7*10^7;
inout.tot_i_g.max_plot=4*10^7;

inout.avs_i_r.max_plot=3*10^4;
inout.avs_i_g.max_plot=2*10^4;



inout.observables{1}={'frac_ic_r','frac_ic_g','radii_in_um','areas_in_um2'}

inout.observables{1}={'frac_tot_ic_r','frac_tot_ic_g','areas_in_um2','radii_in_um'}


inout.observables_fits{1}={'tot_c_g','radii_in_um'}
inout.observables_fits{2}={'tot_c_r','radii_in_um'}
inout.observables_fits{3}={'tot_i_g','areas_in_um2'}
inout.observables_fits{4}={'tot_i_r','areas_in_um2'}

inout.observables_fits_fracs{1}={'frac_tot_ic_g','areas_in_um2'}
inout.observables_fits_fracs{2}={'frac_tot_ic_r','areas_in_um2'}


inout.observables{11}={'avs_c_r','avs_i_g','areas_in_um2'};

inout=rmfield(inout,'observables');

inout.observables{1}={'tot_r','tot_g','radii_in_um'}
inout.observables{1}={'r_means_powf_amp','g_means_powf_amp','areas_in_um2'};
inout.observables{1}={'r_powf_amp','g_powf_amp','areas_in_um2'};
inout.observables{1}={'r_means_power_cv','r_means_powf_amp','areas_in_um2'};
inout.observables{2}={'g_means_power_cv','g_means_powf_amp','areas_in_um2'};

inout=rmfield(inout,'observables');
inout.observables{1}={'avs_i_r','avs_i_g','areas_in_um2'};
% radii_in_um is perimeters divided by 2 pi * xyresolution
% Scatter plots I would like to plot
inout.observables{1}={'avs_c_r','avs_c_g','areas_in_um2'};
inout.observables{2}={'avs_r','avs_g','areas_in_um2'};
inout.observables{3}={'pol_c_g','pol_c_r','areas_in_um2'};
inout.observables{4}={'cl_c_r','cl_c_g','areas_in_um2'};
inout.observables{5}={'g_power_cv','r_power_cv'};
inout.observables{6}={'g_powmax','r_powmax','areas_in_um2'};
inout.observables{7}={'g_power_cv','g_powmax'};
inout.observables{8}={'r_power_cv','r_powmax'};
inout.observables{9}={'g_powermax_reldev','g_powmax'};
inout.observables{10}={'r_powermax_reldev','r_powmax'};
inout.observables{11}={'avs_i_r','avs_i_g','areas_in_um2'};
inout.observables{12}={'r_powf_amp','g_powf_amp'};
inout.observables{13}={'r_means_powf_amp','g_means_powf_amp'};

inout=rmfield(inout,'observables');
inout.observables{1}={'g_means_power_mean','g_means_power_mean','areas_in_um2'}

inout=rmfield(inout,'observables');
inout.observables{1}={'r_means_powf_amp','g_means_powf_amp','areas_in_um2'};
inout.observables{2}={'r_means_power_cv','g_means_power_cv','areas_in_um2'}


inout.observables{1}={'avs_c_r','avs_c_g','radii_in_um'};

inout=rmfield(inout,'observables')
inout.observables{1}={'avs_r','avs_g','areas_in_um2'};
inout.observables{1}={'avs_c_r','avs_c_g','radii_in_um'};

inout.observables{1}={'avs_c_r','areas_in_um2'};
inout.observables{2}={'avs_c_g','areas_in_um2'};
inout.observables{1}={'avs_c_r','radii_in_um2'};
inout.observables{2}={'avs_c_g','radii_in_um2'};
inout=rmfield(inout,'observables');

%inout.observables{1}={'tot_r','tot_g','radii_in_um'}
inout.observables{1}={'tot_c_r','tot_c_g','radii_in_um'}
inout.observables{2}={'tot_i_r','tot_i_g','areas_in_um2'}


inout=rmfield(inout,'observables');
inout.observables{1}={'tot_c_g','tot_i_g'}
inout.observables{1}={'avs_c_g','avs_i_g'}

inout=rmfield(inout,'observables');
inout.observables{1}={'frac_tot_ic_g','frac_tot_ic_r','radii_in_um'}
inout.observables{2}={'frac_tot_ic_g','frac_tot_ic_r','areas_in_um2'}
inout.observables{3}={'tot_r','tot_g','radii_in_um'}
inout.observables{4}={'tot_c_r','tot_c_g','radii_in_um'}
inout.observables{5}={'tot_i_r','tot_i_g','areas_in_um2'}
inout.observables{6}={'cl_c_r','cl_c_g','areas_in_um2'};
inout.observables{7}={'pol_c_g','pol_c_r','areas_in_um2'};
inout.observables{8}={'avs_c_g','avs_i_g'}

inout=rmfield(inout,'observables');
inout.observables{1}={'cl_c_g','avs_c_g'};
inout.observables{1}={'cl_c_g','tot_c_g'};
inout.observables{2}={'cl_c_g','avs_g'};
inout.observables{1}={'cl_c_r','cl_c_g'};

inout=rmfield(inout,'observables');
inout.observables{1}={'pol_c_r_means','pol_c_g_means','areas_in_um2'};
inout.observables{1}={'pol_c_r','pol_c_g','areas_in_um2'};
inout.observables{1}={'pol_c_r','pol_c_g'};
inout.observables{1}={'pol_c_r_means','pol_c_g_means'};

inout=rmfield(inout,'observables');
inout.observables{1}={'avs_c_r','avs_c_g','radii_in_um'};

inout.observables{1}={'pol_c_r_means','pol_c_g_means'};
inout.observables{1}={'angle_c_g_means','pol_c_g_means'};

inout.observables{1}={'pol_c_r_means','areas_in_um2'};
inout.observables{2}={'pol_c_g_means','areas_in_um2'};
inout.observables{3}={'angle_c_r_means','angle_c_g_means'};
inout.observables{4}={'angle_c_r_means','pol_c_r_means'};
inout.observables{5}={'angle_c_g_means','pol_c_g_means'};
inout.observables{6}={'cl_c_g','avs_g'};
inout.observables{7}={'cl_c_r','avs_r'};
inout.observables{8}={'cl_c_g','areas_in_um2'};
inout.observables{9}={'cl_c_r','areas_in_um2'};
inout.observables{10}={'pol_c_r','areas_in_um2'};
inout.observables{11}={'pol_c_g','areas_in_um2'};
inout.observables{12}={'pol_c_r_means','pol_c_g_means'};
inout.observables{13}={'avs_c_r','avs_c_g','radii_in_um'};
inout.observables{14}={'avs_c_r','avs_c_g','areas_in_um2'};
inout.observables{15}={'cl_c_r','cl_c_g'};



% path-related-things
%inout=rmfield(inout,'observables');
%inout.observables{1}={'cl_c_g','areas_in_um2'};
%inout.observables{2}={'cl_c_r','areas_in_um2'};
%inout.observables{1}={'pol_c_r_means','pol_c_g_means'};

inout.schnitzcells.out='0000-00-00';

tmp = matlab.desktop.editor.getActive;
formerdir=fileparts(tmp.Filename)% this will provide the path of the sript calling initialize_for_schnitzcells
posMATLABchar=regexp(formerdir,'MATLAB')
MATLABcodepath=formerdir(1:posMATLABchar+5);

posProtochar=regexp(formerdir,'Protoplast')
codepath=formerdir(1:posProtochar-1);

if isempty(MATLABcodepath)
    codepath=codepath;
    inout.ProtoplastAnalysis=formerdir(1:posProtochar+19);
    addpath(genpath(inout.ProtoplastAnalysis));
else
    codepath=MATLABcodepath;
end


inout.RegionsAnalysispath=fullfile(codepath,'RegionsAnalysis')
addpath(genpath(inout.RegionsAnalysispath)) % add path where there is RegionAnalysis Code

%inout.Other_functions=fullfile(codepath,'Other_functions');
%addpath(genpath(inout.Other_functions)) % add path where there is Other_functions Code

inout.schnitzpath=fullfile(codepath,'Schnitzcells');
addpath(genpath(inout.schnitzpath)) % add path where there is Other_functions Code

inout.schnitzcells.out='0000-00-00';% it should be a date, but for standarizing it , I keep it like that.






















