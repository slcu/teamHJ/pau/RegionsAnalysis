%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Matlab script for preprocessing and 'converting' tiff images. 
% Note its current main function is to change the names so that they can be
% segmented by Shcnitzcells software.

% This script is part of the RegionsAnalysis code.

% RegionsAnalysis code mainly have three structure variables:
% inout structure : provides parameters and strings in relation to input
% and output
% data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.
% dirdata structure: structure compiling the different data structures
% within a folder and some analysis of key features of the different data
% sets.cd 
%
% Contact: pau.formosajordan@slcu.cam.ac.uk
%
% See README.md document for further explanations about the code.
%
% Last version: 10/01/2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialize parameters

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));


%%
% Pau initialization  for regions selection

%[inout]=initialize_pinsigcell;
%inout.scope='spi'; % Spinning disk but without Dual filters is 'spu'. Otherwise say 'spi'
%inout.scope='spu'; % Spinning disk but without Dual filters is 'spu'. Otherwise say 'spi'
%

% Initialization for ImageConverter

[inout]=initialize_for_ImageConverter;

inout.scope='spo'     % probably for mark notation 
inout.scope='spiscancit'       % if the marker is in channel 2 and signal in channel 1
inout.scope='spy' % 'spiscan' when the membrane marker is in w1 and the signal is in w2 
inout.scope='spi'; % Spinning disk but without Dual filters is 'spu'. Otherwise say 'spi'
inout.scope='spiscan' % 'spiscan' when the membrane marker is in w1 and the signal is in w2 

inout.scope='readnb'; %to extract the channel from the nd file
inout.ndfilename='snap1.nd'; % in case of reading the nd file
inout.tif_string_file_to_select=[inout.ndfilename(1:end-3) '_*w1*t1.tif'];
inout.tif_string_file_to_select=[inout.ndfilename(1:end-3) '_*w1*s19*.tif'];
inout.tif_string_file_to_select=[inout.ndfilename(1:end-3) '_*w1*s49_t1.tif'];
inout.tif_string_file_to_select=[inout.ndfilename(1:end-3) '_*w1*s2_t1.tif'];

inout.number_tiff_channels=2;
inout.ndsignal=1;

%Default values are inout.signal_schnitzcells='-g-' and
%inout.marker_schnitzcells='-r-', which works for PIN data and so on.
%%
% for actin and nuclear data:
inout.signal_schnitzcells='-r-';
inout.marker_schnitzcells='-g-';

%%

% if inout.threshold_backgroun_subtraction==1, inout.thresh_fluor_marker and inout.thresh_fluor_signal will be removed from the corresponding signals, and the exported schnitz format files will not have such a background. 
inout.thresh_fluor_marker=0;  % from snap1 s1, t1.
inout.thresh_fluor_signal=0;  % 

inout.threshold_backgroun_subtraction=0;
inout.load_data_for_background_file=0;
inout.extract_regions_mean_features=0;
inout.gradient_marker_max=1000;

%%
% Key parameters to decide what to study
% Selection parameters for polarity
inout.focus_on_single_z_tiff=1;
inout.tiff_max_int=0;
inout.smooth_in_z=1;
inout.num_zs_for_smoothing=5;

%%
% Selection parameters for size analysis
inout.focus_on_single_z_tiff=0;
inout.tiff_max_int=1;

%%
% Matt pipeline
inout.export_renamed_for_schnitzcells=0;
inout.export_working_image_to_tiff_for_schnitzcells=1;
inout.standard_format_filename=3;
inout.scope='spiscan' % 'spiscan' when the membrane marker is in w1 and the signal is in w2 
inout.timecourse=0;
inout.number_tiff_channels=2;
inout.tif_string_file_to_select='*w1*tif';
inout.open_all_positions=1;
inout.rolling_ball_background_filter=1; % set 1 to perform the rolling ball background filtering in the first_image (just one channel)
inout.ballsize=20;
inout.crop_images=1;

%%
% For the annotations pipeline, it helps...
% Note this rolling ball background removal is just applied to the marker
% by default, but here also we can put the signal

inout.rolling_ball_background_filter=1; % set 1 to perform the rolling ball background filtering in the first_image (just one channel)
inout.ballsize=20;
inout.rolling_ball_background_filter_in_signal=1; % set 1 to perform the rolling ball background filtering in the signal as well
inout.combine_signal_and_marker_for_schnitzcells=0; % if 1, it combines membrane and signal images to have a more optimised marker for segmentation
inout.tiff_make_zmean_for_marker=0; % set to 1 to make z-mean in the marker (while z-max in the signal given that needed for this to work inout.tiff_max_int=1 )

%%
% note that schnitzcells will already have a background stuff
% inout.find_background_through_histogram_peak=1;
%%
% Execute read_rawadata for first reading tiff files, etc, and creating data structure
inout.max_fluor_to_plot=2500;

inout.open_several_files_in_directory=1;
inout.open_all_positions=1; % set it to 1 to make the full directory, and to 0 to restrict yourself to a position
inout.make_basic_radial=0;

clear data
if inout.open_several_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end


