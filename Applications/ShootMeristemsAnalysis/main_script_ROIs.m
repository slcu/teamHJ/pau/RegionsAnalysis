%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mais_script_ROIs. The intensity of such fluorescent domains is
% computed as well. 

% The code allows you to draw a region of interest and compute the area and fluorescence intensity features about it. See 
% Formosa-Jordan and Landrein 2022 for further details. 

% The fluorescence intensity is computed through the function 'intensity_in_region', and not
% through a radial profile fitting approach.

% Also, see README.md document for further explanations about this script and its outcome.
 
% This script is part of the RegionsAnalysis code.

% RegionsAnalysis package mainly has three structure variables:
% inout structure : provides parameters and strings in relation to input
% and output
% data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.
% dirdata structure: structure compiling the different data structures
% within a folder and some analysis of key features of the different data
% sets.

% See README.md document in the RegionsAnalysis folder for further 
% explanations about the package.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

%  Execute initialize_meristemcell function to load the default parameters for the analysis. 
[inout]=initialize_FormosaJordan_Landrein_2022_pipelines;

% MOST RELEVANT PARAMETERS (note these parameters might override parameters set by the previous initialize function)
inout.defaultregiontype='Circle';   % type of region that is going to be analysed (current defaultregiontype options: 'Circle','Rectangle','Polygon'). Comment this line if different regions are needed to be explored.;
inout.draw_freehand_polygon=0; % enables drawing of the polyigon in a freehand style. Otherwise set it to 0, and multiple clicks will be needed to plot the polygon.

% If polygon
inout.click_selecting_regions=1;    % set to 1 for selecting regions by clicking, set to 0 to load it from previous data or it will find the origin of the central domain. 
inout.get_center_and_radius_from_previous_data=0; % for loading from previous data. It just works for circular regions (it will load origin and radius used).
inout.find_central_domain=0; % set to 1 to find the central domain in the xy plane. For lsm files. Otherwise, set to 0.
inout.get_center_from_cental_domain=0; % set to 1 to find the origin from the central domain. Note that the radius of the selected region will still be set by the inout.radius_region_in_microns parameter. 

inout.make_orthos=0;                   % set to 1 if making ortogonal slices, otherwise set it to 0. 

inout.radius_region_in_microns=40;     % if this field is removed/commented with '%', then the program asks for a second point to mark the region to study.
inout.ortho_thickness_in_microns=10;   % 

inout.signal_channel_index=1;         %   channel to look at if lsm files
inout.zsum=1;                         %   if we want to do a zsum 
inout.zmax=0;                         %   if we want to do a zmax 
% inout.gauss_sigma_microns=5;              %   sigma in microns units
inout.radiusstep_in_microns=1;            %   parameter just working for lsm, it overrides the inout.radiusstep parameter, which is in pixel units. 

% Less important parameters
inout.defaultnumberregions=1;             %   default number of regions to analyse in each file
inout.defaultnumberbackgroundregions=1;   %   default number of background regions to analyse in each file
inout.post_analysis=0;                    %   set this parameter to 1 to do the fit to averaged exponent found after fittings, otherwise set to 0. Note this is possible when there is just one selected region per image.
inout.background_subtraction=0;           %   set this parameter to 1 to do the background subtraction, and to 0 otherwise.         
inout.make_fittings=0;                    % set to 1 to fit the intensity vs r curves to generalised exponentials and hill functions. Otherwise, set to 0.
inout.ask_numberregions=0;                %   set this parameter to 1 if you want matlab ask you how many regions to analyze per file, otherwise set it to 0. 
inout.select_background_regions=0;        %   set to 1 for selecting background regions, 0 otherwise. Note this will be subtracted from the image to analyse if inout.background_subtraction=1
inout.fit_hole=0;                         %   for fitting TCS signal (circular gradient-like shape with a dip in the center), set it to 1, otherwise, to 0
inout.islsm=1;
inout.make_basic_radial=0; 

inout.make_snap_all_regions=0;
inout.plot_background_regions_stuff=0;

switch inout.defaultregiontype 
    case 'Circle'
        inout.make_basic_radial=inout.make_basic_radial;
    otherwise
        inout.make_basic_radial=0;
end

%%
% Execute RegionsAnalysis for importing the images files, doing the
% pertinent analysis and creating the data structure.

clear data
if inout.open_all_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END of the basic pipeline


