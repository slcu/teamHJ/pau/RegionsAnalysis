ShootMeristemsAnalysis code: Analysis of the fluorescence expression domains in the Arabidopsis shoot meristems
-----------------------------------------------------------------------------------------------------------------------------------------

This Code has been used for a the following book chapter (2023):

Title: Quantifying gene expression domains in plant shoot apical meristems.

Authors: Pau Formosa-Jordan (1,2), Benoit Landrein (1,2).

1. Sainsbury Laboratory, University of Cambridge, Bateman Street, Cambridge CB2 1LR, UK.
2. Max Planck Institute for Plant Breeding Research, Cologne, Germany.
3. Laboratoire Reproduction et Développement des Plantes, Univ Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRA, Lyon, France.


Contact: Pau Formosa-Jordan - pformosa@mpipz.mpg.de

See book chapter in this [link](https://link.springer.com/protocol/10.1007/978-1-0716-3299-4_25).
An open source equivalent manuscript can be found in this [link](https://hal.science/hal-03909435/document).

INSTALLATION NOTES
---------------------------

This code has been run using MATLAB 20018b with Windows 7 Professional and in Mac (OS X Mojave). To use the code, just open and execute any of the main_scripts in the present subfolder.  

For further details about the installation, see the main Readme file of the RegionsAnalysis repository.   

SUMMARY
------------

These scripts are supporting the pipelines explained in the above mentioned manuscript to characterize the fluorescence domains of inflorescence and floral meristems.

Main scripts:

main_script_basicpipeline_fittings.m  : basic script to extract the characteristic length of a circular fluorescence domain.
main_script_inflorescense_meristem.m  : this script can automatically find the central fluorescent domain of the shoot apical meristem and perform its analysis. This script is very similar to the RegionsAnalysis script main_script_meristem_Landrein_etal_2018.  
main_script_floral_meristems.m  : script for computing the basic features of the fluorescence domains in both the inflorescence and floral meristems.
main_script_ROIs.m : basic script to draw regions of interest and computing basic features  (pending to correct fluorescence computation)

These scripts are part of the RegionsAnalysis package. Run the first part of the script to read the data and get the analysis done.

EXPLANATION OF THE CODE AT A GLANCE
------------------------------------

INPUT:

Folder containing just the LSM Images to analyse, see the provided Landrein_et_al dataset in the subfolder Applications/Landrein_et_al_2018. To use this dataset, you will first need to unzip it. When running the different scripts, you will be asked to select the folder containing the images.

Note that some parameters might need to be set/modified to do your analysis (mostly stored in the inout structure).

OUTPUT:

(Need to be updated, in progress)  

Note the outcome will be dependent on the initialization parameters.

These are some of the expected outputs you should get given the default parameters.

dirdata_features.csv : CSV file where the final features are stored.

subfolders named as each input image containing
- data.mat structure, compiling different information about the image, includying its analysis.
- subfolders containing the performed analysis (e.g. intensity profile fit)
- matlab figure showing the selected ROIs, or a label in the centroid in each the ROI

dirdata.mat: matlab structure compiling different information (eg all the data.mat structures from all images)

selected_regions: folder showing the processed images with the different selected regions (i.e. the central fluorescent domain)
tiffs_orthos_max: maximal intensity orthogonal projections of the images
tiffs_with_numbered_regions
