%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Matlab script for floral meristems analysis. 

% This script performs an analysis in two stages.
% In a first stage, it allows you to draw polygonal ROIS, and gets the characteristics of that.
% In the second stage, it automatically finds the fluorescence domains and
% performs the usual radial study. Note that the radius of the different ROIs
% to analyse will be proportional to the maximal radius of the region found
% (with a lower bound of the radius set by the user).

% Last, the script 'registers' the manually draws ROIs and the automatically
% found ROIs, so that it creates excel files called 'final', that allow you doing comparisons. 

% This script is part of the RegionsAnalysis code.

% RegionsAnalysis code mainly have three structure variables:
% inout structure : provides parameters and strings in relation to input
% and output
% data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.
% dirdata structure: structure compiling the different data structures
% within a folder and some analysis of key features of the different data
% sets.
%
%
% See README.md document for further explanations about the code.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder))
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis')
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

%  Execute initialize_floral_meristemcell function to load the default parameters for the analysis. 
[inout]=initialize_floral_meristemcell;
%%
% MOST RELEVANT PARAMETERS (note these override parameters in  initialize_meristem function)

%   Channels to look at if lsm files
channel_for_manual_drawing_ROIs=2;           
channel_to_automatically_detect=1;

inout.overwriting_mode=1; % Set 1 to activate the overwriting mode, where all files will be analysed. Otherwise set 0, so that just non-analysed files will be analysed. For LSM files. If this is set to 0, inout.get_polygon_from_previous_data will be automatically set to 0.
inout.imshow_in_fire=1;                   % set 1 to show image in fire, 0 otherwise. 
inout.fontsize=22;
inout.get_polygon_from_previous_data=0; % 0 if new data. 1 to get from previous data
inout.max_normalised_level_to_show=0.5; % set a number between 0 to 1. 1 is a non enhanced contrast image.
inout.magnification=180;                % Percentage of image magnification. Note that if this is higher than what the screen allows, it will be rest to the default value, which is 100% magnification.
inout.init_file_to_open=1;              % Initial image number to open. If this line is commented, the default number is one. Useful when willing to execute the pipeline from a certain image number.  

% Parameters for the automatic stage of the pipeline
otsu_threshold_prefactor=0.7; % Otsu threshold prefactor for the automatic domain detection
minimal_radius_region_in_microns=20; % Minimal radius allowed for the radial study of the automatically detected regions
radius_prefactor=2.2; % Prefactor to delimit the size of the domains under study automatically found. If 1, the ROI will have a radius equal to the major axis length found.

inout.zsum=1;                         %   if we want to do a zsum 
inout.zmax=0;                         %   if we want to do a zmax 
inout.gauss_sigma_microns=5;              %   sigma in microns units
inout.radiusstep_in_microns=1;            %   parameter just working for lsm, it overrides the inout.radiusstep parameter, which is in pixel units. 

% Other parameters 

inout.label_type_in_snap='class';    % set 'label' or 'class'
inout.ask_regionclass=1;  % set this parameter to 1 if wanting to class each region after selecting it (single key character), 0 otherwise

inout.defaultregiontype='Polygon';        % type of region that is going to be analysed (current defaultregiontype options: 'Circle' , 'Cake piece', 'Rectangle', 'Polygon'...). Comment this line if different regions are needed to be explored.;
inout.number_of_regions_setting='on_the_fly'; % 'on_the_fly' option allows you to not havin to set the number of regions to mark in advance.

inout.make_orthos=0;                   % set to 1 if making ortogonal slices, otherwise set it to 0. 
inout.ortho_thickness_in_microns=10;   % if this field is removed/commented with '%', then the program asks for a second point to mark the region to study.
inout.ask_numberregions=0;          % set this parameter to 1 if you want matlab ask you how many regions to analyze per file, otherwise set it to 0. 
inout.get_centroids_from_found_domains=0; % parameter to transfer the found domains centroid to the regions of interest.

inout.signal_channel_index=channel_for_manual_drawing_ROIs;           %   Channel to look at if lsm files

% Leave the following parameters here, given that it may be useful in case
% the first pipeline is run after the second (these parameters are
% different in this pipeline)

inout.find_all_domains=0; % set to 1 to find all domains in the xy plane. For lsm files. Otherwise, set to 0.
inout.make_basic_radial=0;
inout.make_fittings=0;               % set to 1 to fit the intensity vs r curves to generalised exponentials and hill functions. Otherwise, set to 0.


%%

% First stage of the pipeline, to mark the different floral organs

% Execute read_rawadata for importing tiff or lsm files, doing the
% pertinent analysis and creating the data structure.

clear data
if inout.open_all_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end

create_dir(fullfile(inout.datapath,'nonaligned_output'));

filein=fullfile(inout.datapath,'dirdata_features.csv');
fileout=fullfile(inout.datapath,'nonaligned_output','dirdata_features_floral_domains.csv');
movefile(filein,fileout)

folderin=fullfile(inout.datapath,'selected_regions');
folderout=fullfile(inout.datapath,'manually_selected_regions');
force_movefolder(folderin,folderout);

if isfield(inout,'init_file_to_open')
    if inout.init_file_to_open>1
        disp('Warning: the data compilation has not taken into account all files given that inout.init_file_to_open>1. To do so, set it back to 1, and run the first part of the pipeline with inout.get_polygon_from_previous_data=1.')
    end
end

%%

% Second stage of the pipeline. Automatic selection of the fluorescence
% domains.
%
inout.signal_channel_index=channel_to_automatically_detect;         %   channel to look at if lsm files
inout.defaultregiontype='Circle';
inout.find_all_domains=1; % set to 1 to find all domains in the xy plane. For lsm files. Otherwise, set to 0.
inout.exclude_central_domain=1; % if find_all_domains==1, setting this parameter to 1 it excludes the central domain.

inout.outsu_threshold_prefactor=otsu_threshold_prefactor;
inout.get_polygon_from_previous_data=0;
inout.data_structure_filename='data_auto';
inout.number_of_regions_setting='by_number';
inout.minimal_radius_region_in_microns=minimal_radius_region_in_microns;
% 
inout.get_centroids_from_found_domains=1; % parameter to transfer the found domains centroid to the regions of interest.

inout.label_type_in_snap='label'    % set 'label' or 'class'
inout.ask_regionclass=0;            % set this parameter to 1 if wanting to class each region after selecting it (single key character), 0 otherwise
inout.make_basic_radial=1;
inout.make_fittings=1;               % set to 1 to fit the intensity vs r curves to generalised exponentials and hill functions. Otherwise, set to 0.

inout.get_radius_from_found_domains=1; % For automatically found domains when using find_all_domains, if wanting to get region to study delimited by the size of those domains, set 1, otherwise set 0.
inout.radius_prefactor=radius_prefactor; % Prefactor to delimit the size of the domains under study. If 1, the ROI will have a radius equal to the major axis length.
inout.open_all_files_in_directory=1; % For doing the automatic analysis to all LSM/LIF files in the directory, set it to 1, otherwise set 0.

clear data

if inout.open_all_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end

filein=fullfile(inout.datapath,['dir' inout.data_structure_filename '_features.csv']);

create_dir(fullfile(inout.datapath,'nonaligned_output'));
fileout=fullfile(inout.datapath,'nonaligned_output','dirdata_all_features_floral_domains.csv')

movefile(filein,fileout)

folderin=fullfile(inout.datapath,'selected_regions');
folderout=fullfile(inout.datapath,'automatically_found_regions')

create_dir(folderout);
force_movefolder(folderin,folderout);

%%
% Data alignememnt process between the domains found in the first and second 
% stages of the pipeline and generation of the final excel files with the
% aligned datasets.

merge_features_datasets(inout,'data','data_auto'); 

%%
% To recreate the manual dirdata structure (outcome of the manual part of the pipeline)
inout.data_structure_filename='data';
create_dirdata(inout)

%%
% To recreate the automatic dirdata structure (outcome of the automatic part of the pipeline)
inout.data_structure_filename='data_auto';
create_dirdata(inout)