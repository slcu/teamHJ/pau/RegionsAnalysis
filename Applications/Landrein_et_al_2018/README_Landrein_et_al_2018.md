Landrein et al Code: Analysis of the central fluorescence expression domain in the Arabidopsis shoot apical meristem
-----------------------------------------------------------------------------------------------------------------------------------------

This Code has been used for the following publication in PNAS (2018):

Title: Nitrate modulates stem cell dynamics in Arabidopsis shoot meristems through cytokinins.

Authors: Benoit Landrein (1), Pau Formosa-Jordan (1), Alice Malivert (1), Christoph Schuster (1), Charles W. Melnyk (1), Weibing Yang (1), Colin Turnbull (2), Elliot M. Meyerowitz (1,3), James C.W. Locke (1), Henrik Jönsson (1,4)

1 Sainsbury Laboratory, University of Cambridge, Bateman Street, Cambridge CB2 1LR, UK.
2 Division of Cell and Molecular Biology, Imperial College London, London, UK.
3 Howard Hughes Medical Institute and Division of Biology and Biological Engineering, California Institute of Technology, Pasadena, CA 91125, USA.
4 Computational Biology and Biological Physics Group, Department of Astronomy and Theoretical Physics, Lund University, S-221 00 Lund, Sweden.

CONTACT

Pau Formosa-Jordan - pformosa@mpipz.mpg.de

Henrik Jönsson - Henrik.Jonsson@slcu.cam.ac.uk

James Locke - James.Locke@slcu.cam.ac.uk

Elliot Meyerowitz - meyerow@caltech.edu


INSTALLATION NOTES
---------------------------

This code has been run using MATLAB 20017a with Windows 7 Professional and in Mac (OS X El Capitan).  

For further details about the installation, see the main Readme file of the RegionsAnalysis repository.


SUMMARY
------------

main_script_meristem_Landrein_etal_2018 script is used for computing the
characteristic length of expression domains at the shoot apical meristem
for different fluorescent reporters. The intensity of such fluorescent domains is
computed as well.

The code can automatically find the central flourescent domain of the shoot apical meristem.
See Supp Methods of Landrein et al 2018 for further details.

This script is part of the RegionsAnalysis package. Run the first part of the script to read the data and get the analysis done.


EXPLANATION OF THE CODE AT A GLANCE
------------------------------------

INPUT:

Folder containing just the LSM Images to analyse, see the provided Landrein_et_al dataset. To use this dataset, you will first need to unzipp it.
When running main_script_meristem_Landrein_etal_2017 script, you will be asked to select the folder containing the images.

Note that some parameters might need to be set/modified to do your analysis (mostly stored in the inout structure).

OUTPUT:

Note the outcome will be dependent on the initialization parameters.

These are some of the expected outputs you should get given the default parameters.

dirdata_features.csv : CSV file where the final features are stored.

subfolders named as each input image containing
- data.mat structure, compiling different information about the image, includying its analysis.
- subfolders containing the performed analysis (e.g. intensity profile fit)
- matlab figure showing the selected ROIs, or a label in the centroid in each the ROI

dirdata.mat: matlab structure compiling different information (eg all the data.mat structures from all images)

selected_regions: folder showing the processed images with the different selected regions (i.e. the central fluorescent domain)
tiffs_orthos_max: maximal intensity orthogonal projections of the images
tiffs_with_numbered_regions

Note: this current version has improved the pipeline to find the central expression domain. Specifically, the algorithm now uses the domain that is closest to the center of mass of all the expression domains found in the image of interest (see find_central_domain function). Still, it is recommended to visually assess the outcome of the central domain.
