# QuickGuide for the use of `main_script_fluorescence_curve_lif.m`
### Contact:
Pau Formosa-Jordan - pformosa@mpipz.mpg.de   
Pau Casanova-Ferrer - pcasanov@math.uc3m.es

## PRE-ANALYSIS: Optimal `.lif` structure
To ease and agile the analysis of the confocal images the script has two optimal file configurations:
1. Only one `.lif` file that contains all the images to analyse sequentially without non used images in between.
2. Several `.lif` files with the same number of images to analyse in all the files and located in the first positions of the `.lif` file.
In both cases, only the `.lif` files to analyse should be on the selected directory.

## ANALYSIS: Parameter definition
Right at the beginning of the script, we added the possibility of defining the location of the `.lif` file to reduce the need to manually select the directory:
```
%Comodities
here="/netscratch/dep_coupland/grp_formosa/pcasanova/Pau Martina/StacksEnric/onlyLD/10LD/Col-0/morpho";
cd(here);
```
Then the location of the `.lif` file should be introduced in the `here` variable. Then, when the pop-up window appears, you will only need to accept the default selection.

The Matlab script behaviour can be modified by the parameters in the structure `inout.`. Most of those parameters are set in the configuration file `Applications/Coupland/Initializations_Coupland/initialize_curve_script.m` but some of them can be modified in the script preamble. Here we will describe the most relevant ones:

First, we will consider the parameters regarding the channel organization of the confocal images (located in lines 48-51)
```
% Channel selection
inout.marker_channel_index_lif=1;       %   marker channel number showing cell outlines 
inout.signal_channel_index_lif=1;       %   signal channel number
inout.number_of_channels_in_lif=1;      % Number of channels the lif file has. If set to 1, the channel under study will be set to 1, overriding the channel number parameters above.
```
From this section, the most relevant parameters to check are `inout.marker_channel_index_lif` which should point to the channel that contains the cell wall signal and `inout.number_of_channels_in_lif` where one has to indicate the number of channels on the confocal images.

Following this, we have several parameters that we do not recommend modifying without contacting the developers (lines 53-65). Then, the next parameters susceptible to modification are in lines 67-69:  
```
% Analysis options
inout.get_polygon_from_previous_data=0;     % set 1 to get polygons from previous data, otherwise set it to 0. Note that if setting this to 1, if you did not analyse all images from the lif project, you need to specify parameters inout.numfiles_to_open
inout.analysis_after_reading=0;             % set to 0 for analysing the data after each particular curve
```
The first one, `inout.get_polygon_from_previous_data` controls if the user wants to reuse a previous region selection "drawing". Then, if this parameter is set to 1 the script will fit again the region stored from a previous analysis. 
The second one controls the order in which the code runs. For the value 1, the user is asked to "draw" the profiles for all the meristems and afterwards all of them are fitted. While for the value 0 the code analyses and store the data of every meristem before asking for the region selection of the next one. The 1 mode allows for a bit less wait between the meristem selection while the 0 mode allows for an easier rerun of a particular meristem whose profile selection must be redone. 

From this point of the preamble (line 71) onwards, the parameters presented are available exclusively for `.lif` files. The following section (lines 73-75) controls the postprocessing done to the image before the region selection. 
```
% Region appearance (.lif)
inout.max_normalised_level_to_show=1;   % set the relative intensity interval [0,inout.max_normalised_level_to_show].
inout.enhance_contrast=1;               % set to 1 to enhance contrast, otherwise set it to 0
```
`inout.max_normalised_level_to_show` allows for the rescaling of the normalized intensity of the image by binning together the pixels with a value higher than `inout.max_normalised_level_to_show`.  
`inout.enhance_contrast` if this parameter is set to 1 the contrast is artificially enhanced in the images.
These two parameters should only be modified from the default value of 1 if you encounter problems with the assistance of the region selection.

The next section (lines 77-99) is the most heterogeneous one:
```
% Analysis options (.lif)
inout.NonEqual_XYResolution=0;      % set to 1 to use images that have diferent X and Y resolution

inout.create_control_paraboloid=0;              % set to 1 to substitute the current image for a control paraboloid
% inout.control_paraboloid_curvature=0.008;     % set the curvature of the control paraboloid (the default value is 0.01)
% inout.control_paraboloid_tilt=10;             % set the tilting from the Zaxis of the control paraboloid in degrees (the default value is 0)

% inout.find_radial_distance=0;     % set to 1 if you want to make a Top view estimation of the meristem radial size using a projection.

inout.correct_Zfocus=1.066;         % set to value obtained from the ImageJ pluggin: https://static-content.springer.com/esm/art%3A10.1038%2Fs41596-020-0360-2/MediaObjects/41596_2020_360_MOESM3_ESM.zip
                                    % More info in Diel et all. Nat Protoc 15, 2773–2784 (2020) https://doi.org/10.1038/s41596-020-0360-2
                                        % Martina Side: 0.957
                                        % Enric Top: 1.066
inout.double_parabola=1;            % set to 0 if you do not want to study the ortho parabola (prepared to work with the current parameters)
                            
inout.work_on_orthos=1;             % set to 1 to use an orthogonal view as main image to work

inout.zmax=0;                       % set to 1 if you want to Zproject all the slides to draw the parabolas
inout.zmax_sandwich=1;              % set to 1 if you want to select a sandwich of slides to Zproject to draw the parabolas

inout.homogeneous_selection=1;      % set to 1 if you want the homogenous stack for the sandwich selection to Zproject

inout.otherOrtho=0;                 % If the orthogonal view is not chosen properly set to 1.
```
By setting `inout.NonEqual_XYResolution` to 1 the script modifies the homogenization of resolution protocol to be able to work with images that do not have the same resolution for the X and Y axis. In that case, the working resolution will the best one of the three axes.      

Following we have the group of 3 parameters that refer to the control mode with the custom paraboloid:
`inout.create_control_paraboloid` activates the substitution of the working image by the custom paraboloid when set to 1. `inout.control_paraboloid_curvature` controls the curvature of the custom paraboloid with the default value being 0.01 $`\mu m`$. Finally `inout.control_paraboloid_tilt` controls the tilting of the meristem in degrees over the XZ plane of the meristem, with a default value of 0º.   

The next parameter `inout.find_radial_distance` is commented to mark that this function is not used currently. But by uncommenting and setting it to 1 the script will allow you to do a manual measurement of the meristem radius over a Z projection of a selected interval of slides.  

In `inout.correct_Zfocus` you can introduce the correction factor obtained using the ImageJ plugin from Diel et all. Nat Protoc 15, 2773–2784 (2020) https://doi.org/10.1038/s41596-020-0360-2. This factor will modify the distance between slides of the stack in order to correct the spherical aberration produced by differences in the refractive index between the objective and the sample. The correction values for the two datasets provided for the testing are also commented in the script `Martina Side: 0.957` and `Enric Top: 1.066`.  

If `inout.double_parabola` is set to 1 the script will also obtain the parabolic profile of the orthogonal view. If the stack does not allow for this orthogonal fitting set this value to 0.   

`inout.work_on_orthos` control the view that will be used during the profile analysis. 
* If the images are taken from the SIDE (in a longitudinal view) this parameter should be 0. 
* If the images are taken from the TOP (in a transversal view) this parameter should be 1. 

The next couple of parameters, `inout.zmax` and `inout.zmax_sandwich`, select the type of image that will be used during the analysis:
* When both are set to 0 (`inout.zmax=0` and `inout.zmax_sandwich=0`) the script will ask you to select a certain slide and the profiling will be done in that slide.
* When `inout.zmax=1` is  (with `inout.zmax_sandwich=0`) the script will use a maximum value projection of the full stack for the analysis.
* When `inout.zmax_sandwich=1` is  (with `inout.zmax=0`) the script will ask you to select two slides and the maximum value projection of all the intermediate slides will be used for the curvature analysis.
We recommend avoiding fitting in only one slide because that measurement is more dependent on the particular choice of a slide than the one using a maximum value projection. Typically is safer to use the `inout.zmax_sandwich=1` mode than the `inout.zmax=1` to avoid primordia that could cover the meristem.

As the orthogonal views typically will be modified during the analysis in order to homogenize the resolutions but this process is done later in the script. Then, if you want to already have the final resolution during the selection you can set `inout.homogeneous_selection=1`. With this, the code will convert the full stack to the final resolution at the cost of a bit more computational time.   
This is especially recommended for images with a larger difference between the resolutions of the XY plane and the Z-axis.

Finally, to close this section of the parameters, we have `inout.otherOrtho` which acts as a toggle of the automatic detection of the proper orthogonal view. If during an analysis you observe that the script fails to properly select the other longitudinal (SIDE) view you should stop the analysis and repeat it (for that particular image) with `inout.otherOrtho=1` to select automatically the other view. These errors are produced by the presence of primordia over the meristem or due to empty space on the laterals of SIDE images. Therefore, it typically occurs in some vegetative or proliferative meristems which are smaller and/or whose primordia are bigger.

The final section of the parameters preamble of the script (lines 101-104) selects the file or files that will be analysed:
```
% Input image (.lif)
inout.open_multiple_lifs=1;         % This parameter changes the behaviour of inout.init_file_to_open, now controls the first file to open and not the images. You will open all the lif files in the directory taking an inout.numfiles_to_open number of images of each file.
inout.numfiles_to_open=1;           % Number of images to open from a lif file. Comment this line with the "%" symbol if wanting to open all images in a lif file
inout.init_file_to_open=2;          % Initial image number to open in a lif file. If this line is commented, the default number is one. Uncomment this line if needed to make the ROIs from a certain image in the lif file onwards.  
```
`inout.open_multiple_lifs` defines if the data that you want to analyse is all in one file (when set to 0) or in several ones (when set to 1) located in the same chosen directory. Depending on the mode the behaviour of the other parameters changes:
* If `inout.open_multiple_lifs=0` then we are analysing only one `.lif` file. Then, `inout.numfiles_to_open` controls the number of images that you want to open from that file and `inout.init_file_to_open` select the first image to open (all images before this one will not be analysed). Is also possible to comment on both lines as is stated in the script comments. 

* If `inout.open_multiple_lifs=1` then we are analysing all the `.lif` files in the directory. Then, `inout.numfiles_to_open` controls the number of images that you want to open from all the files (if a certain file has fewer images you will get an error message) and `inout.init_file_to_open` select the first file to open (all files before this one will not be analysed). In this case, if `inout.numfiles_to_open=1;` is commented the script will try to open as many images as the first file contains from all the files. On the other hand, commenting `inout.init_file_to_open=2;` will act the same as in the previous case but will open from the first file instead of the first image.

## ANALYSIS: Usage recommendations
First, we will present the best methodology to select a couple of slides that define the interval of slides to project.  
The optimal method will be to start from a point before the apex of the meristem and advance through the meristem until the meristem stops increasing and starts decreasing. Then, we continue until the meristem has decreased considerably obtaining the final slide of the interval. Finally, we return through the stack until we observe approximately the meristem the same meristem height but on the other side of the apex. That point will be the initial slide of the interval.   
In both cases, one should avoid including in the interval primordia that could mask the profile of the meristem.   

Then, given that the meristem size can be obtained with a different measurement not linked with the parabolic profile (using the MorphographX protocol) there is more freedom to select the region to fit. There is no longer the need for selecting the meristem fully between the first couple of visible primordia. Now is better to restrict the "drawing of the parabola" more to the apex region of the meristem to avoid including incipient primordia which would reduce the observed curvature.

## POST-ANALYSIS: Results location

