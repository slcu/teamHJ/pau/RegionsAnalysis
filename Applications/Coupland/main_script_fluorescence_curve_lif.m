%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% main_script_fluorescence_curve_lif is a variation of 
% main_script_fluorescence_multipolygons_twochannels_lif Matlab script 
% it allows to draw curves in lsm or lif files, and extract its characteristics. 

% This code has been tested in lif formatted input files.If other
% input file formats are used, be aware there might be errors.

% This script is part of the RegionsAnalysis code.

% RegionsAnalysis code mainly have three structure variables:

% 1) inout structure : provides parameters and strings in relation to input
% and output

% 2) data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.

% 3) dirdata structure: structure compiling the different data structures 
% within a folder and some analysis of key features of the different data
% sets.

% If loading the dirdada structure, you can access the regions analysis 
% by typing dirdata.dataset{i}.region{j}, beisng i and j two indices.

% See README.md document for further explanations about the code.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Comodities
here="/netscratch/dep_coupland/grp_coupland/Martina/WUS_and_AP2/pWUS_Venus__pCLV3_mCherry__Shift__experiment/LDs/13DAS";
cd(here);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

%  Execute initialize_multipolygons_twochannels function to load the default parameters for the analysis. 
[inout]=initialize_curve_script;
%%

% MOST RELEVANT PARAMETERS (note these parameters might override parameters set by the previous initialize function)
% NOTE: do not move these parameters above the initialization line.

% Channel selection
inout.number_of_channels_in_lif=3;      % Number of channels the lif file has. If set to 1, the channel under study will be set to 1, overriding the channel number parameters above.
inout.marker_channel_index_lif=1;       %   marker channel number showing cell outlines 
inout.signal_channel_index_lif=2;       %   signal channel number

% Region type
inout.defaultregiontype='Polygon';      % type of region that is going to be analysed (current defaultregiontype options: 'Circle' , 'Cake piece', 'Rectangle', 'Polygon'...). Comment this line if different regions are needed to be explored.;
inout.polygontype='opened';             % type of polygon; set either 'closed' or 'opened'
inout.draw_freehand_polygon=1;          % enables drawing of the polyigon in a freehand style. Otherwise set it to 0, and multiple clicks will be needed to plot the polygon. Option available just without zoom

% Region appearance
inout.make_zoom=0;              % set to 1 if zoom needed for drawing ROI, and 0 otherwise. When set to 1, place cursor in the region where zoom is needed, and press click.  
inout.magnification=140;        % Percentage of image magnification. Note that if this is higher than what the screen allows, it will be rest to the default value, which is 100% magnification.
inout.imshow_in_fire=1;         % set 1 to show image in fire, 0 otherwise.

% Region labeling
inout.number_of_regions_setting='by_number';        % 'on_the_fly' option allows you to not having to set the number of regions to mark in advance. When being 'on_the_fly' mode, press space bar to start selecting the next ROI, or press key 'q' to finish selecting ROIs in the current image. Otherwise, set 'by_number'.
inout.ask_numberregions=0;                          % set this parameter to 1 if you want matlab ask you how many regions to analyze per file, otherwise set it to 0. 

% Analysis options
inout.get_polygon_from_previous_data=0;     % set 1 to get polygons from previous data, otherwise set it to 0. Note that if setting this to 1, if you did not analyse all images from the lif project, you need to specify parameter inout.numfiles_to_open
inout.analysis_after_reading=0;             % set to 0 for analysing the data after each particular curve

%%% Just for LIF files

% Region appearance (.lif)
inout.max_normalised_level_to_show=1;   % set the relative intensity interval [0,inout.max_normalised_level_to_show].
inout.enhance_contrast=1;               % set to 1 to enhance contrast, otherwise set it to 0

% Analysis options (.lif)
inout.NonEqual_XYResolution=0;      % set to 1 to use images that have diferent X and Y resolution

inout.create_control_paraboloid=0;              % set to 1 to substitute the current image for a control paraboloid
% inout.control_paraboloid_curvature=0.008;     % set the curvature of the control paraboloid (the default value is 0.01)
% inout.control_paraboloid_tilt=10;             % set the tilting from the Zaxis of the control paraboloid in degrees (the default value is 0)

% inout.find_radial_distance=0;     % set to 1 if you want to make a Top view estimation of the meristem radial size using a projection.

inout.correct_Zfocus=0.957;         % set to value obtained from the ImageJ pluggin: https://static-content.springer.com/esm/art%3A10.1038%2Fs41596-020-0360-2/MediaObjects/41596_2020_360_MOESM3_ESM.zip
                                    % More info in Diel et all. Nat Protoc 15, 2773–2784 (2020) https://doi.org/10.1038/s41596-020-0360-2
                                        % Martina Side: 0.957
                                        % Enric Top: 1.066
inout.double_parabola=1;            % set to 0 if you do not want to study the ortho parabola (prepared to work with the current parameters)
                            
inout.work_on_orthos=0;             % set to 1 to use an orthogonal view as main image to work

inout.zmax=0;                       % set to 1 if you want to Zproject all the slides to draw the parabolas
inout.zmax_sandwich=1;              % set to 1 if you want to select a sandwich of slides to Zproject to draw the parabolas

inout.homogeneous_selection=1;      % set to 1 if you want the homogenous stack for the sandwich selection to Zproject

inout.otherOrtho=0;                 % If the orthogonal view is not choosen properly set to 1.

% Input image (.lif)
inout.open_multiple_lifs=0;         % This parameter changes the behaviour of inout.init_file_to_open, now controls the first lif to open and not the images on a lif. You will open all the lif files in the directory taking a inout.numfiles_to_open number of images of each file.
%inout.numfiles_to_open=1;           % Number of images to open from a lif file. Comment this line with the "%" symbol if wanting to open all images in a liff file
inout.init_file_to_open=1;          % Initial image number to open in a lif file. If this line is commented, the default number is one. Uncomment this line  if needed to make the ROIs from a certain image in the lif file onwards.  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Execute read_rawadata for importing lif, tiff or lsm files, doing the
% pertinent analysis and creating the data structure for each image, and the dirdata structure if necessary.
if inout.number_of_channels_in_lif==1
        inout.signal_channel_index=1;
        inout.marker_channel_index_lif=1;
else
        inout.signal_channel_index=inout.marker_channel_index_lif;      % Do not modify this parameter for the current pipeline;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
clear data
if inout.open_all_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout);
end
%%
% %  Renaming dirdata structure
% filein=fullfile(inout.datapath,['dir' inout.data_structure_filename '_features.csv']);
% fileout=fullfile(inout.datapath,'dirdata_curve_features.csv'); 
% movefile(filein,fileout)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END of the basic pipeline
%%
% This bit of code reads the different data.mat files within the subfolders inside
% the selected data folder, and generates the superstructure dirdata. Some
% selected features are exported into a csv file. 
% here="/netscratch/dep_coupland/grp_formosa/pcasanova/Pau Martina/pCVL3 pWUS April 2021/24DAS";
cd(here);
[inout]=initialize_curve_script;
create_dirdata_and_export_cvs(inout)

% To extract dirdata structure, write e.g. dirdata.dataset{number}
%%
%xlswrite('text.xls',my_data)
%writetable(struct2table(yourstructure), 'someexcelfile.xlsx')

%%
% inout = rmfield(inout,'init_file_to_open')
% inout = rmfield(inout,'numfiles_to_open')

%%
% For recursive analysis in a folder that contains multiple folders 
% inout.data_files_format='lif'; % field used just for the recursive analysis
% run_RegionsAnalysis_recursively(inout);
