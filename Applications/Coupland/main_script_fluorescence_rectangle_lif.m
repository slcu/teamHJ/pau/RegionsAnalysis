%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% main_script_fluorescence_Rectangle_lif is a variation of 
% main_script_fluorescence_multipolygons_twochannels_lif Matlab script 
% it allows to draw different regions of
% interest (ROI) in lsm or lif files, and extract its characteristics. This script has
% been optimised for drawing polygons as ROIs. It has been tested in lif
% format (but other formats could work as well)

% This script is part of the RegionsAnalysis code.

% RegionsAnalysis code mainly have three structure variables:

% 1) inout structure : provides parameters and strings in relation to input
% and output

% 2) data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.

% 3) dirdata structure: structure compiling the different data structures 
% within a folder and some analysis of key features of the different data
% sets.

% If loading the dirdada structure, you can access the regions analysis 
% by typing dirdata.dataset{i}.region{j}, being i and j two indices.

% See README.md document for further explanations about the code.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

%  Execute initialize_multipolygons_twochannels function to load the default parameters for the analysis. 
[inout]=initialize_multipolygons_twochannels;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% MOST RELEVANT PARAMETERS (note these parameters might override parameters set by the previous initialize function)
% NOTE: do not move these parameters above the initialization line.

inout.get_polygon_from_previous_data=0; % set 1 to get polygons from previous data, otherwise set it to 0. Note that if setting this to 1, if you did not analyse all images from the lif project, you need to specify parameter inout.numfiles_to_open

inout.marker_channel_index_lif=2;           %   marker channel number showing cell outlines
inout.signal_channel_index_lif=1;           %   signal channel number

inout.make_zoom=0; % set to 1 if zoom needed for drawing ROI, and 0 otherwise. When set to 1, place cursor in the region where zoom is needed, and press click.  
inout.magnification=140; % Percentage of image magnification. Note that if this is higher than what the screen allows, it will be rest to the default value, which is 100% magnification.
inout.defaultregiontype='Rectangle';   % type of region that is going to be analysed (current defaultregiontype options: 'Circle' , 'Cake piece', 'Rectangle', 'Polygon'...). Comment this line if different regions are needed to be explored.;

inout.number_of_channels_in_lif=2; % Number of channels the lif file has. If set to 1, the channel under study will be set to 1, overriding the channel number parameters above.
inout.number_of_regions_setting='on_the_fly'; % 'on_the_fly' option allows you to not having to set the number of regions to mark in advance. When being 'on_the_fly' mode, press space bar to start selecting the next ROI, or press key 'q' to finish selecting ROIs in the current image. Otherwise, set 'by_number'.
%%

% Just for LIF files
inout.numfiles_to_open=4; % Number of images to open from a lif file. Comment this line with the "%" symbol if wanting to open all images in a liff file
inout.init_file_to_open=4; % Initial image number to open in a lif file. If this line is commented, the default number is one. Uncomment this line  if needed to make the ROIs from a certain image in the lif file onwards.  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Execute read_rawadata for importing lif, tiff or lsm files, doing the
% pertinent analysis and creating the data structure for each image, and the dirdata structure if necessary.
if inout.number_of_channels_in_lif==1
        inout.signal_channel_index=1;
else
        inout.signal_channel_index=inout.marker_channel_index_lif; % Do not modify this parameter for the current pipeline;
end

clear data
if inout.open_all_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end
%%
%  Renaming dirdata structure
filein=fullfile(inout.datapath,['dir' inout.data_structure_filename '_features.csv']);
fileout=fullfile(inout.datapath,'dirdata_features_marker.csv')
 
movefile(filein,fileout)



