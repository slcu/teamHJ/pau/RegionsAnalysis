%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% main_script_inflorescense_meristem is used for computing the
% characteristic length of expression domains at the shoot apical meristem 
% for different fluorescent reporters. The intensity of such fluorescent domains is
% computed as well. 

% The code can automatically find and analyse the flourescent domain of the 
% influorescense meristem, referred here also as the central domain. 
% The algorithm finds the domain that is closest to the 
% center of mass of all the expression domains found in the image of interest 
% (see find_central_domain function).
% Still, it is recommended to visually assess the outcome of the central domain. 

% Note this script is similar to the one found at 
% Applications/Landrein_et_al_2018/main_script_meristem_Landrein_etal_2018, 
% which was used in Landrein et al 2018 (PNAS). 

% This code has been optimised for lif formatted input files. If other
% input file formats are used, be aware there might be errors.

% See Supplementary Materials and Methods of Landrein et al 2018 for further details.
% Also, see README.md document in  Landrein_et_al_2018 folder for further 
% explanations about this script and its outcome.
 
% Landrein B, Formosa-Jordan P, Malivert A  et al. (2018) Nitrate modulates
% stem cell dynamics in Arabidopsis shoot meristems through cytokinins.
% Proc. Nat. Acad. Sci. USA. 115: 1382-1387.

% This script is part of the RegionsAnalysis code.

% RegionsAnalysis package mainly has three structure variables:
% inout structure : provides parameters and strings in relation to input
% and output
% data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.
% dirdata structure: structure compiling the different data structures
% within a folder and some analysis of key features of the different data
% sets.

% See README.md document in the RegionsAnalysis folder for further 
% explanations about the package.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Comodities
here="/netscratch/dep_coupland/grp_formosa/pcasanova/Pau Martina/pCVL3 pWUS April 2021/16DAS";
cd(here);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

%  Execute initialize_meristemcell function to load the default parameters for the analysis. 
[inout]=initialize_domains_script;

% MOST RELEVANT PARAMETERS (note these parameters might override parameters set by the previous initialize function)

%Channel selection:
inout.marker_channel_index_lif=1;       % marker channel number showing cell outlines (useful for Top automatic detection)
inout.signal_channel_index=2;           % channel to look at if lsm or lif files

% Thresholding:
inout.outsu_threshold_prefactor=1;      % prefactor for the otsu thresholding. If 1, the Otsu factor is taken. For dimmer signal take prefactors between 0 and 1.
inout.num_thresholds=2;                 %  number of thresholds to obtain via otsu thresholding. To avoid overlaping of nearby expresion domains take aditional thresholds.

% Region selection:
inout.click_selecting_regions=0;        % set to 1 for selecting regions by clicking, set to 0 to load it from previous data or it will find the origin of the central domain. 
inout.get_center_and_radius_from_previous_data=0; % for loading from previous data. It just works for circular regions (it will load origin and radius used).
inout.find_central_domain=1;            % set to 1 to find the central domain in the xy plane.
inout.get_center_from_cental_domain=1;  % set to 1 to find the origin from the central domain. Note that the radius of the selected region will still be set by the inout.radius_region_in_microns parameter. 

% Orthogonal views:
inout.make_orthos=1;                    % set to 1 if making ortogonal slices, otherwise set it to 0. 
% inout.ortho_thickness_in_microns=40;  % uncomment it in case the ortho is constrained in a smaller "sandwich" (see code for further understanding)
% inout = rmfield(inout,'ortho_thickness_in_microns'); % 

% Analisys options:
inout.zsum=0;                               % if we want to do a zsum 
inout.zmax=1;                               % if we want to do a zmax
% inout.gauss_sigma_microns=5;              % sigma in microns units
inout.radiusstep_in_microns=1;              % it overrides the inout.radiusstep parameter, which is in pixel units.  
inout.radius_region_in_microns=50;          % if this field is removed/commented with '%', then the program asks for a second point to mark the region to study.

% Less important parameters
inout.islsm=0;
inout.defaultnumberregions=1;               % default number of regions to analyse in each file
inout.defaultnumberbackgroundregions=1;     % default number of background regions to analyse in each file
inout.ask_numberregions=0;                  % set this parameter to 1 if you want matlab ask you how many regions to analyze per file, otherwise set it to 0. 
inout.select_background_regions=0;          % set to 1 for selecting background regions, 0 otherwise. Note this will be subtracted from the image to analyse if inout.background_subtraction=1
inout.background_subtraction=0;             % set this parameter to 1 to do the background subtraction, and to 0 otherwise.         
inout.post_analysis=0;                      % set this parameter to 1 to do the fit to averaged exponent found after fittings, otherwise set to 0. Note this is possible when there is just one selected region per image.
inout.make_fittings=1;                      % set to 1 to fit the intensity vs r curves to generalised exponentials and hill functions. Otherwise, set to 0.
inout.plotwinfit=1;                         % set to 1 to plot the resulting fittings, and to 0 otherwise.
%inout.fit_fixed_exponent=0;                % do a second fitting analisis, with the average exponents. NOTE THIS parameter is overritten in RegionsAnalysis
inout.make_historegion=1;                   % this would plot the intensity histogram of the selected region
inout.fit_hole=0;                           % for fitting TCS signal (circular gradient-like shape with a dip in the center), set it to 1, otherwise, to 0
% inout.get_mask_from_cental_domain=1;

inout.data_structure_filename=['data_domains_channel_' num2str(inout.signal_channel_index)] ;
inout.selected_channel_string=['channel_' num2str(inout.signal_channel_index)];

% Just for LIF files

% Analisys options (.lif):
inout.NonEqual_XYResolution=0;      % set to 1 to use images that have diferent X and Y resolution
inout.correctZfocus=0.966;          % set to value obtained from the ImageJ pluggin: https://static-content.springer.com/esm/art%3A10.1038%2Fs41596-020-0360-2/MediaObjects/41596_2020_360_MOESM3_ESM.zip
                                    % More info in Diel et all. Nat Protoc 15, 2773–2784 (2020) https://doi.org/10.1038/s41596-020-0360-2
                                        % Martina Side: 0.957
inout.work_on_orthos=1;             % in case we want to use one of the orthos as main image to work, set this parameter to 1, otherwise leave it to 0.
inout.otherOrtho=0;                 % If the lowest intensity border is not the Top set to 1.

% Input image (.lif):
inout.open_multiple_lifs=0;         % This parameter changes the behaviour of inout.init_file_to_open, now controls the first lif to open and not the images on a lif. You will open all the lif files in the directory taking a inout.numfiles_to_open number of images of each file.
inout.numfiles_to_open=1;           % Number of images to open from a lif file. Comment this line with the "%" symbol if wanting to open all images in a liff file
inout.init_file_to_open=4;          % Initial image number to open in a lif file. If this line is commented, the default number is one. Uncomment this line  if needed to make the ROIs from a certain image in the lif file onwards.  
% inout.wrong_slides=[7];           % If there is some puntual problem (overexpression) in a particular slide insert the value to substitute it for an interpolation of the neigbouring slides

clear data
if inout.open_all_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END of the basic pipeline
%%

% This bit of code reads the different data.mat files within the subfolders inside
% the selected data folder, and generates the superstructure dirdata. Some
% selected features are exported into a csv file. 

% This bit of code reads the different data.mat files within the subfolders inside
% the selected data folder, and generates the superstructure dirdata. Some
% selected features are exported into a csv file. 
% here="/netscratch/dep_coupland/grp_formosa/pcasanova/Pau Martina/pCVL3 pWUS April 2021/7DAS";
cd(here);
[inout]=initialize_domains_script;
create_dirdata_and_export_cvs(inout)

% To extract dirdata structure, write e.g. dirdata.dataset{number}
%%
% For post regions selection in a directory. Finding the average exponent,
% doing the fitting again to such exponent, and exporting a new cvs file. 
[inout]=initialize_domains_script;
inout.signal_channel_index=2;
[dirdata]=postanalysis_regions(inout);

[inout]=initialize_domains_script;
inout.signal_channel_index=3;
[dirdata]=postanalysis_regions(inout);

%%
% inout = rmfield(inout,'init_file_to_open')
% inout = rmfield(inout,'numfiles_to_open')
%%
% For recursive analysis in a folder that contains multiple folders 
%inout.data_files_format='lif'; % field used just for the recursive analysis
% run_RegionsAnalysis_recursively(inout);

