% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

datapath="/home/pcasanova/Documents/Meristem/LocalData/Estandar"
fname=fullfile(datapath,dir(fullfile(datapath,'*.lif')).name);
[imgout]=ci_loadLif(fname, 3);
imaux=imgout.Image(2);
imauxaux=imaux{1};
num_images=ndims(imauxaux);
show(imauxaux)