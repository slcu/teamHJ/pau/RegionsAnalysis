function [inout]=initialize_domains

% Parameter settings
[inout]=initialize_default_parameters; % initialize default parameters


% general parameters
inout.open_all_files_in_directory=1; % set to 1 for opening all the tiffs or lsm files in a directory, 0 otherwise.
inout.lsm_signal_channel_index=1;    % channel to look at if lsm files
inout.zsum=0;                    % if we want to do a zsum 
inout.zmax=1;                    % if we want to do a zmax 
inout.lsm_gauss_filter=1;            % if we want to do a gaussian filter
inout.gauss_sigma_microns=5;         %   sigma in microns units
inout.fit_hole=0;                         %   for fitting TCS signal (circular gradient-like shape with a dip in the center), set it to 1, otherwise, to 0
inout.r_crit_thresh=0;              % set to 1 if you want to compute a threshold-based characteristic length, otherwise set it to 0
inout.max_normalised_level_to_show=0.2; % set a number between 0 to 1. 1 is a non enhanced contrast image. 

inout.select_region=0;              % set to 1 for selecting regions, 0 otherwise.
inout.select_background_regions=0;   % set to 1 for selecting background regions, 0 otherwise
inout.snap_regions_with_labels=1;   % set to 1 for drawing the regions labels in a figure, 0 otherwise. 
inout.export_subfolderdatapath=1;   % set to 1 for exporting files in a subfolder where the raw data is, 0 for exporting to the same folder as raw data. 
inout.standard_format_filename=0;   % set to 0 to load TIFF files with arbitrary names.
inout.export_data=1;                % set to 1 to export data structure into the output folder after reading data and so on.
inout.num_channels=1;               % number of channels of your image. Please set to 1 for the moment.
inout.create_dirdata=1;             % if all files in a directory are explored, this gives the possibility to create another structure, called dirdata in the raw data directory. 
inout.defaultregiontype='Circle';   % type of region that is going to be analysed (current defaultregiontype options: 'Circle' and 'Cake piece'). Comment this line if different regions are needed to be explored.;
inout.defaultbackgroundregiontype='Circle';

inout.defaultnumberregions=1;       % default number of regions to analyse in each file
inout.defaultnumberbackgroundregions=1; % default number of background regions to analyse in each file


inout.show_label_image=1 % 
inout.make_snap_with_numbered_centroid_regions=1; % export the raw image with labels in the centroids of the regions

inout.make_orthos=0;                  % set to 1 if making ortogonal slices, otherwise set it to 0. 

% pop-up-related parameters

inout.ask_numberregions=0;          % set this parameter to 1 if you want matlab ask you how many regions to analyze per file, otherwise set it to 0. 
inout.ask_labelregions=0;           % set this parameter to 1 if you want matlab ask you the labels of the regions
inout.ok_boxes=0;                   % set this parameter to 1 if you want matlab poping-up 'oks' during the RegionAnalysis execution

inout.ask_numberbackgroundregions=0;

% data analysis parameters
inout.focus_on_single_z_tiff=0; % for tiff
inout.tiff_max_int=1;

inout.analysis_regions=0;           % set to 1 for analysing the different regions, 0 otherwise.
inout.analysis_after_reading=1;     % set to 1 for analysing the data after doing all the files opening

inout.export_data_analysis=1;       % set to 1 for exporting data analysis into the data structure.
inout.analysis_regions=0;
inout.radiusstep=2;                 % set the differential radial length (dr) for doing the circle integration
inout.radiusstep_in_microns=1;      % just working for lsm, override the inout.radiusstep. 
inout.plotfits=1;                   % set to 1 to plot the resulting fittings, and to 0 otherwise.
inout.plotwinfit=0;                 % set to 1 to plot the resulting fittings, and to 0 otherwise.
inout.post_analysis=0;               %   set this parameter to 1 to do the fit to averaged exponent found after fittings, otherwise set to 0. Note this is possible when there is just one selected region per image.
%inout.fit_fixed_exp=1;
inout.compute_int_vs_r=1            % set to 1 to compute and plot the intensity vs r in the rectangle or circular region. Otherwise, set it to 0.

inout.crit_conc=10;                 % critical threshold fluorescense to define a threshold-based characteristic size.
inout.make_basic_radial=1; 
inout.make_fittings=1;              % set to 1 to fit the intensity vs r curves to generalised exponentials and hill functions. Otherwise, set to 0.
inout.make_historegion=0;           % this would plot the intensity histogram of the selected region

inout.extract_features=0;         % set this parameter to 1 to extract dirdata information, otherwise set it to 0;  
inout.islsm=1;

% other stuff
inout.timecourse=0;
inout.background_subtraction=0; 
inout.make_movie=0;


% labels and class related
inout.color_labels='white';
inout.color_labels_while_selection='black';
inout.plot_ROIs_in_snap_all_regions=1;

inout.get_center_and_radius_from_previous_data=0; % for loading from previous data. It just works for circular regions (it will load origin and radius used).
inout.find_central_domain=0; % set to 1 to find the central domain in the xy plane. For lsm files. Otherwise, set to 0.
inout.get_center_from_cental_domain=0; % set to 1 to find the origin from the central domain. Note that the radius of the selected region will still be set by the inout.radius_region_in_microns parameter. 


inout.imshow_in_fire=1;                   % set 1 to show image in fire, 0 otherwise. 
inout.extract_features_stats=0;

% End of the parameters definition

str = which(check_computer);
[inout.codefolder,inout.datafolder]=check_computer;


datafolder=inout.datafolder;
datapath = uigetdir(datafolder,'Select the data set location');

if ~datapath
return;
end

disp('chosen directory is');
disp(datapath)

inout.datapath=datapath;


tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename)

addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

inout.schnitzpath=fullfile(inout.codefolder(1:25),'Schnitzcells');
addpath(genpath(inout.schnitzpath))