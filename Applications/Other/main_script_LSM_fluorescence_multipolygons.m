%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% main_LSM_fluorescence_multipolygons Matlab script allows to draw different regions of
% interest (ROI) in lsm files, and extract its characteristics. This script has
% been optimised for drawing polygons as ROIs.

% This script is part of the RegionsAnalysis code.

% RegionsAnalysis code mainly have three structure variables:
% inout structure : provides parameters and strings in relation to input
% and output
% data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.
% dirdata structure: structure compiling the different data structures
% within a folder and some analysis of key features of the different data
% sets.

% See readme.doc document for further explanations about the code.

% Contact: pau.formosajordan@slcu.cam.ac.uk
% Last version: 29/03/2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

%  Execute initialize_meristemcell function to load the default parameters for the analysis. 
[inout]=initialize_LSM_multipolygons;

% MOST RELEVANT PARAMETERS (note these parameters might override parameters set by the previous initialize function)

inout.defaultregiontype='Polygon';   % type of region that is going to be analysed (current defaultregiontype options: 'Circle' , 'Cake piece', 'Rectangle', 'Polygon'...). Comment this line if different regions are needed to be explored.;
inout.make_historegion=1; % set to 1 to make histograms for the regions of interest, otherwise set to 0.

inout.get_polygon_from_previous_data=0; % set 1 to get polygons from previous data (note, then also set inout.click_selecting_regions=0), otherwise set it to 0
inout.click_selecting_regions=1;    % set to 1 for selecting regions by clicking, set to 0 to load it from previous data or it will find the origin of the central domain. 
inout.signal_channel_index=1;
inout.extract_features=1;    % set to 1 to extract the basic computed features into a csv file, otherwise set to 0.
inout.extract_features_stats=0;  % set to 1 to extract some averages of the basic computed features into a csv file, otherwise set to 0. Not functional if more than one region are selected per file.

inout.imshow_in_fire=0;                   % set 1 to show image in fire, 0 otherwise. 

inout.lsm_signal_channel_index=1;         %   channel to look at if lsm files
inout.lsm_zsum=1;                         %   if we want to do a zsum 
inout.lsm_zmax=0;                         %   if we want to do a zmax 

%
inout.background_subtraction=0;           %   set this parameter to 1 to do the background subtraction, and to 0 otherwise.         
inout.select_background_regions=0;        %   set to 1 for selecting background regions, 0 otherwise. Note this will be subtracted from the image to analyse if inout.background_subtraction=1

% Less important parameters
inout.ask_numberregions=1;                %   set this parameter to 1 if you want matlab ask you how many regions to analyze per file, otherwise set it to 0. 
inout.defaultnumberregions=0;             %   default number of regions to analyse in each file
inout.defaultnumberbackgroundregions=1;   %   default number of background regions to analyse in each file


% Execute read_rawadata for importing tiff or lsm files, doing the
% pertinent analysis and creating the data structure.

clear data
if inout.open_all_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end


%%

% This bit of code reads the different data.mat files within the subfolders inside
% the selected data folder, and generates the superstructure dirdata. Some
% selected features are exported into a csv file. 

% To extract dirdata set, write 
% e.g. dirdata.dataset{number}

[inout]=initialize_LSM_multipolygons;
[dirdata]=create_dirdata(inout);

