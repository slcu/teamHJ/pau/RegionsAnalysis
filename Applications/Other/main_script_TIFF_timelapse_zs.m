%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Matlab script for analysing 3D time-lapse movies in tiff format. 

% A featured outcome of this script is to show the fluorescence across a round or
% rectangular region of internest.

% This script is part of the RegionsAnalysis code.

% RegionsAnalysis code mainly have three structure variables:
% inout structure : provides parameters and strings in relation to input
% and output
% data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.
% dirdata structure: structure compiling the different data structures
% within a folder and some analysis of key features of the different data
% sets.
%
% Contact: pau.formosajordan@slcu.cam.ac.uk
%
% See README.md document for further explanations about the code.
%
% Last version: 25/02/2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialize parameters

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

%% NOTE: THIS SCRIPT IS STILL UNDER CONSTRUCTION.
%
% Initialization

[inout]=initialize_fluor_analysis; % some default parameters for this script

inout.scope='spo'; % Spinning disk settings for clock movie
inout.scope='spy'; % Spinning disk settings for H2B movie
inout.scope='spi'; % Spinning disk but without Dual filters is 'spu'. Otherwise say 'spi'
inout.scope='readnb'; %to extract the channel from the nd file
inout.ndfilename='snap.nd'; % in case of reading the nd file
inout.tif_string_file_to_select=[inout.ndfilename(1:end-3) '_*w1*t1.tif'];
inout.ndsignal=1; % when reading nd file, it is assumed the first wavenumber is the signal, and the second is the marker; ie, this parameter is set to 1.

inout.thresh_fluor_marker=780;  % from snap1 s1, t1.
inout.thresh_fluor_marker=720;  % from snap1 s1, t1.

inout.thresh_fluor_signal=1100;
inout.threshold_backgroun_subtraction=1;
inout.load_data_for_background_file=0;
inout.extract_regions_mean_features=1;

% specific parameters to initialize_fluor_analysis
inout.marker_channel_index=2; % comment this line if there is no marker in the lsm file
inout.signal_channel_index=inout.ndsignal; % signal channel to look at if lsm files
inout.get_polygon_from_previous_data=0;
%%
% 

inout.focus_on_single_z_tiff=0;
inout.select_z_stack=0;
inout.smooth_timecourse=0;
inout.smooth_in_z=1;
inout.num_zs_for_smoothing=21;
inout.defaultnumberregions=5;

inout.max_fluor_to_plot=2500;


inout.open_all_files_in_directory=0;
inout.open_all_positions=1; % For opening all the positions in a directory.

inout.defaultregiontype='Circle'
inout.make_basic_radial=0;
inout.plot_timecourse_signal=0; % set 1 for signal, 0 for the marker
inout.plot_time_in_frames=0;
inout.hours_per_frame=0.5;
inout.median_filter=0;
inout.tiff_max_int=1;

inout.make_movie_combinedchannels=1 
inout.make_movie_juxtchannels=1;
inout.make_movie=1;

inout.number_of_channels_to_compute_intensity=1;

%%
% initialisation for fluorescence analysis of the gradient movie

inout.select_region=1;              % set to 1 for selecting regions, 0 otherwise.
inout.analysis_regions=1;
inout.defaultregiontype='Rectangle'
inout.number_tiff_channels=2;
inout.defaultnumberregions=1;
inout.make_basic_radial=1; % to create movie vs x set it to 1.
inout.magnification=240; % Percentage of image magnification. Note that if this is higher than what the screen allows, it will be rest to the default value, which is 100% magnification.
inout.open_all_positions=0;
inout.number_of_regions_setting='on_the_fly'; % 'on_the_fly' option allows you to not having to set the number of regions to mark in advance. When being 'on_the_fly' mode, press space bar to start selecting the next ROI, or press key 'q' to finish selecting ROIs in the current image. Otherwise, set 'by_number'.
inout.focus_on_single_z_tiff=1; % required to set to 1 for doing the smoothening
inout.spatial_filtering=1; % double median filter for the fluorescence spatial analysis

inout.gradient_marker_max=150;
inout.make_kymo_region=1; % set it to 1 if kymos computation for rectangular regions, otherwise set it to 0.

%%
% parameters in case of movie generation [ note; for movies also see
% main_script_TIFF_movie; the following bit then might be removed, given this is not the
% central point of this script]

inout.select_region=0;              % set to 1 for selecting regions, 0 otherwise.
inout.analysis_regions=0;

inout.focus_on_single_z_tiff=1;
inout.select_z_stack=0;
inout.smooth_timecourse=0;
inout.smooth_in_z=1;
inout.num_zs_for_smoothing=21;
inout.defaultnumberregions=5;

inout.max_fluor_to_plot=2500;

inout.plot_timecourse_signal=0; % set 1 for signal, 0 for the marker

inout.open_all_files_in_directory=0;
inout.open_all_positions=1; % For opening all the positions in a directory.

inout.defaultregiontype='Circle'
inout.make_basic_radial=0;
inout.plot_timecourse_signal=0; % set 1 for signal, 0 for the marker
inout.plot_time_in_frames=0;
inout.hours_per_frame=0.5;
inout.median_filter=0;
inout.tiff_max_int=1;

inout.make_movie_combinedchannels=1 
inout.make_movie_juxtchannels=1;
inout.make_movie=1;
inout.saturation_percentile_signal=99.99 % default is 70.
inout.saturation_percentile_marker=99.99 % default is 50.

%%

inout.standard_format_filename=4;
inout.defaultregiontype='Line';
%inout.defaultregiontype='Tilted rectangle';

inout.zsum=1;

%%

clear data

if inout.open_all_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end


%%
add_regions_and_resume_analysis(inout.datapath)
%%
% Execute create_dirdata for creating and loading a particular dirdata structure and playing with it
% To extract dirdata set, write 
% e.g. dirdata.dataset{number}

[dirdata,datapath]=create_dirdata;

%%
% Execute import_data loading a particular data structure and playing with it
% To extract data set, write data.featureofinterest
% e.g. data.region{number}
% e.g. data.region{1}.regionmask shows the mask of the selected region
[data]=import_data;

%%
% Given a list of regions to analize in a data structure, analysis_regions
% provide differerent tools to analyze the data.
% 3 main options for passing a lis:
% 1.- Using the list in data.regionlist, what contains all regions analysed
% for a data set.
% 2.- Using the temporary list data.regionlisttemp (updated every time one
% adds new regions)
% 3.- Using a particular list set by you, e.g. list_of_regions=[2 1]

list_of_regions=data.regionlist;

%list_of_regions=data.regionlisttemp;  % Note data.regionlisttemp 
%list_of_regions=[2,1];
[data]=analysis_regions(inout,data,list_of_regions);
%%
plot_regions_mean_features(inout,data,data.regionlist);
%%

% Use add_regions in case you have already a data structure and you want to keep it but add new regions.
% Note that add_regions function calls import_data function

[data]=add_regions_to_study(data);


%%

make_snap_all_regions(inout,data)

