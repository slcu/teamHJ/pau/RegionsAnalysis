%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Matlab script for finding blobs (derived from find domains
% analysis pipeline) with an otsu thresholding of the zmax projection
% [ there is possibility to do water shedding, see find_blobs function]

% Note it is also exported the zsum of the different channels 

% This script is part of the RegionsAnalysis code.

% RegionsAnalysis code mainly have three structure variables:
% inout structure : provides parameters and strings in relation to input
% and output
% data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.
% dirdata structure: structure compiling the different data structures
% within a folder and some analysis of key features of the different data
% sets.
%
% See README.md document for further explanations about the code.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename)

addpath(genpath(inout.applicationcodefolder))
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis')
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

inout.codefolderSchnitz=fullfile(inout.applicationcodefolder(1:startIndex-1),'Schnitzcells');
addpath(genpath(inout.codefolderSchnitz));

%  Execute initialize_meristemcell function to load the default parameters for the analysis. 
[inout]=initialize_domains;

%%
% I think some of these parameters need to be cleaned out together with
% next section

% MOST RELEVANT PARAMETERS (note these override parameters in  initialize_meristem function)
inout.overwriting_mode=1; % Set 1 to activate the overwriting mode, where all files will be analysed. Otherwise set 0, so that just non-analysed files will be analysed. For LSM files. If this is set to 0, inout.get_polygon_from_previous_data will be automatically set to 0.

inout.get_polygon_from_previous_data=0; % 0 if new data. 1 to get from previous data
inout.max_normalised_level_to_show=0.2; % set a number between 0 to 1. 1 is a non enhanced contrast image.
inout.magnification=160; % Percentage of image magnification. Note that if this is higher than what the screen allows, it will be rest to the default value, which is 100% magnification.

% Caution, this is missleading, read explanation
inout.signal_channel_index=1;         %   channel to look for the segmentation !
inout.marker_channel_index=1           % channel for the signal to quantify !

% Parameters for the automatic stage of the pipeline
otsu_threshold_prefactor=1; % Otsu threshold prefactor for the automatic domain detection
minimal_radius_region_in_microns=1; % Minimal radius allowed for the radial study of the automatically detected regions
radius_prefactor=1; % Prefactor to delimit the size of the domains under study automatically found. If 1, the ROI will have a radius equal to the major axis length.

inout.zsum=0;                         %   if we want to do a zsum 
inout.zmax=1;                         %   if we want to do a zmax 
inout.gauss_sigma_microns=0.5;              %   sigma in microns units
inout.radiusstep_in_microns=1;            %   parameter just working for lsm, it overrides the inout.radiusstep parameter, which is in pixel units. 

% Other parameters 

inout.label_type_in_snap='class';    % set 'label' or 'class'
inout.ask_regionclass=1;  % set this parameter to 1 if wanting to class each region after selecting it (single key character), 0 otherwise

inout.defaultregiontype='Polygon';        % type of region that is going to be analysed (current defaultregiontype options: 'Circle' , 'Cake piece', 'Rectangle', 'Polygon'...). Comment this line if different regions are needed to be explored.;
inout.number_of_regions_setting='on_the_fly'; % 'on_the_fly' option allows you to not havin to set the number of regions to mark in advance.

inout.open_all_positions=1;
inout.make_orthos=0;                   % set to 1 if making ortogonal slices, otherwise set it to 0. 
inout.ortho_thickness_in_microns=10;   % if this field is removed/commented with '%', then the program asks for a second point to mark the region to study.

% Less important parameters
inout.defaultnumberregions=1;             %   default number of regions to analyse in each file
inout.defaultnumberbackgroundregions=0;   %   default number of background regions to analyse in each file
inout.background_subtraction=0;           %   set this parameter to 1 to do the background subtraction, and to 0 otherwise.         
inout.make_fittings=0;                    % set to 1 to fit the intensity vs r curves to generalised exponentials and hill functions. Otherwise, set to 0.
inout.select_background_regions=0;        %   set to 1 for selecting background regions, 0 otherwise. Note this will be subtracted from the image to analyse if inout.background_subtraction=1
inout.make_basic_radial=0;


%%
% Automatic domains finding (through find_all_domains function, in the zmax)

inout.isforschnitz=1; % to export to restults into a schnitz directory, set it to 1, otherwise 0

inout.find_blobs=1; 
inout.min_blob_area=0;

inout.defaultregiontype='Circle';
inout.outsu_threshold_prefactor=otsu_threshold_prefactor;
inout.data_structure_filename='data_auto';
inout.number_of_regions_setting='by_number';
inout.minimal_radius_region_in_microns=minimal_radius_region_in_microns;
% 
inout.get_centroids_from_found_domains=1; % parameter to transfer the found domains centroid to the regions of interest.
inout.radius_region_in_microns=10;     % if this field is removed/commented with '%', then the program asks for a second point to mark the region to study.

inout.label_type_in_snap='label'    % set 'label' or 'class'
inout.ask_regionclass=0;
inout.make_basic_radial=0;
inout.make_fittings=0;     % set to 1 to fit the intensity vs r curves to generalised exponentials and hill functions. Otherwise, set to 0.

inout.get_radius_from_found_domains=1; % For automatically found domains when using find_all_domains, if wanting to get region to study delimited by the size of those domains, set 1, otherwise set 0.
inout.radius_prefactor=radius_prefactor; % Prefactor to delimit the size of the domains under study. If 1, the ROI will have a radius equal to the major axis length.
inout.exclude_central_domain=0



%%
% for background extraction of 3D tiff z-stacks

inout.find_blobs=0; 
inout.zmax=0;           %   if we want to do a zmax 
inout.find_background_through_histogram_peak=1;
inout.extract_features=1;
inout.timecourse=0;

inout.select_background_regions=1;        %   set to 1 for selecting background regions, 0 otherwise. Note this will be subtracted from the image to analyse if inout.background_subtraction=1
inout.defaultnumberbackgroundregions=1;
inout.islsm=0;
inout.tiff_max_int=0;

%%

clear data
if inout.open_all_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end



