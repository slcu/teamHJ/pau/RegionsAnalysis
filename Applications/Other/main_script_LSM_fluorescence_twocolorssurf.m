%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script allows to manually select different ROIs and plot the fluorescence of two
% channels in surf plots. 

% This script is part of the RegionsAnalysis code.

% RegionsAnalysis code mainly have three structure variables:
% inout structure : provides parameters and strings in relation to input
% and output
% data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.
% dirdata structure: structure compiling the different data structures
% within a folder and some analysis of key features of the different data
% sets.

% See readme.doc document for further explanations about the code.

% Contact: pau.formosajordan@slcu.cam.ac.uk
% Last version: 03/02/2017

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.codefolder=fileparts(tmp.Filename)
addpath(genpath(inout.codefolder))

%  Execute initialize_meristemcell function to load the default parameters for the analysis. 
[inout]=initialize_twocolorsurf;

% MOST RELEVANT PARAMETERS (note these parameters might override parameters set by the previous initialize function)

inout.click_selecting_regions=1;    % set to 1 for selecting regions by clicking, set to 0 to load it from previous data. It just works for circular regions (it will load origin and radius used).
inout.get_center_and_radius_from_previous_data=0; % for loading from previous data. It just works for circular regions (it will load origin and radius used).

inout.make_orthos=0;                   % set to 1 if making ortogonal slices, otherwise set it to 0. 
inout.plot_fluor_surf_2channels=1;

inout.ortho_thickness_in_microns=10;   % if this field is removed/commented with '%', then the program asks for a second point to mark the region to study.

inout.background_subtraction=0;           %   set this parameter to 1 to do the background subtraction, and to 0 otherwise.         
inout.select_background_regions=0;        %   set to 1 for selecting background regions, 0 otherwise. Note this will be subtracted from the image to analyse if inout.background_subtraction=1

inout.signal_channel_index=1;         %   channel to look at if lsm files
inout.marker_channel_index=2;
inout.zsum=0;                         %   if we want to do a zsum 
inout.zmax=0;                         %   if we want to do a zmax 
inout.gauss_sigma_microns=1;              %   sigma in microns units
inout.radiusstep_in_microns=1;            %   parameter just working for lsm, it overrides the inout.radiusstep parameter, which is in pixel units. 

% Less important parameters
inout.make_fittings=0;              % set to 1 to fit the intensity vs r curves to generalised exponentials and hill functions. Otherwise, set to 0.
inout.ask_numberregions=1;                %   set this parameter to 1 if you want matlab ask you how many regions to analyze per file, otherwise set it to 0. 
inout.defaultnumberregions=1;             %   default number of regions to analyse in each file
inout.defaultnumberbackgroundregions=1;   %   default number of background regions to analyse in each file
inout.post_analysis=0;                    %   set this parameter to 1 to do the fit to averaged exponent found after fittings, otherwise set to 0. Note this is possible when there is just one selected region per image.
inout.compute_int_vs_r=0                  % set to 1 to compute and plot the intensity vs r in the rectangle or circular region. Otherwise, set it to 0.

inout.plot_background_regions_stuff=0 ; % if select_background_regions, in case you want to see histograms and surface plots, set it to 1, otherwise to 0.

inout.plot_fluor_surf=0;

% Execute read_rawadata for importing tiff or lsm files, doing the
% pertinent analysis and creating the data structure.

clear data
if inout.open_all_files_in_directory==1
    read_rawdata(inout)
else
    [data]=read_rawdata(inout)
end

