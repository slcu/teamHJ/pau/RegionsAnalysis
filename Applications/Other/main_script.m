% Initialize parameters

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

%addpath(genpath('/Users/pauformosa-jordan/Copy/Code/MATLAB/RegionsAnalysis'))
%%
% Benoit initiatization. Execute this function to load the
% default parameters of the script. 

[inout]=initialize_meristemcell;
[dirdata]=create_dirdata(inout);

%%
% Pau initialization

[inout]=initialize_pinsigcell;
inout.scope='spi'; % Spinning disk but without Dual filters is 'spu'. Otherwise say 'spi'

%%
%
% Pau initialization

[inout]=initialize_fluor_analysis;
inout.scope='spi'; % Spinning disk but without Dual filters is 'spu'. Otherwise say 'spi'
%%

% Pau initialization
[inout]=initialize_for_schnitzcells;

%%

inout.thresh_fluor_marker=0;  % from snap1 s1, t1.
inout.thresh_fluor_signal=0;
inout.threshold_backgroun_subtraction=1;
inout.load_data_for_background_file=0;
inout.extract_regions_mean_features=1;
inout.gradient_marker_max=1000;

%%
% Execute read_rawadata for first reading tiff files, etc, and creating data structure
inout.max_fluor_to_plot=2500;

inout.make_basic_radial=1
clear data
if inout.open_all_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end

%[data]=RegionsAnalysis;

%%
% Execute create_dirdata for creating and loading a particular dirdata structure and playing with it
% To extract dirdata set, write 
% e.g. dirdata.dataset{number}

[dirdata,datapath]=create_dirdata;


%%
% Execute import_data loading a particular data structure and playing with it
% To extract data set, write data.featureofinterest
% e.g. data.region{number}
% e.g. data.region{1}.regionmask shows the mask of the selected region
[data]=import_data;

%%
% Given a list of regions to analize in a data structure, analysis_regions
% provide differerent tools to analyze the data.
% 3 main options for passing a lis:
% 1.- Using the list in data.regionlist, what contains all regions analysed
% for a data set.
% 2.- Using the temporary list data.regionlisttemp (updated every time one
% adds new regions)
% 3.- Using a particular list set by you, e.g. list_of_regions=[2 1]

list_of_regions=data.regionlist;
%list_of_regions=data.regionlisttemp;  % Note data.regionlisttemp 
%list_of_regions=[2,1];
[data]=analysis_regions(inout,data,list_of_regions);

%%

% Use add_regions in case you have already a data structure and you want to keep it but add new regions.
% Note that add_regions function calls import_data function

[data]=add_regions_to_study(data);


%%

make_snap_all_regions(inout,data)

