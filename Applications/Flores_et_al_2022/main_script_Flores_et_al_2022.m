%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% main_script_Flores_et_al_2022 script allows the selection of a polyonal ROI 
% in one of the channels of an LSM, CZI or LIF files, and computes the mean intensity for the selected ROI. 

% This script is part of the RegionsAnalysis code.

% RegionsAnalysis code mainly have three structure variables:
% inout structure : provides parameters and strings in relation to input
% and output
% data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.
% dirdata structure: structure compiling the different data structures
% within a folder and some analysis of key features of the different data
% sets.

% See README.md document for further explanations about the code.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

%  Execute initialize_meristemcell function to load the default parameters for the analysis. 
[inout]=initialize_Flores_et_al_2022;

% MOST RELEVANT PARAMETERS (note these parameters might override parameters set by the previous initialize function)
inout.init_file_to_open=1;              % Initial image number to open. If this line is commented, the default number is one. Useful when willing to execute the pipeline from a certain image number.  

inout.draw_freehand_polygon=1; % enables drawing of the polyigon in a freehand style. Otherwise set it to 0, and multiple clicks will be needed to plot the polygon.
inout.defaultregiontype='Polygon';   % type of region that is going to be analysed (current defaultregiontype options: 'Circle' and 'Cake piece'). Comment this line if different regions are needed to be explored.;
inout.make_historegion=1;           % set to 1 to make histograms for the regions of interest, otherwise set to 0.

inout.signal_channel_index=2;         %   channel to look at if lsm, czi or lif files
inout.imshow_in_fire=1;                   % set 1 to show image in fire, 0 otherwise. 

inout.max_normalised_level_to_show=0.9; % set a number between 0 to 1. 1 is a non enhanced contrast image.
inout.get_polygon_from_previous_data=0; % set 1 to get polygons from previous data (note, then also set inout.click_selecting_regions=0), otherwise set it to 0
inout.click_selecting_regions=1;    % set to 1 for selecting regions by clicking, set to 0 to load it from previous data or it will find the origin of the central domain. 

inout.magnification=180;                % Percentage of image magnification. Note that if this is higher than what the screen allows, it will be rest to the default value, which is 100% magnification.

inout.plot_ROIs_in_snap_all_regions=1; % set 1 to plot the ROIs in the file showing the ROI labels, otherwise set 0.
inout.extract_features=1;  % set to 1 to extract the basic computed features into a csv file, otherwise set to 0.

inout.make_basic_radial=0;
inout.get_center_and_radius_from_previous_data=0; % for loading from previous data. It just works for circular regions (it will load origin and radius used).
inout.find_central_domain=0; % set to 1 to find the central domain in the xy plane. For lsm files. Otherwise, set to 0.
inout.get_center_from_cental_domain=0; % set to 1 to find the origin from the central domain. Note that the radius of the selected region will still be set by the inout.radius_region_in_microns parameter. 

inout.open_all_positions=1;
inout.make_orthos=0;                   % set to 1 if making ortogonal slices, otherwise set it to 0. 

inout.radius_region_in_microns=50;     % if this field is removed/commented with '%', then the program asks for a second point to mark the region to study.
inout.ortho_thickness_in_microns=10;   % if this field is removed/commented with '%', then the program asks for a second point to mark the region to study.

inout.zsum=1;                         %   if we want to do a zsum 
inout.zmax=0;                         %   if we want to do a zmax 
inout.gauss_sigma_microns=5;              %   sigma in microns units
inout.radiusstep_in_microns=1;            %   parameter just working for lsm, it overrides the inout.radiusstep parameter, which is in pixel units. 

% Less important parameters
inout.defaultnumberregions=1;             %   default number of regions to analyse in each file
inout.defaultnumberbackgroundregions=0;   %   default number of background regions to analyse in each file
inout.post_analysis=0;                    %   set this parameter to 1 to do the fit to averaged exponent found after fittings, otherwise set to 0. Note this is possible when there is just one selected region per image.
inout.background_subtraction=0;           %   set this parameter to 1 to do the background subtraction, and to 0 otherwise.         
inout.make_fittings=0;                    % set to 1 to fit the intensity vs r curves to generalised exponentials and hill functions. Otherwise, set to 0.
inout.ask_numberregions=0;                %   set this parameter to 1 if you want matlab ask you how many regions to analyze per file, otherwise set it to 0. 
inout.select_background_regions=0;        %   set to 1 for selecting background regions, 0 otherwise. Note this will be subtracted from the image to analyse if inout.background_subtraction=1
inout.fit_hole=0;                         %   for fitting TCS signal (circular gradient-like shape with a dip in the center), set it to 1, otherwise, to 0

%
% Execute read_rawadata for importing tiff or lsm files, doing the
% pertinent analysis and creating the data structure.

clear data
if inout.open_all_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end


if inout.extract_features==1
    filein=fullfile(inout.datapath,'dirdata_features.csv');
    fileout=fullfile(inout.datapath,'dirdata_features_STM_maxima.csv')
    movefile(filein,fileout);
end


