Flores et al Code: Analysis of the central fluorescence expression domain in the Arabidopsis shoot apical meristem
-----------------------------------------------------------------------------------------------------------------------------------------

This Code has been used for the following manuscript (2022):

XYZXYZ

CONTACT

Pau Formosa-Jordan - pformosa@mpipz.mpg.de


INSTALLATION NOTES
---------------------------

This code has been run using MATLAB 20017a with Windows 7 Professional and in Mac (OS X El Capitan).  

For further details about the installation, see the main Readme file of the RegionsAnalysis repository.


SUMMARY
------------

main_script_Flores_et_al_2022 script is used for computing the average fluorescence intensity at the shoot apical meristem for different fluorescent reporters.   

This script is part of the RegionsAnalysis package. Run the first part of the script to read the data and get the analysis done.


EXPLANATION OF THE CODE AT A GLANCE
------------------------------------

INPUT:


OUTPUT:
