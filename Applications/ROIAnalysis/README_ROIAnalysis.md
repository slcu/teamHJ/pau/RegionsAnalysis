ROIAnalysis Code
---------------------------------------

CONTACT: pformosa@mpipz.mpg.de


SUMMARY
------------
(Under construction)

Scripts within the ROIAnalysis Code allow to draw, save and analyse regions of interest of for a given set of images. Regions of interest can be loaded and the analysis can be run again on the regions.

This script is part of the RegionsAnalysis package.

INSTRUCTIONS at a GLANCE
-----------------------------------

(Under construction)

- Main files:
main_script_multipolygons : optimised to draw ROIs in TIFF, LSM and CZI files.

See parameter descriptions in the comments of the main files.


INSTALLATION NOTES
---------------------------

This code has been run using MATLAB 2020a with Windows 7 Professional and in Mac (OS X El Capitan).

- Note :

For doing zooms on the studied images, you will need to use some external functions from Schnitzcells software that are not provided herein. To download Schnitzcells, please go to
http://easerver.caltech.edu/wordpress/schnitzcells/ . Once downloaded it, you can copy the whole package within the directory of External Functions, or just need to copy makergb.m, maxmax.m, minmin.m.
