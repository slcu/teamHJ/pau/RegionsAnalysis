function [smooth_timecourse_vect]=smooth_mean_timecourse_vect(timecourse_vect,smoothening_window_width)

if mod(smoothening_window_width,2)==0
    displ('window width has to be odd')
    return
end

vectsize=size(timecourse_vect);
timepoints=vectsize(1);
window=smoothening_window_width;
widthmargin=round((window-1)/2);
% the first time points can not be smoothened, so they keep the same
smooth_timecourse_vect(1:widthmargin,:)=timecourse_vect(1:widthmargin,:);
smooth_timecourse_vect(timepoints-(widthmargin+1):timepoints,:)=timecourse_vect(timepoints-(widthmargin+1):timepoints,:);

for k=widthmargin+1:timepoints-widthmargin 
    win_tc=timecourse_vect(k-(widthmargin):k+widthmargin,:);
%    size(win_tc);
%    win_tc(win_tc==min(win_tc,2))=[];
%    win_tc(win_tc==max(win_tc,2))=[];
    reduced_win_tc=zeros(smoothening_window_width-2,vectsize(2));

    for j=1:vectsize(1)
        wj=win_tc(j,:);
        mn=min(wj);
        mx=max(wj);
        wj(wj==mn)=[];
        wj(wj==mx)=[];
        
        if isempty(wj)==1
            wj=mn*ones(smoothening_window_width-2,1);
        end
        
        reduced_win_tc(j,:)=wj;
    end
    smooth_timecourse_vect(:,k)=mean(reduced_win_tc,2);
end


