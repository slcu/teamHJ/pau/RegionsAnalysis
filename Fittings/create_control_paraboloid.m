function[Paraboloid]=create_control_paraboloid(inout,data,ISIZE,curvature,tilt)
    %Top Paraboloid Control
    Paraboloid=zeros(ISIZE(1),ISIZE(2),ISIZE(3));
    ISIZE_um=[ISIZE(1)*data.original_resolution(1,1),ISIZE(2)*data.original_resolution(1,2),ISIZE(3)*data.original_resolution(1,3)];
    tilt=tilt*pi/180;
    Rx=[1 0 0; 0 cos(tilt) -sin(tilt); 0 sin(tilt) cos(tilt)];
    Ry=[cos(tilt) 0 sin(tilt); 0 1 0; -sin(tilt) 0 cos(tilt)];
    Rz=[cos(tilt) -sin(tilt) 0; sin(tilt) cos(tilt) 0; 0 0 1];
    a=curvature;
    b=a;
    if inout.work_on_orthos==1
        %Top Paraboloid Control
        for zpos = 1:ISIZE(3)                
            for xpos = 1:ISIZE(1)                                
                for ypos = 1:ISIZE(2)
                    xpos_um=xpos*data.original_resolution(1,1);
                    ypos_um=ypos*data.original_resolution(1,2);
                    zpos_um=zpos*data.original_resolution(1,3);
                    tilted=[xpos_um ypos_um zpos_um]*Rx;
                    paraboloid_um=-a*(tilted(1)-ISIZE_um(1)/2).^2 -b*(tilted(2)-ISIZE_um(2)/2).^2 +ISIZE_um(3)*3/4;
                    if zpos_um<paraboloid_um
                        Paraboloid(xpos,ypos,zpos)=1;
                    end                         
                end
            end
        end
    else
        %Side Paraboloid Control
        for zpos = 1:ISIZE(3)                
            for xpos = 1:ISIZE(1)                                
                for ypos = 1:ISIZE(2)
                    xpos_um=xpos*data.original_resolution(1,1);
                    ypos_um=ypos*data.original_resolution(1,2);
                    zpos_um=zpos*data.original_resolution(1,3);
                    tilted=[xpos_um ypos_um zpos_um]*Rz;
                    paraboloid_um=-a*(tilted(3)-ISIZE_um(3)/2).^2 -b*(tilted(2)-ISIZE_um(2)/2).^2 +ISIZE_um(1)*3/4;
                    if xpos_um<paraboloid_um
                        Paraboloid(xpos,ypos,zpos)=1;                        
                    end                            
                end
            end
        end 
    end
    figure('Position',[800 2000 1080 1080])
    imshow3Dfull(Paraboloid);
end