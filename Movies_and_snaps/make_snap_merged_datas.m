function make_snap_merged_datas(inout,data1,vect1,data2,vect2)

% This function gets data1 with assigned classes and data2, and its respective alignement vectors
% (see merge_features_datasets) and plots just the resulting common regions
% with the regions drawn from data2 and the classes from data1.

font=18;
out_merged_path=fullfile(inout.datapath,'aligned_regions');
create_dir(out_merged_path)
rootstring=strcat(data1.rootstring_first_image_name,'_aligned_showing_class');
name=strcat(rootstring,'.jpeg');
namefig=strcat(rootstring, '.fig');

outfilename=fullfile(out_merged_path,name);
outfilenamefig=fullfile(out_merged_path,namefig);

h=figure();

is=imshow(data1.image_with_regions);
%is=imshow(mat2gray(data1.image_with_regions),'Colormap',morgenstemning);

if inout.snap_regions_with_labels==1
    for ii=1:size(vect2,2)
        jj=vect2(ii);
        kk=vect1(ii);
        pos=data2.region{jj}.origin;
        class=num2str(data1.region{kk}.class);

         hnd1=text(pos(1)-10,pos(2),class);

        if inout.plot_ROIs_in_snap_all_regions==1
            set(hnd1,'FontSize',inout.fontsize,'Color',inout.color_labels)
            BW8 = uint16(bwperim(data2.region{jj}.regionmask,8));
            BW8=imdilate(BW8, strel('disk',2));
            is.CData(:,:,3)=is.CData(:,:,3)+double(BW8); 
        end

    end
end

saveas(h,outfilename)


savefig(outfilenamefig)

%print -dpdf -painters epsFig
end
