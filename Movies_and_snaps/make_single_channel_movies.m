function [M1,M2]=make_single_channel_movies(inout,data)

% make_2channels_movies_new creates two different movies of the marker and signal data channels
% The first movie is the combined snap
% The second movie is a concatenated combined snap, the marker snap and the
% signal snap.
% The input function is the data structure, but if no inputs 
% are provided the data.mat file is imported automatically after selection of
% its directory location . 

if(nargin < 1)
datapath = uigetdir('/','Select the data set location');
[data,~]=import_data(datapath);
else
  datapath=inout.datapath;
end

signal=permute(data.fluor_signal_timecourse(:,:,1,:),[1,2,4,3]);
time_frames=data.time_frames;

string_name=data.twochannels_string;



%mxs=double(max(max(max(signal))));

sigvect=reshape(signal,[1 data.image_size(1)*data.image_size(2)*time_frames]);


if isfield(inout,'saturation_percentile_signal')
    %mxs=double(prctile(max(max(signal)),inout.saturation_percentile_signal));
     saturation_percentile_signal=inout.saturation_percentile_signal;
else
    %mxs=double(prctile(max(max(signal)),70));
    saturation_percentile_signal=70;
end

mxs=double(prctile(sigvect,saturation_percentile_signal));

white=zeros(size(signal,1),size(signal,2));


%%% First movie

name=strcat('Signal_mov_',string_name);
%outfilename=fullfile(data.outdatapath,name);
outfilename=fullfile(datapath,name);



if inout.make_signal_movie==1 
f0 = figure; 

aviobj = VideoWriter(outfilename,'Motion JPEG AVI'); aviobj.FrameRate = 5;
open(aviobj);

ind=0; h=waitbar(0,'Generating the signal channels movie'); % generates a waitbar

imm=zeros(size(signal,1),size(signal,2),3);

if isfield(inout,'saturation_percentile_marker')    
    saturation_percentile_marker=inout.saturation_percentile_marker;
else
    saturation_percentile_marker=50;
end

marker=permute(data.fluor_marker_timecourse(:,:,1,:),[1,2,4,3]);
markvect=reshape(marker,[1 data.image_size(1)*data.image_size(2)*time_frames]);
mxm=double(prctile(markvect,saturation_percentile_marker));

mxm=double(max(max(max(marker))));
output=zeros(size(signal,1),size(signal,2),time_frames);
for k=1:time_frames
    for i=1:size(signal,1)
        for j=1:size(signal,2)
            output(i,j,k)=max(signal(i,j,k),marker(i,j,k));
        end
    end
end

% signal movie
for k=1:time_frames
    waitbar(1.0*ind/length(time_frames)); ind=ind+1; 
   % imm(:,:,1)=1.0*marker(:,:,k)/(1.0*mxm);
  %  imm(:,:,2)=1.0*marker(:,:,k)/(1.0*mxm);
   % imm(:,:,3)=1.0*signal(:,:,k)/(1.0*mxs);

    imm(:,:,1)=white;
    imm(:,:,2)=mat2gray(signal(:,:,k),[0 mxs]);
    imm(:,:,3)=white;

if inout.brightfield_in_movie==1
    imm(:,:,1)=mat2gray(signal(:,:,k),[0 mxs]);
    imm(:,:,2)=mat2gray(output(:,:,k),[0 mxs]);
    imm(:,:,3)=mat2gray(signal(:,:,k),[0 mxs]);
end

    %  before it was  imshow(imm), but for some reason, sometimes the
    %  signal was not appearing in the movie, so I normalised again with mat2gray (not
    %  sure why is necessary ...). Idem for marker movie.
    
    imshow(mat2gray(imm)); 
    M1 = getframe(f0);
    writeVideo(aviobj, M1);
end 

close(h)
close(aviobj);

end 




%
%%% Second movie
if inout.make_marker_movie==1 

if isfield(inout,'saturation_percentile_marker')    
    saturation_percentile_marker=inout.saturation_percentile_marker;
else
    saturation_percentile_marker=50;
end

marker=permute(data.fluor_marker_timecourse(:,:,1,:),[1,2,4,3]);
markvect=reshape(marker,[1 data.image_size(1)*data.image_size(2)*time_frames]);
mxm=double(prctile(markvect,saturation_percentile_marker));

mxm=double(max(max(max(marker))));

name=strcat('Marker_mov_',string_name);
%outfilename=fullfile(data.outdatapath,name);
outfilename=fullfile(datapath,name);

f0 = figure; 

aviobj = VideoWriter(outfilename,'Motion JPEG AVI'); aviobj.FrameRate = 5;
open(aviobj);

ind=0; h=waitbar(0,'Generating the marker channels movie'); % generates a waitbar

imm=zeros(size(marker,1),size(marker,2),3);

% signal movie
for k=1:time_frames
    waitbar(1.0*ind/length(time_frames)); ind=ind+1; 
   % imm(:,:,1)=1.0*marker(:,:,k)/(1.0*mxm);
  %  imm(:,:,2)=1.0*marker(:,:,k)/(1.0*mxm);
   % imm(:,:,3)=1.0*signal(:,:,k)/(1.0*mxs);

    imm(:,:,1)=mat2gray(marker(:,:,k),[0 mxm]);
    imm(:,:,2)=white;
    imm(:,:,3)=mat2gray(marker(:,:,k),[0 mxm]);

    imshow(mat2gray(imm));
    M1 = getframe(f0);
    writeVideo(aviobj, M1);
end 

close(h)
close(aviobj);


end

%movefile(fullfile(datapath,'*.avi'),fullfile(datapath,'MOVS'))

end

%movefile(fullfile(datapath,'*.avi'),fullfile(datapath,'MOVS'))

