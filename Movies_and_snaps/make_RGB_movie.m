function [M1,M2]=make_RGB_movie(inout,data)

% make_RGB_movie creates a movie coming from TIFF files conatining
% different channels. 

% The input function is the data structure, but if no inputs 
% are provided the data.mat file is imported automatically after selection of
% its directory location . 

if(nargin < 1)
datapath = uigetdir('/','Select the data set location');
[data,~]=import_data(datapath);
else
  datapath=inout.datapath;
end

signal=data.fluor_signal_timecourse(:,:,:,:);
time_frames=data.time_frames;

string_name=data.rootstring_first_image_name;


%%% First movie

name=strcat('Combined_channels_mov_',string_name);
%outfilename=fullfile(data.outdatapath,name);
outfilename=fullfile(datapath,name);

f0 = figure; 

aviobj = VideoWriter(outfilename,'MPEG-4'); 
%aviobj.FrameRate = 5;
%aviobj.FileFormat='avi'
open(aviobj);

ind=0; h=waitbar(0,'Generating the combined channels movie'); % generates a waitbar

%imm=zeros(size(signal,1),size(signal,2),3);


for k=1:time_frames
    waitbar(1.0*ind/length(time_frames)); ind=ind+1; 

    imm=signal(:,:,:,k);
    imm=imresize(imm, 0.4);
    imshow(imm);
    M1 = getframe(f0);
    writeVideo(aviobj, M1);
end 

close(h)
close(aviobj);

reader = VideoReader(strcat(outfilename,'.mp4'));
writer = VideoWriter(strcat(outfilename,'www.avi'), ...
                        'Uncompressed AVI');
writer.FrameRate = reader.FrameRate;
open(writer);

while hasFrame(reader)
   img = readFrame(reader);
   writeVideo(writer,img);
end
%dipl('printing')
close(writer);
%movefile(fullfile(datapath,'*.mp4'),fullfile(datapath,'MOVS'))

end


